//
//  WRUserManager.h
//  WickedRide
//
//  Created by Ajith Kumar on 10/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WRUser.h"

@interface WRUserManager : NSObject

@property (nonatomic, strong) WRUser *user;
@property (nonatomic, strong) WRUser *unArchivedUser;

@property (nonatomic, strong) NSArray *locationArray;
@property (nonatomic, strong) NSArray *areaArray;
@property (nonatomic, strong) NSDictionary *htmlUrlDict;

+ (WRUserManager *)sharedManager;

- (BOOL)hasPreviousLoggedInUser;
- (void)logOutUser;

@end
