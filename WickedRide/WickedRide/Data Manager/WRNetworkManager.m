//
//  WRNetworkManager.m
//  WickedRide
//
//  Created by Ajith Kumar on 10/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRNetworkManager.h"
#import "WRConstants.h"

@implementation WRNetworkManager

+ (WRNetworkManager *)sharedManager
{
    __strong static id _manager = nil;
    
    static dispatch_once_t onceToken = 0;
    
    dispatch_once(&onceToken, ^{
        
        _manager = [[self alloc] initWithBaseURL:[NSURL URLWithString:Base_URL]];
        
    });
    
    return _manager;
}


#pragma mark - Netwokring Methods

- (void)startGETRequestWithAPI:(NSString *)getAPI andParameters:(id)parameters withSuccess:(GETResponseWithSuccess)success andFailure:(GETResponseFailed)failure
{
    
    [self GET:getAPI parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSError *error = nil;
        
        // Check for response object type
        if ([responseObject isKindOfClass:[NSDictionary class]])
        {
            success (responseObject);
        }
        else if ([responseObject isKindOfClass:[NSData class]])
        {
            NSDictionary *objDict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
            if (error != nil)
            {
                failure (error.localizedDescription);
            }
            else
            {
                success (objDict);
            }
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        failure (error.localizedDescription);
    }];
}


- (void)startPOSTRequestWithAPI:(NSString *)postAPI andParameters:(id)parameters withSuccess:(POSTResponseWithSuccess)success andFailure:(POSTResponseFailed)failure
{
 
    [self setRequestSerializer:[AFJSONRequestSerializer serializer]];
    [self.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [self POST:postAPI parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSError *error = nil;
        
        // Check for response object type
        if ([responseObject isKindOfClass:[NSDictionary class]])
        {
            success (responseObject);
        }
        else if ([responseObject isKindOfClass:[NSData class]])
        {
            NSDictionary *objDict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
            if (error != nil)
            {
                failure (error.localizedDescription);
            }
            else
            {
                success (objDict);
            }
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
//        NSInteger statusCode = operation.response.statusCode;
//        if (statusCode == 401) {
//            failure (@"401");
//        }
//        else
       // {
            failure (error.localizedDescription);
       // }
    }];
}



-(void)uploadImage:(UIImage *)lookImage withUserID:(NSString *)userID WithSuccessHandler:(SuccessHandler)resultHandler WithErrorHandler:(ErrorHandler)errorHandler
{
    int userId = [userID intValue];
    NSString *apiURL = [NSString stringWithFormat:@"%@cloverupload/",@""];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSData *imageData = UIImageJPEGRepresentation(lookImage, 1);
    AFHTTPRequestOperation *operation = [manager POST:apiURL parameters:@{@"userid":[NSNumber numberWithInt:userId]} constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        [formData appendPartWithFileData:imageData name:@"files" fileName:@"photo.jpg" mimeType:@"image/jpeg"];
        
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *responseDict = (NSDictionary *)responseObject;
        NSString *imageLink = [responseDict valueForKey:@"url"];
        
        resultHandler(nil,imageLink,YES);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        errorHandler(error,@"Error in network connection. Try again",NO);
    }];
    [operation start];
}

- (BOOL)isNetworkReachable
{
    return [AFNetworkReachabilityManager sharedManager].reachable;
}

@end
