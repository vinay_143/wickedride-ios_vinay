//
//  UIImage+colors.h
//  WickedRide
//
//  Created by Ajith Kumar on 10/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//
//

#import <UIKit/UIKit.h>

@interface UIImage (colors)
+ (UIImage *)imageWithColor:(UIColor *)color;
+ (UIImage *)imageWithColor:(UIColor *)color withRect : (CGRect )imageRect;
- (NSString *)base64String;
@end
