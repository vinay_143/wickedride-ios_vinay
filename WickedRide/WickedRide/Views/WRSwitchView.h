//
//  WRSwitchView.h
//  WickedRide
//
//  Created by Ajith Kumar on 11/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol WRSwitchViewDelegate <NSObject>

@optional

-(void)switchViewTappedAndIsHourModeSelected:(BOOL )isHourModeSelected;

@end

@interface WRSwitchView : UIView

@property (nonatomic, weak) id <WRSwitchViewDelegate> delegate;
@property (nonatomic, assign) BOOL isHoursSelected;

-(void)configureTheViewPropertiesAndIsHourSelected:(BOOL )isHoursSelected;


@end
