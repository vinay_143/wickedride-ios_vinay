//
//  WRBikationMoreInfoView.h
//  WickedRide
//
//  Created by Ajith Kumar on 26/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WRBikationMoreInfoView : UIView
@property (weak, nonatomic) IBOutlet UIImageView *infoImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@end
