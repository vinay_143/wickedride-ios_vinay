//
//  WRCustomNavView.h
//  WickedRide
//
//  Created by Ajith Kumar on 14/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol WRCustomNavViewDelegate <NSObject>

@optional

-(void)customNavCloseButtonTapped;
-(void)customNavBackButtonTapped;

@end

@interface WRCustomNavView : UIView

@property (weak, nonatomic) IBOutlet UILabel *navTitleLabel;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;
@property (weak, nonatomic) IBOutlet UIButton *backButton;


@property (nonatomic, weak) id <WRCustomNavViewDelegate> delegate;


@end
