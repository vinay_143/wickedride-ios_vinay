//
//  WRCustomNavView.m
//  WickedRide
//
//  Created by Ajith Kumar on 14/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRCustomNavView.h"

@implementation WRCustomNavView

-(void)awakeFromNib
{
    [super awakeFromNib];
//    [self.backButton.layer setShadowColor:[[UIColor blackColor] CGColor]];
//    [self.backButton.layer setShadowOffset:CGSizeMake(1.0, 1.0)];
//    [self.backButton.layer setShadowOpacity:1.0];
//    [self.backButton.layer setShadowRadius:2.0];
}

#pragma mark - UIButton/ Tap Action methods

- (IBAction)closeButtonActionMethod:(id)sender
{
    [self.delegate customNavCloseButtonTapped];
}

- (IBAction)backButtonAction:(id)sender
{
    [self.delegate customNavBackButtonTapped];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
