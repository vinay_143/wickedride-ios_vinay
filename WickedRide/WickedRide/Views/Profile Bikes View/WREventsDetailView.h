//
//  WREventsDetailView.h
//  WickedRide
//
//  Created by Ajith Kumar on 10/12/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WRBikation.h"

@protocol WREventsDetailViewDelegate <NSObject>

@optional

-(void)attendedLabelTappedWithSelectedBikation:(WRBikation *)selectedBikation;

@end

@interface WREventsDetailView : UIView
@property (weak, nonatomic) IBOutlet UILabel *eventNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *BookingStatusLabel;
@property (weak, nonatomic) IBOutlet UILabel *eventConductorLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UIView *noEventsView;

@property (nonatomic, strong) WRBikation *bikationObj;

@property (nonatomic, weak) id <WREventsDetailViewDelegate> delegate;

-(void)configureViewWithBookingDetails:(WRBikation *)bikation andIsAttended:(BOOL )isAttended;

@end
