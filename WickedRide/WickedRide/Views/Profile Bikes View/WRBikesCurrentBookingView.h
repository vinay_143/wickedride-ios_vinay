//
//  WRBikesCurrentBookingView.h
//  WickedRide
//
//  Created by Ajith Kumar on 31/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WRBooking.h"


@interface WRBikesCurrentBookingView : UIView

-(void)configureViewWithBookingDetails:(WRBooking *)bookingObj;


@end
