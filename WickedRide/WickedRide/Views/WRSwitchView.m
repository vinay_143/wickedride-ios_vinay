//
//  WRSwitchView.m
//  WickedRide
//
//  Created by Ajith Kumar on 11/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRSwitchView.h"
#import "UIView+FrameChange.h"

@interface WRSwitchView ()


@property (weak, nonatomic) IBOutlet UIView *tintView;
@property (weak, nonatomic) IBOutlet UILabel *typeLabel;


@end

@implementation WRSwitchView

-(void)awakeFromNib
{
    [super awakeFromNib];
}

-(void)configureTheViewPropertiesAndIsHourSelected:(BOOL)isHoursSelected
{
    self.isHoursSelected = isHoursSelected;
    
    UITapGestureRecognizer *switchTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(switchViewTapped)];
    [self addGestureRecognizer:switchTap];
    
    UITapGestureRecognizer *tintTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(switchViewTapped)];
    [self.tintView addGestureRecognizer:tintTap];
    

    self.layer.cornerRadius = 14.0;
    self.clipsToBounds = YES;
    
    self.layer.borderWidth = 0.8;
    self.layer.borderColor = [[UIColor colorWithRed:51.0/255.0 green:51.0/255.0 blue:51.0/255.0 alpha:1.0] CGColor];
    
    self.tintView.layer.cornerRadius = 10.0;
    self.tintView.clipsToBounds = YES;

}


#pragma mark - UIButton/ Tap Actions

-(void)switchViewTapped
{
    if (self.isHoursSelected) {
        self.isHoursSelected = NO;
        [UIView animateWithDuration:0.2 animations:^{
            [self.tintView changedXAxisTo:54];
            self.typeLabel.text = @"DAYS";
            self.typeLabel.textAlignment = NSTextAlignmentLeft;
        }];
    }
    else
    {
        self.isHoursSelected = YES;
        [UIView animateWithDuration:0.2 animations:^{
            [self.tintView changedXAxisTo:3];
            self.typeLabel.text = @"HOURS";
            self.typeLabel.textAlignment = NSTextAlignmentRight;
        }];
    }
    
    [self.delegate switchViewTappedAndIsHourModeSelected:self.isHoursSelected];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
