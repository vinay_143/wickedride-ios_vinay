//
//  WRClockView.h
//  WickedRide
//
//  Created by Ajith Kumar on 09/09/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol WRClockViewDelegate <NSObject>

@optional

-(void)meridiemType:(NSString *)type;
-(void) selectedHourValue:(NSString *)hourValue;
@end

@interface WRClockView : UIView

@property (weak, nonatomic) IBOutlet UIView *backgroundView;

@property (strong, nonatomic) UIFont *digitFont;
@property(nonatomic,assign)int clockNumber;

-(void)configureViewAccordingToViewSize;

@property (nonatomic, weak) id <WRClockViewDelegate>delegate;


@end
