//
//  WRClockView.m
//  WickedRide
//
//  Created by Ajith Kumar on 09/09/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRClockView.h"
#import "WRConstants.h"
#import "UIView+FrameChange.h"

@interface WRClockView ()

@property (weak, nonatomic) IBOutlet UIButton *amButton;
@property (weak, nonatomic) IBOutlet UIButton *pmButton;
@end
@implementation WRClockView

-(void)awakeFromNib
{
    [super awakeFromNib];
}

-(void)configureViewAccordingToViewSize
{
    if (IS_IPHONE_5_OR_BELLOW) {
        [self.backgroundView changeWidthTo:250.0 andHeightTo:250.0];
        self.backgroundColor = [UIColor clearColor];
    }
    self.backgroundView.layer.cornerRadius = self.backgroundView.frame.size.width/2;
    self.backgroundView.clipsToBounds = YES;
    
    self.amButton.layer.cornerRadius = self.amButton.frame.size.width/2;
    self.amButton.clipsToBounds = YES;
    
    self.pmButton.layer.cornerRadius = self.pmButton.frame.size.width/2;
    self.pmButton.clipsToBounds = YES;
    
    self.clockNumber = 1;
    self.digitFont = AVENIR_BOOK(24.0);
    [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(runTimer:) userInfo:nil repeats:YES];
}

-(void)runTimer:(NSTimer *)timer
{
    if (self.clockNumber <=12) {
        [self drawnumber];
        self.clockNumber +=1;
    }
    else
    {
        [timer invalidate];
    }
}

#pragma mark- Draw Number
-(void)drawnumber
{
    //To draw number in circle
    int distancebetweentheNumber=30; // if u want 24 hr format then  distancebetweentheNumber=15
    CGRect rect=self.backgroundView.frame;
    CGFloat paddingFromMargin=2;
    CGPoint center = CGPointMake(rect.size.width/2.0f, rect.size.height/2.0f);
    CGFloat markingDistanceFromCenter = rect.size.width/2.0f - self.digitFont.lineHeight/4.0f - paddingFromMargin ;
    NSInteger offset = 0;
    
    //to draw number in circle manner
    int i =self.clockNumber;
    
    NSString *hourNumber = [NSString stringWithFormat:@"%d",i];
    
    CGFloat labelX = center.x + (markingDistanceFromCenter - self.digitFont.lineHeight/2.0f) * cos((M_PI/180)* (i+offset) *distancebetweentheNumber - M_PI/2);
    
    CGFloat labelY = center.y + 1 * (markingDistanceFromCenter - self.digitFont.lineHeight/2.0f) * sin((M_PI/180)*(i+offset) * distancebetweentheNumber - M_PI/2);
    
    UILabel *numberlable=[[UILabel alloc] initWithFrame:CGRectMake(labelX  - self.digitFont.lineHeight/2.0f,labelY - self.digitFont.lineHeight/2.0f,45,45) ];
    numberlable.font=self.digitFont;
    
    numberlable.textColor=[UIColor colorWithRed:159.0/255.0 green:159.0/255.0 blue:159.0/255.0 alpha:1.0];
    numberlable.textAlignment = NSTextAlignmentCenter;
//    numberlable.backgroundColor=[UIColor yellowColor];
    numberlable.text=hourNumber;
    numberlable.layer.cornerRadius = numberlable.frame.size.width/2;
    numberlable.clipsToBounds = YES;
    numberlable.tag = i;
    numberlable.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *numberTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(numberLabelTapped:)];
    [numberlable addGestureRecognizer:numberTap];
    
    numberlable.transform=CGAffineTransformMakeScale(0.5, 0.5);
    [UIView animateWithDuration:0.1 animations:^{
        numberlable.transform=CGAffineTransformIdentity;
    } completion:^(BOOL finished) {
        [self addSubview:numberlable];
        
    }];
    
    NSDate* now = [NSDate date];
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *dateComponents = [gregorian components:(NSCalendarUnitHour  | NSCalendarUnitMinute | NSCalendarUnitSecond) fromDate:now];
    NSInteger hour = [dateComponents hour];
    NSString *am_OR_pm=@"AM";
    [self amButtonAction:self];
    if (hour>12)
    {
        hour=hour%12;
        [self pmButtonAction:self];
        am_OR_pm = @"PM";
    }
    NSString *hourStr;
    if (hour<10) {
        hourStr = [NSString stringWithFormat:@"0%ld",(long)hour];
    }
    else
    {
        hourStr = [NSString stringWithFormat:@"%ld",(long)hour];
    }

    [self.delegate selectedHourValue:hourStr];

    UILabel *numberLabel = (UILabel *)[self viewWithTag:hour];
    numberLabel.textColor = [UIColor whiteColor];
    numberLabel.backgroundColor = [UIColor colorWithRed:168.0/255.0 green:168.0/255.0 blue:170.0/255.0 alpha:1.0];

}

#pragma mark - UITapGesture Methods

-(void)numberLabelTapped :(UITapGestureRecognizer *)tapView
{
    UILabel *numberLabel = (UILabel *)[self viewWithTag:tapView.view.tag];
    
    for (UIView *view in self.subviews) {
        if ([view isKindOfClass:[UILabel class]]) {
            UILabel *label = (UILabel *)view;
            if (label == numberLabel)
            {
                label.textColor = [UIColor whiteColor];
                label.backgroundColor = [UIColor colorWithRed:168.0/255.0 green:168.0/255.0 blue:170.0/255.0 alpha:1.0];
                int hour = [label.text intValue];
                NSString *hourStr;
                if (hour<10) {
                    hourStr = [NSString stringWithFormat:@"0%d",hour];
                }
                else
                {
                    hourStr = [NSString stringWithFormat:@"%d",hour];
                }
                [self.delegate selectedHourValue:hourStr];
            }
            else
            {
                label.textColor=[UIColor colorWithRed:159.0/255.0 green:159.0/255.0 blue:159.0/255.0 alpha:1.0];
                label.backgroundColor=[UIColor clearColor];
                
            }
        }
    }
}

- (IBAction)amButtonAction:(id)sender
{
    [self.amButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.amButton setBackgroundColor:[UIColor colorWithRed:33.0/255.0 green:33.0/255.0 blue:33.0/255.0 alpha:1.0]];
    [self.pmButton setTitleColor:[UIColor colorWithRed:114.0/255.0 green:114.0/255.0 blue:114.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    [self.pmButton setBackgroundColor:[UIColor clearColor]];
    [self.delegate meridiemType:@"AM"];
}
- (IBAction)pmButtonAction:(id)sender
{
    [self.pmButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.pmButton setBackgroundColor:[UIColor colorWithRed:33.0/255.0 green:33.0/255.0 blue:33.0/255.0 alpha:1.0]];
    [self.amButton setTitleColor:[UIColor colorWithRed:114.0/255.0 green:114.0/255.0 blue:114.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    [self.amButton setBackgroundColor:[UIColor clearColor]];
    [self.delegate meridiemType:@"PM"];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
