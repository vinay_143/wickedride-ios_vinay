//
//  WRPromoCodeTableViewCell.m
//  WickedRide
//
//  Created by Ajith kumar on 12/18/15.
//  Copyright © 2015 Inkoniq. All rights reserved.
//

#import "WRPromoCodeTableViewCell.h"
#import "WRConstants.h"

@interface WRPromoCodeTableViewCell ()<UITextFieldDelegate>
{
    
}


@end

@implementation WRPromoCodeTableViewCell

- (void)awakeFromNib {
    // Initialization code
    self.nameLabel.font = MONTSERRAT_BOLD(12.0);
    self.applyButton.layer.cornerRadius = 15.0;
    self.applyButton.clipsToBounds = YES;
    self.promoCodeTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.addtionalCommentTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    
    self.balanceLabel.font = MONTSERRAT_BOLD(12.0);
    self.redeemButton.layer.cornerRadius = 15.0;
    self.redeemButton.clipsToBounds = YES;
    
//    self.promoCodeTextField.delegate = self;
    
    self.redeemTextField.delegate = self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - UIButton/Tap Action

- (IBAction)applyButtonAction:(id)sender
{
    [self endEditing:YES];
    [self.delegate promoCodeApplyButtonTappedWith:self.promoCodeTextField.text];
}

- (IBAction)redeemButtonAction:(id)sender {
    
    [self endEditing:YES];
    
    if(self.redeemTextField.text.length>0)
    {
        if(_isReedem)
        {
            _isReedem = false;
            [sender setTitle:@"Redeem" forState:UIControlStateNormal];
            self.redeemTextField.userInteractionEnabled = true;
        }
        else
        {
            _isReedem = true;
            [sender setTitle:@"Cancel" forState:UIControlStateNormal];
            self.redeemTextField.userInteractionEnabled = false;
        }
        
        [self.delegate redeemButtonTappedWith:self.redeemTextField.text];
    }
    else
    {
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Enter the amount" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
    }
    
}

#pragma mark - UITextFieldDelegate Methods

//-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
//{
//    return YES;
//}
//
//-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
//{
//    [textField resignFirstResponder];
//    return YES;
//}
//
//-(BOOL)textFieldShouldReturn:(UITextField *)textField
//{
//    [textField resignFirstResponder];
//    return YES;
//}


@end
