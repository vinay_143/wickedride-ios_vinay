//
//  WRPromoCodeTableViewCell.h
//  WickedRide
//
//  Created by Ajith kumar on 12/18/15.
//  Copyright © 2015 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol WRPromoCodeTableViewCellDelegate <NSObject>

@optional

-(void)promoCodeApplyButtonTappedWith:(NSString *)promoCode;
-(void)redeemButtonTappedWith:(NSString *)amount;

@end

@interface WRPromoCodeTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UITextField *promoCodeTextField;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@property (weak, nonatomic) IBOutlet UILabel *balanceLabel;
@property (weak, nonatomic) IBOutlet UITextField *availableAmountLabel;
@property (weak, nonatomic) IBOutlet UITextField *redeemTextField;

@property (weak, nonatomic) IBOutlet UIButton *applyButton;

@property (weak, nonatomic) IBOutlet UIButton *redeemButton;

@property (weak, nonatomic) IBOutlet UITextField *addtionalCommentTextField;

@property BOOL isReedem;

@property (nonatomic, weak) id <WRPromoCodeTableViewCellDelegate> delegate;

@end
