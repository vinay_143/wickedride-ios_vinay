//
//  WRRentalView.h
//  WickedRide
//
//  Created by Ajith Kumar on 21/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WRRentalView : UIView
@property (weak, nonatomic) IBOutlet UILabel *daysLabel;
@property (weak, nonatomic) IBOutlet UILabel *currencyLabel;

@end
