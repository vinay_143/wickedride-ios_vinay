//
//  WRBike.h
//  WickedRide
//
//  Created by Ajith Kumar on 22/09/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WRBike : NSObject

@property (nonatomic, strong) NSDictionary *imageDict;
@property (nonatomic, strong) NSString *bikeId;
@property (nonatomic, strong) NSString *bikeName;
@property (nonatomic, assign) BOOL isSoldOut;
@property (nonatomic, strong) NSMutableArray *availableAreaArray;

+(WRBike *)getBikeInformationFrom:(NSDictionary *)resultDict;

@end
