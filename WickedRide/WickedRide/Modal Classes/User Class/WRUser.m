//
//  WRUser.m
//  WickedRide
//
//  Created by Ajith Kumar on 10/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRUser.h"
#import "WRConstants.h"

@implementation WRUser

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.firstName forKey:@"first_name"];
    [aCoder encodeObject:self.lastName forKey:@"last_name"];
    [aCoder encodeObject:self.emailId forKey:@"email"];
    [aCoder encodeObject:self.mobileNumber forKey:@"mobile"];
    [aCoder encodeObject:self.userId forKey:@"id"];
    [aCoder encodeObject:self.dob forKey:@"dob"];
    [aCoder encodeObject:self.token forKey:@"token"];
    [aCoder encodeObject:self.profileImageUrl forKey:@"image"];
    
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self)
    {
        self.firstName = [aDecoder decodeObjectForKey:@"first_name"];
        self.lastName = [aDecoder decodeObjectForKey:@"last_name"];
        self.emailId = [aDecoder decodeObjectForKey:@"email"];
        self.mobileNumber = [aDecoder decodeObjectForKey:@"mobile"];
        self.userId = [aDecoder decodeObjectForKey:@"id"];
        self.dob = [aDecoder decodeObjectForKey:@"dob"];
        self.profileImageUrl = [aDecoder decodeObjectForKey:@"image"];
    }
    return self;
}


+(WRUser *)getLoggedInUserValueWith:(NSDictionary *)resultDict
{
    WRUser *user = [WRUser new];
    
    user.firstName = [NSString stringWithFormat:@"%@",nullCheck([resultDict objectForKey:@"first_name"])];
    user.lastName = [NSString stringWithFormat:@"%@",nullCheck([resultDict objectForKey:@"last_name"])];
    user.emailId = [NSString stringWithFormat:@"%@",nullCheck([resultDict objectForKey:@"email"])];
    NSString *mobile = [NSString stringWithFormat:@"%@",nullCheck([resultDict objectForKey:@"mobile"])];
        
    if ([mobile length]>10) {
        mobile = [mobile substringFromIndex:3];
    }
    user.mobileNumber = mobile;

    user.userId = [NSString stringWithFormat:@"%@",nullCheck([resultDict objectForKey:@"id"])];
    user.dob = [NSString stringWithFormat:@"%@",nullCheck([resultDict objectForKey:@"dob"])];
    if ([[resultDict objectForKey:@"image"] count]) {
        user.profileImageUrl = [NSString stringWithFormat:@"%@",nullCheck([[resultDict objectForKey:@"image"] objectForKey:@"full"])];
    }
    return user;
}

@end
