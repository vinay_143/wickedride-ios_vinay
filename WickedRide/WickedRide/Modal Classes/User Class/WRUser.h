//
//  WRUser.h
//  WickedRide
//
//  Created by Ajith Kumar on 10/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WRUser : NSObject

@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSString *emailId;
@property (nonatomic, strong) NSString *mobileNumber;
@property (nonatomic, strong) NSString *userId;
@property (nonatomic, strong) NSString *token;
@property (nonatomic, strong) NSString *dob;
@property (nonatomic, strong) NSString *profileImageUrl;

+(WRUser *)getLoggedInUserValueWith:(NSDictionary *)resultDict;

@end
