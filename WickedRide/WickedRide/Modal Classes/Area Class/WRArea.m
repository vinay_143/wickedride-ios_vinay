//
//  WRArea.m
//  WickedRide
//
//  Created by Ajith Kumar on 22/09/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRArea.h"

@implementation WRArea

+(WRArea *)getAreaInformationFrom:(NSDictionary *)resultDict
{
    WRArea *areaObj = [WRArea new];
    
    areaObj.areaName = [NSString stringWithFormat:@"%@",[resultDict objectForKey:@"area"]];
    areaObj.area_id = [NSString stringWithFormat:@"%@",[resultDict objectForKey:@"id"]];
    areaObj.latitude = [NSString stringWithFormat:@"%@",[resultDict objectForKey:@"latitude"]];
    areaObj.longitude = [NSString stringWithFormat:@"%@",[resultDict objectForKey:@"longitude"]];
    areaObj.address = [NSString stringWithFormat:@"%@",[resultDict objectForKey:@"address"]];
    if ([resultDict objectForKey:@"price_details"]!=nil) {
        areaObj.effectivePrice = [NSString stringWithFormat:@"%@",[[resultDict objectForKey:@"price_details"] objectForKey:@"effective_price"]];
        areaObj.pricePerDay = [NSString stringWithFormat:@"%@",[[resultDict objectForKey:@"price_details"] objectForKey:@"price_per_day"]];
        areaObj.totalAmount = [NSString stringWithFormat:@"%@",[[resultDict objectForKey:@"price_details"] objectForKey:@"total_price"]];
        areaObj.noOfDay = [NSString stringWithFormat:@"%@",[[resultDict objectForKey:@"price_details"] objectForKey:@"no_of_days"]];
        areaObj.noOfHour = [NSString stringWithFormat:@"%@",[[resultDict objectForKey:@"price_details"] objectForKey:@"no_of_hours"]];
    }
    areaObj.priceWeekdays = [NSString stringWithFormat:@"%@",[[resultDict objectForKey:@"price"] objectAtIndex:0]];
    areaObj.priceWeekEnd = [NSString stringWithFormat:@"%@",[[resultDict objectForKey:@"price"] objectAtIndex:1]];
    
    NSString *bikeId = [NSString stringWithFormat:@"%@",[[resultDict objectForKey:@"bikeAvailabilityStatus"] objectForKey:@"bike_id"]];
    if ([bikeId isEqualToString:@"none"]) {
        areaObj.isBikeAvailabel = NO;
    }
    else
    {
        areaObj.isBikeAvailabel = YES;
    }
    return areaObj;
}
@end
