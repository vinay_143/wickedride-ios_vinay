//
//  WRAccessories.h
//  WickedRide
//
//  Created by Ajith Kumar on 25/11/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WRAccessories : NSObject

@property (nonatomic, strong) NSString *accessoryId;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSDictionary *imageDict;
@property (nonatomic, strong) NSString *accessoryDescription;
@property (nonatomic, strong) NSString *rentalAmount;

+(WRAccessories *)getAccessoriesInformationFrom:(NSDictionary *)resultDict;

@end

