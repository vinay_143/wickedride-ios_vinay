//
//  WRBikation.h
//  WickedRide
//
//  Created by Ajith Kumar on 23/11/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WRBikation : NSObject

@property (nonatomic, strong) NSString *conductorId;
@property (nonatomic, strong) NSString *conductorName;
@property (nonatomic, strong) NSString *contactNumber;
@property (nonatomic, strong) NSString *averageRating;
@property (nonatomic, strong) NSDictionary *conductorImageDict;

@property (nonatomic, strong) NSString *distance;
@property (nonatomic, strong) NSString *duration;
@property (nonatomic, strong) NSString *bikationId;
@property (nonatomic, strong) NSDictionary *imageDict;
@property (nonatomic, strong) NSString *price;
@property (nonatomic, strong) NSString *startCity;
@property (nonatomic, strong) NSString *startDate;
@property (nonatomic, strong) NSString *startTime;
@property (nonatomic, strong) NSString *bikationTitle;


+(WRBikation *)getBikationInformationFrom:(NSDictionary *)resultDict;


@end
