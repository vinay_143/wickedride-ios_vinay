//
//  WRBikationDetails.m
//  WickedRide
//
//  Created by Ajith Kumar on 23/11/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRBikationDetails.h"
#import "WRReview.h"
#import "WRFeatureInfo.h"

@implementation WRBikationDetails


+(WRBikationDetails *)getBikationDetailedInformationFrom:(NSDictionary *)resultDict
{
    WRBikationDetails *bikationDetails = [WRBikationDetails new];
    bikationDetails.availableSlot = [NSString stringWithFormat:@"%@",[resultDict objectForKey:@"available_slots"]];
    bikationDetails.bikationDescription = [NSString stringWithFormat:@"%@",[resultDict objectForKey:@"description"]];
    bikationDetails.distance = [NSString stringWithFormat:@"%@",[resultDict objectForKey:@"distance"]];
    bikationDetails.duration = [NSString stringWithFormat:@"%@",[resultDict objectForKey:@"duration"]];
    bikationDetails.meetingPoint = [NSString stringWithFormat:@"%@",[resultDict objectForKey:@"meeting_point"]];
    bikationDetails.price = [NSString stringWithFormat:@"%@",[resultDict objectForKey:@"price"]];
    bikationDetails.startDate = [NSString stringWithFormat:@"%@",[resultDict objectForKey:@"start_date"]];
    bikationDetails.startTime = [NSString stringWithFormat:@"%@",[resultDict objectForKey:@"start_time"]];
    bikationDetails.totalSlots = [NSString stringWithFormat:@"%@",[resultDict objectForKey:@"total_slots"]];
    
    bikationDetails.exculusionArray = [NSMutableArray array];
    for (NSDictionary *exclusionDict in [resultDict objectForKey:@"exclusions"]) {
        WRFeatureInfo *exclusionObj = [WRFeatureInfo getFeatureInfoOfBikationFrom:exclusionDict];
        [bikationDetails.exculusionArray addObject:exclusionObj];
    }
    
    bikationDetails.inclusionArray = [NSMutableArray array];
    for (NSDictionary *inclusionDict in [resultDict objectForKey:@"inclusions"]) {
        WRFeatureInfo *inclusionObj = [WRFeatureInfo getFeatureInfoOfBikationFrom:inclusionDict];
        [bikationDetails.inclusionArray addObject:inclusionObj];
    }
    
    bikationDetails.reviewArray = [NSMutableArray array];
    for (NSDictionary *reviewDict in [resultDict objectForKey:@"reviews"]) {
        WRReview *reviewObj = [WRReview getReviewInfoOfBikationFrom:reviewDict];
        [bikationDetails.reviewArray addObject:reviewObj];
    }
    bikationDetails.recommendedBikes = [resultDict objectForKey:@"recommended_bikes"];
    bikationDetails.rideRules = [resultDict objectForKey:@"ride_rules"];
    
    return bikationDetails;
}
@end
