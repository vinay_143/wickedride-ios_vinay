//
//  WRReview.m
//  WickedRide
//
//  Created by Ajith Kumar on 23/11/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRReview.h"

@implementation WRReview

+(WRReview *)getReviewInfoOfBikationFrom:(NSDictionary *)resultDict
{
    WRReview *reviewObj = [WRReview new];
    
    reviewObj.date = [NSString stringWithFormat:@"%@",[resultDict objectForKey:@"created_at"]];
    reviewObj.reviewId = [NSString stringWithFormat:@"%@",[resultDict objectForKey:@"id"]];
    reviewObj.rating = [NSString stringWithFormat:@"%@",[resultDict objectForKey:@"rating"]];
    reviewObj.review = [NSString stringWithFormat:@"%@",[resultDict objectForKey:@"review"]];
    reviewObj.reviewerId = [NSString stringWithFormat:@"%@",[[resultDict objectForKey:@"reviewer"] objectForKey:@"id"]];
    reviewObj.reviewerName = [NSString stringWithFormat:@"%@",[[resultDict objectForKey:@"reviewer"] objectForKey:@"name"]];
    reviewObj.reviewerImageDict = [[resultDict objectForKey:@"reviewer"] objectForKey:@"image"];
    reviewObj.reviewTitle = [NSString stringWithFormat:@"%@",[resultDict objectForKey:@"title"]];

    return reviewObj;
}

@end
