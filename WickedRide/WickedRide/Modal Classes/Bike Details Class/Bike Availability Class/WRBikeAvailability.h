//
//  WRBikeAvailability.h
//  WickedRide
//
//  Created by Ajith Kumar on 25/11/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WRBikeAvailability : NSObject
@property (nonatomic, strong) NSString *dateStr;
@property (nonatomic, strong) NSMutableArray *slotsArray;

+(WRBikeAvailability *) getBikeAvailabilityInfoFrom:(NSDictionary *)resultDict;
@end
