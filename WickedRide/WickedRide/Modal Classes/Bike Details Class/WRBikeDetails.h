//
//  WRBikeDetails.h
//  WickedRide
//
//  Created by Ajith Kumar on 27/10/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WRBikeDetails : NSObject

//@property (nonatomic, strong) NSArray *imageArray;
@property (nonatomic, strong) NSString *bikeId;
@property (nonatomic, strong) NSString *bikeName;
@property (nonatomic, strong) NSString *bikeDescription;
@property (nonatomic, strong) NSString *bikeMakeLogo;

@property (nonatomic, strong) NSMutableArray *availabelAreaArray;
@property (nonatomic, strong) NSDictionary *bikeLocationDict;

+(WRBikeDetails *)getBikeDetailsInformationFrom:(NSDictionary *)resultDict;

@end
