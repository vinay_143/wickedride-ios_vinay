//
//  WRBikeDetails.m
//  WickedRide
//
//  Created by Ajith Kumar on 27/10/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRBikeDetails.h"
#import "WRArea.h"

@implementation WRBikeDetails

+(WRBikeDetails *)getBikeDetailsInformationFrom:(NSDictionary *)resultDict
{
    WRBikeDetails *bikeDetails = [WRBikeDetails new];
    bikeDetails.bikeName = [NSString stringWithFormat:@"%@",[resultDict objectForKey:@"name"]];
    bikeDetails.bikeId = [NSString stringWithFormat:@"%@",[resultDict objectForKey:@"id"]];
    bikeDetails.bikeDescription = [NSString stringWithFormat:@"%@",[resultDict objectForKey:@"description"]];
    bikeDetails.bikeMakeLogo = [NSString stringWithFormat:@"%@",[[resultDict objectForKey:@"logo"] objectForKey:@"full"]];
    
    bikeDetails.bikeLocationDict = [resultDict objectForKey:@"bike_location"];
    bikeDetails.availabelAreaArray = [NSMutableArray array];
    
    for (NSDictionary *areaDict in [resultDict objectForKey:@"available_locations"]) {
        WRArea *area = [WRArea getAreaInformationFrom:areaDict];
        [bikeDetails.availabelAreaArray addObject:area];
    }
    return bikeDetails;
}
@end
