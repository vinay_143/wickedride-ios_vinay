//
//  WRBooking.m
//  WickedRide
//
//  Created by Ajith Kumar on 10/12/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRBooking.h"
#import "WRAccessories.h"
#import "WRConstants.h"
#import "NSString+DataValidator.h"

@implementation WRBooking

+(WRBooking *)getBookingInformationFrom:(NSDictionary *)resultDict
{
    WRBooking *booking = [WRBooking new];
    booking.bookingId = [NSString stringWithFormat:@"%@",[resultDict objectForKey:@"booking_id"]];
    NSDictionary *bookingDict = [resultDict objectForKey:@"items"];
    booking.accessoriesArray = [NSMutableArray array];
    for (NSDictionary *dict in [bookingDict objectForKey:@"accessories"]) {
        WRAccessories *accessory = [WRAccessories getAccessoriesInformationFrom:dict];
        [booking.accessoriesArray addObject:accessory];
    }
    booking.bikeObj = [WRBike getBikeInformationFrom:[[bookingDict objectForKey:@"bike"] objectForKey:@"model"]];
    booking.bikeObj.bikeId = [[bookingDict objectForKey:@"bike"] objectForKey:@"id"];
    
    booking.area = [WRArea getAreaInformationFrom:[resultDict objectForKey:@"pickup_area"]];
    
    booking.pickUpDate = [NSString stringWithFormat:@"%@",nullCheck([resultDict objectForKey:@"pickup_date"])];
    booking.pickUpTime = [NSString stringWithFormat:@"%@",nullCheck([resultDict objectForKey:@"pickup_time"])];
    
    NSString *priceString = nullCheck([resultDict objectForKey:@"price"]);
//    if (![priceString isEmptyString]) {
//        
//        priceString = [[[resultDict objectForKey:@"price"] componentsSeparatedByCharactersInSet:
//                        [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
//                       componentsJoinedByString:@""];
//    }
    booking.price = nullCheck(priceString);
    booking.profileImage = [NSString stringWithFormat:@"%@",nullCheck([resultDict objectForKey:@"profile_image"])];
    booking.profileImageId = [NSString stringWithFormat:@"%@",nullCheck([resultDict objectForKey:@"profile_image_id"])];
    
    return booking;
}

@end
