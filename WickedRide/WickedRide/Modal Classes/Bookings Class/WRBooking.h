//
//  WRBooking.h
//  WickedRide
//
//  Created by Ajith Kumar on 10/12/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WRBike.h"
#import "WRArea.h"

@interface WRBooking : NSObject

@property (nonatomic, strong) NSString *bookingId;
@property (nonatomic, strong) NSMutableArray *accessoriesArray;
@property (nonatomic, strong) WRBike *bikeObj;
@property (nonatomic, strong) WRArea *area;

@property (nonatomic, strong) NSString *pickUpDate;
@property (nonatomic, strong) NSString *pickUpTime;
@property (nonatomic, strong) NSString *price;
@property (nonatomic, strong) NSString *profileImage;
@property (nonatomic, strong) NSString *profileImageId;


+(WRBooking *)getBookingInformationFrom:(NSDictionary *)resultDict;

@end
