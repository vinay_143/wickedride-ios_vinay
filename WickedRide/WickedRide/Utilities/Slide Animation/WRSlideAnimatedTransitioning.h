
#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIViewControllerTransitioning.h>

@interface WRSlideAnimatedTransitioning : NSObject <UIViewControllerAnimatedTransitioning>

@end
