//
//  WRCustomLoader.h
//  WickedRide
//
//  Created by Ajith Kumar on 28/10/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface WRCustomLoader : NSObject

@property(nonatomic,strong)UIImageView *loaderImageView;

+(WRCustomLoader *)sharedInstance;

@end
