//
//  AppDelegate.m
//  WickedRide
//
//  Created by Ajith Kumar on 10/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRAppDelegate.h"
#import <RESideMenu/RESideMenu.h>
#import "UIImage+colors.h"
#import "WRBikeListingViewController.h"
#import <IQKeyboardManager.h>
#import "QGSdk.h"
#import <UserNotifications/UserNotifications.h>

//Define macros for checking iOS version
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)



#define kVersionButtonTitle @"Update"
#define kVersionUpdateMessage @"A new version of the app  is available and is required to continue, please click below to update to the latest version."

@interface WRAppDelegate () <LeftMenuViewControllerDelegate,UNUserNotificationCenterDelegate>
@property (nonatomic, strong) RESideMenu *sideMenuViewController;

@end

@implementation WRAppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [[UINavigationBar appearance] setShadowImage:[UIImage imageWithColor:[UIColor whiteColor]]];
    [self setupSideMenuViewControllers];
    
    self.window.backgroundColor = [UIColor whiteColor];
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
   // [self loadForceUpdateView];
    
    [IQKeyboardManager sharedManager].enable = true;
    
    
    QGSdk *qgsdk = [QGSdk getSharedInstance];
    
   [qgsdk onStart:kQGraphId withAppGroup:@"group.com.wickedRide.app.group1" setDevProfile:NO];
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"10.0")) {
        UNAuthorizationOptions options = (UNAuthorizationOptions) (UNAuthorizationOptionAlert | UNAuthorizationOptionBadge | UNAuthorizationOptionSound | UNAuthorizationOptionCarPlay);
        
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        
        NSSet *categories = [NSSet setWithObjects:[qgsdk getQGSliderPushActionCategoryWithNextButtonTitle:nil withOpenAppButtonTitle:nil], nil];
        [center setNotificationCategories:categories];
        
        [center requestAuthorizationWithOptions:options completionHandler:^(BOOL granted, NSError *error){
            NSLog(@"GRANTED: %i, Error: %@", granted, error);
        }];
    } else if (SYSTEM_VERSION_LESS_THAN(@"10.0")) {
        UIUserNotificationType types = UIUserNotificationTypeAlert | UIUserNotificationTypeSound |
        UIUserNotificationTypeBadge;
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:types
                                                                                 categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    }
    
    
    
    //replace <your app id> with the one you received from QGraph
    [[QGSdk getSharedInstance] onStart:kQGraphId setDevProfile:NO];
  
    return YES;
}

#pragma mark Notification

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    NSLog(@"My token is: %@", deviceToken);
    [[QGSdk getSharedInstance] setToken:deviceToken];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    [[QGSdk getSharedInstance] application:application didReceiveRemoteNotification:userInfo];
}

//used for silent push handling
//pass completion handler UIBackgroundFetchResult accordingly
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(nonnull NSDictionary *)userInfo fetchCompletionHandler:(nonnull void (^)(UIBackgroundFetchResult))completionHandler {
    [[QGSdk getSharedInstance] application:application didReceiveRemoteNotification:userInfo];
    completionHandler(UIBackgroundFetchResultNoData);
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler {
    [[QGSdk getSharedInstance] userNotificationCenter:center willPresentNotification:notification];
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    UNNotificationPresentationOptions option = UNNotificationPresentationOptionBadge | UNNotificationPresentationOptionSound | UNNotificationPresentationOptionAlert;
    
    completionHandler(option);
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)())completionHandler {
    
    
    NSDictionary *userInfo = response.notification.request.content.userInfo;
    if ([userInfo objectForKey:@"deepLink"]) {
        NSURL *url = [NSURL URLWithString:userInfo[@"deepLink"]];
        dispatch_async(dispatch_get_main_queue(), ^{
            [[UIApplication sharedApplication] openURL:url];
        });
    }
    
    
    [[QGSdk getSharedInstance] userNotificationCenter:center didReceiveNotificationResponse:response];
    completionHandler();
    
    
}


- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString *,id> *)options {
    NSLog(@"deeplink");
    return true;
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    NSLog(@"Failed to get token, error: %@", error.localizedDescription);
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
  //  [self loadForceUpdateView];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [[NSNotificationCenter defaultCenter] postNotificationName:kBecomeActivePlayVideo object:nil];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    return YES;
}


#pragma mark - Set up RESideMenu

- (void)setupSideMenuViewControllers
{
    
   /* UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    // Root View
    if ([[NSUserDefaults standardUserDefaults] valueForKey:IS_HIDE_LOCATION_VIEW]) {
        WRBikeListingViewController *bikeListVC = [mainStoryboard instantiateViewControllerWithIdentifier:kBikeListingViewControllerID];
        self.homeViewController = (WRHomeViewController *)bikeListVC;
    }
    else
    {
        WRPickLocationViewController *homeViewController = [mainStoryboard instantiateViewControllerWithIdentifier:kPickLocationViewControllerID];
        self.homeViewController = (WRHomeViewController *) homeViewController;
    }*/
    
    // Storyboard
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    // Root View
    WRHomeViewController *homeViewController = [mainStoryboard instantiateViewControllerWithIdentifier:kHomeViewControllerID];
    self.homeViewController = homeViewController;
    
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:homeViewController];
    
    [navigationController.navigationBar setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
    
    // Left Menu View
    WRLeftMenuViewController *leftMenuViewController = [mainStoryboard instantiateViewControllerWithIdentifier:kLeftMenuViewControllerID];
    leftMenuViewController.delegate = self;
    self.leftMenuViewController = leftMenuViewController;
    
    //Side Menu allocation
    self.sideMenuViewController = [[RESideMenu alloc] initWithContentViewController:navigationController
                                                             leftMenuViewController:leftMenuViewController
                                                            rightMenuViewController:nil];
    // Make side menu as root view controller
    self.window.rootViewController = self.sideMenuViewController;
    self.sideMenuViewController.parallaxEnabled = NO;
}

#pragma mark Left Menu Button Action

- (void)showLeftMenuViewController
{
    [self.leftMenuViewController reloadTableViewWithDetails];
    [self.sideMenuViewController presentLeftMenuViewController];
    self.sideMenuViewController.panGestureEnabled = NO;
}


#pragma mark - LeftMenuViewControllerDelegate Methods

-(void)leftMenuFirstSectionTappedWithSelectedItem:(WRLeftMenuFirstSectionType)firstSectionSelectedType
{
    [self.sideMenuViewController hideMenuViewController];
    [self.homeViewController loadFirstSectionViewsWithItem:firstSectionSelectedType];
}


-(void)leftMenuProfileViewTapped
{
    [self.sideMenuViewController hideMenuViewController];
    [self.homeViewController leftMenuProfileViewTapped];
}

#pragma mark - Custom Methods

-(void)loadForceUpdateView
{
    BOOL isUpdateAvailable =[self isUpdateAvailable];
    if (isUpdateAvailable) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:kVersionUpdateMessage delegate:self cancelButtonTitle:kVersionButtonTitle otherButtonTitles:nil, nil];
        alertView.tag = 101;
        [alertView show];
        
    }
}

-(BOOL) isUpdateAvailable{
    NSDictionary* infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString* appID = infoDictionary[@"CFBundleIdentifier"];
    NSString *urlString = [NSString stringWithFormat:@"https://itunes.apple.com/lookup?bundleId=%@",appID];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    NSURLResponse *response;
    NSError *error;
    NSData *data = [NSURLConnection  sendSynchronousRequest:request returningResponse: &response error: &error];
    NSError *e = nil;
    NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &e];
    
    NSString *versionInAppStore = [[[jsonDict objectForKey:@"results"] objectAtIndex:0] objectForKey:@"version"];
    
    NSString *localAppVersion = infoDictionary[@"CFBundleShortVersionString"];
    
    if ([versionInAppStore compare:localAppVersion options:NSNumericSearch] == NSOrderedDescending) {
        // currentVersion is lower than the version
        return YES;
    }
    return NO;
}




#pragma mark - UIAlertViewDelegate Methods

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 101) {
        NSString *url = [NSString stringWithFormat:@"itms-apps://itunes.apple.com/app/id%@",kAppId];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
    }
}
    //#pragma mark Load it as Root View Controller
//
//-(void)loadBikeListingVCToRootViewController
//{
//    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    WRBikeListingViewController *bikeListingVC = [mainStoryboard instantiateViewControllerWithIdentifier:kBikeListingViewControllerID];
//    UINavigationController *navCtr = [[UINavigationController alloc] initWithRootViewController:bikeListingVC];
//    self.window.rootViewController = navCtr;
//}

@end
