//
//  AppDelegate.h
//  WickedRide
//
//  Created by Ajith Kumar on 10/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WRLeftMenuViewController.h"
#import "WRHomeViewController.h"


@interface WRAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (nonatomic, strong) WRLeftMenuViewController *leftMenuViewController;
@property (nonatomic, strong) WRHomeViewController *homeViewController;

- (void)showLeftMenuViewController;

//-(void)loadBikeListingVCToRootViewController;

@end

