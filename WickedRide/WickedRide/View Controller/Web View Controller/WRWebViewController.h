//
//  WRWebViewController.h
//  WickedRide
//
//  Created by Ajith Kumar on 23/09/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WRBaseViewController.h"

@interface WRWebViewController : WRBaseViewController

@property (weak, nonatomic) IBOutlet UIWebView *webView;

@property (nonatomic, assign) BOOL isFromReview;
@property (nonatomic, assign) BOOL isFromTariff;
@property (nonatomic, assign) BOOL isFromFAQ;
@property (nonatomic, assign) BOOL isFromAboutUs;
@property (nonatomic, assign) BOOL isFromContactUs;
@property (nonatomic, assign) BOOL isFromThankYouVC;
@property (nonatomic, assign) BOOL isFromSummarryTermsVC;
@end
