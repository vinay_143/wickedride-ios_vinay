//
//  WRWebViewController.m
//  WickedRide
//
//  Created by Ajith Kumar on 23/09/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRWebViewController.h"

@interface WRWebViewController ()<UIWebViewDelegate>

@end

@implementation WRWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    self.navigationController.navigationBarHidden = YES;
    
    if (self.isFromSummarryTermsVC) {
        [self loadMBProgressCustomLoaderView];
        [self loadRespectiveWebViewWithUrl:@"https://www.wickedride.com/termsandconditions"];
        [self showNavigationViewWithBackButtonWithTitle:@"Terms & Condition"];
    }
    else
    {
        if (self.userManager.htmlUrlDict == nil) {
            [self loadMBProgressCustomLoaderView];
            [self.networkManager startGETRequestWithAPI:aGetAllHtmlUrl andParameters:nil withSuccess:^(NSDictionary *responseDictionary) {
                self.userManager.htmlUrlDict = [[responseDictionary objectForKey:@"result"] objectForKey:@"data"];
                [self loadWebViewWithWebService];
            } andFailure:^(NSString *errorMessage) {
                [self hideMBProgressCutomLoader];
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:errorMessage delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alertView show];
                
            }];
        }
        else
        {
            [self loadMBProgressCustomLoaderView];
            [self loadWebViewWithWebService];
        }
    }
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
}

-(void)loadWebViewWithWebService
{
    if (self.isFromThankYouVC) {
        [self loadRespectiveWebViewWithUrl:[self.userManager.htmlUrlDict objectForKey:@"faq"]];
        [self showNavigationViewWithBackButtonWithTitle:@"FAQ"];
    }
    else
    {
        if (self.isFromReview) {
            [self loadRespectiveWebViewWithUrl:[self.userManager.htmlUrlDict objectForKey:@"review"]];
            [self showNavigationMenuForFilterScreenWithTitle:@"REVIEW"];
        }
        else if (self.isFromTariff)
        {
            [self loadRespectiveWebViewWithUrl:[self.userManager.htmlUrlDict objectForKey:@"tariff"]];
            [self showNavigationMenuForFilterScreenWithTitle:@"TARIFF"];
        }
        else if (self.isFromFAQ)
        {
            [self loadRespectiveWebViewWithUrl:[self.userManager.htmlUrlDict objectForKey:@"faq"]];
            [self showNavigationMenuForFilterScreenWithTitle:@"FAQ"];
        }
        else if (self.isFromAboutUs)
        {
            [self loadRespectiveWebViewWithUrl:[self.userManager.htmlUrlDict objectForKey:@"about"]];
            [self showNavigationMenuForFilterScreenWithTitle:@"ABOUT"];
        }
        else if (self.isFromContactUs)
        {
            [self loadRespectiveWebViewWithUrl:[self.userManager.htmlUrlDict objectForKey:@"contact"]];
            [self showNavigationMenuForFilterScreenWithTitle:@"CONTACT US"];
        }
    }

}

#pragma mark - Web View Custom Methods

-(void)loadRespectiveWebViewWithUrl:(NSString *)htmlUrl
{
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:htmlUrl]]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)webViewDidStartLoad:(UIWebView *)webView
{
}

-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self hideMBProgressCutomLoader];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
