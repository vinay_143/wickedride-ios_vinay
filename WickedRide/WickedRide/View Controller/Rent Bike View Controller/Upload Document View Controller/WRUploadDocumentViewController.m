//
//  WRUploadDocumentViewController.m
//  WickedRide
//
//  Created by Apple on 25/12/16.
//  Copyright © 2016 Inkoniq. All rights reserved.
//

#import "WRUploadDocumentViewController.h"
#import "WRUploadDocumentTableViewCell.h"
#import "WRGalleryViewController.h"
#import "WRUploadImageViewController.h"

#define kDrivingLicense @"DRIVING LICENSE"
#define kAddressProof @"ADDRESS PROOF"
#define kTitleStr @"Upload Documents"

@interface WRUploadDocumentViewController ()<UITableViewDelegate, UITableViewDataSource, UIActionSheetDelegate, WRUploadImageViewControllerDelegate, WRGalleryViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *documentsTableView;
@property (nonatomic, strong) NSDictionary *licenseDataArray;
@property (nonatomic, strong) NSDictionary *addressDataArray;

@end

@implementation WRUploadDocumentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self showNavigationMenuForFilterScreenWithTitle:kTitleStr];
    self.documentsTableView.hidden = YES;
    [self callWebServiceForNumberOfDocuments];
}

#pragma mark - Call we service for documents count

-(void)callWebServiceForNumberOfDocuments
{
    [self loadMBProgressCustomLoaderView];
    [self.networkManager.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@",[[NSUserDefaults standardUserDefaults] objectForKey:kToken]] forHTTPHeaderField:@"Authorization"];
    
    [self.networkManager POST:aGetUploadedDocuments parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self hideMBProgressCutomLoader];
        int status = (int)[[[responseObject objectForKey:@"result"] objectForKey:@"status_code"] integerValue];
        if (status == 200) {
            self.licenseDataArray = [[[responseObject objectForKey:@"result"] objectForKey:@"data"] objectForKey:@"dl"];
            self.addressDataArray = [[[responseObject objectForKey:@"result"] objectForKey:@"data"] objectForKey:@"id"];
            self.documentsTableView.hidden = NO;
            [self.documentsTableView reloadData];
        }
        else
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Something went Wrong. Please try later" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self hideMBProgressCutomLoader];
        if (![error.localizedDescription isEqualToString:kErrorMessage]) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Something went Wrong. Please try later" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
        else
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:error.localizedDescription delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
    }];
    
}

#pragma mark - UITableViewDelegate, UITableViewDataSource Methods

-(NSInteger )numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

-(NSInteger )tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    WRUploadDocumentTableViewCell *cell = (WRUploadDocumentTableViewCell *)[tableView dequeueReusableCellWithIdentifier:kUploadDocumentTableViewCellID];
    
    if (indexPath.section == 0) {
        cell.headerLabel.text = kDrivingLicense;
        cell.numberOfDocsLabel.text = [[self.licenseDataArray objectForKey:@"status"] uppercaseString];
    }
    else
    {
        cell.headerLabel.text = kAddressProof;
        cell.numberOfDocsLabel.text = [[self.addressDataArray objectForKey:@"status"] uppercaseString];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

-(CGFloat )tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        
        if ([[self.licenseDataArray objectForKey:@"images"] count]) {
            UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Action" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
                                          @"Upload images",
                                          @"View Photos",
                                          nil];
            actionSheet.tag = 111;
            [actionSheet showInView:self.view];
        }
        else
        {
            UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Action" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
                                          @"Upload images",
                                          nil];
            actionSheet.tag = 112;
            [actionSheet showInView:self.view];
        }
        
    }
    else
    {
        if ([[self.addressDataArray objectForKey:@"images"] count]) {
            UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Action" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
                                          @"Upload images",
                                          @"View Photos",
                                          nil];
            actionSheet.tag = 222;
            [actionSheet showInView:self.view];
        }
        else
        {
            UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Action" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
                                          @"Upload images",
                                          nil];
            actionSheet.tag = 223;
            [actionSheet showInView:self.view];
        }
    }
}

#pragma mark - UIActionSheetDelegate Methods

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag == 111) {
        switch (buttonIndex) {
            case 0:
                [self loadUploadImageViewControllerWithType:YES];
                break;
            case 1:
                [self loadViewImagesViewControllerWithType:YES];
                break;
                
            default:
                break;
        }
    }
    else if (actionSheet.tag == 112) {
        switch (buttonIndex) {
            case 0:
                [self loadUploadImageViewControllerWithType:YES];
                break;
                
            default:
                break;
        }
    }
    else if(actionSheet.tag == 222)
    {
        switch (buttonIndex) {
            case 0:
                [self loadUploadImageViewControllerWithType:NO];
                break;
            case 1:
                [self loadViewImagesViewControllerWithType:NO];
                break;
                
            default:
                break;
        }
    }
    else
    {
        switch (buttonIndex) {
            case 0:
                [self loadUploadImageViewControllerWithType:NO];
                break;
            default:
                break;
        }
    }
}

#pragma mark - Action Sheet custom Method

-(void)loadUploadImageViewControllerWithType:(BOOL )isFromLicense
{
    WRUploadImageViewController *uploadImageVC = [self.storyboard instantiateViewControllerWithIdentifier:kUploadImageViewControllerID];
    uploadImageVC.isFromLicence = isFromLicense;
    uploadImageVC.delegate = self;
    [self.navigationController pushViewController:uploadImageVC animated:YES];
}

-(void)loadViewImagesViewControllerWithType:(BOOL )isFromLicense
{
    WRGalleryViewController *galleryVC = [self.storyboard instantiateViewControllerWithIdentifier:kGalleryViewControllerID];
    if (isFromLicense) {
        galleryVC.imageArray = [self.licenseDataArray objectForKey:@"images"];
    }
    else
    {
        galleryVC.imageArray = [self.addressDataArray objectForKey:@"images"];
    }
    galleryVC.delegate = self;
    [self.navigationController pushViewController:galleryVC animated:YES];
}


#pragma mark - WRUploadImageViewControllerDelegate Methods

-(void)imageUploadedSuccessfully
{
    [self callWebServiceForNumberOfDocuments];
}

#pragma mark - WRGalleryViewControllerDelegate Methods

-(void)imageDeletedSuccessfully
{
    [self callWebServiceForNumberOfDocuments];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
