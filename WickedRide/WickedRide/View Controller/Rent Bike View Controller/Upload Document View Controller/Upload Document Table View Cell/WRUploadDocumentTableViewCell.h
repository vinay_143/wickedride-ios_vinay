//
//  WRUploadDocumentTableViewCell.h
//  WickedRide
//
//  Created by Apple on 25/12/16.
//  Copyright © 2016 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WRUploadDocumentTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *headerLabel;
@property (weak, nonatomic) IBOutlet UILabel *numberOfDocsLabel;

@end
