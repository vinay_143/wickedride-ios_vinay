//
//  WRUploadImageViewController.m
//  WickedRide
//
//  Created by Apple on 25/12/16.
//  Copyright © 2016 Inkoniq. All rights reserved.
//

#import "WRUploadImageViewController.h"

@interface WRUploadImageViewController ()<UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (nonatomic, strong) UIButton *addImageButton;
@property (nonatomic, strong) UIScrollView *imageScrollView;
@property (nonatomic, strong) UIImageView *docsImageView;
@property (nonatomic) int imageTag;
@property (nonatomic, strong) NSMutableArray *docsImageArray;
@property (nonatomic, strong) UIButton *uploadButton;
@end

@implementation WRUploadImageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.imageTag = 1;
    // Do any additional setup after loading the view.
    [self showNavigationViewWithBackButtonWithTitle:@"Upload License"];
    self.docsImageArray = [NSMutableArray array];
    [self loadImageScrollViewWithImages];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

#pragma mark - Custom Loading View

-(void)loadImageScrollViewWithImages
{
    self.docsImageView = [[UIImageView alloc] initWithFrame:CGRectMake(30, 10, self.view.frame.size.width-60, self.view.frame.size.height -190)];
    self.docsImageView.contentMode = UIViewContentModeScaleAspectFit;
    
     self.uploadButton = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width-45, self.view.frame.size.height -230, 40, 40)];
    [self.uploadButton setImage:[UIImage imageNamed:@"upload"] forState:UIControlStateNormal];
    [self.uploadButton addTarget:self action:@selector(uploadImageToServer) forControlEvents:UIControlEventTouchDown];
    self.uploadButton.hidden = YES;
    [self.view addSubview:self.uploadButton];
    [self.docsImageView setImage:[UIImage imageNamed:@"place_holder_upload"]];
    [self.view addSubview:self.docsImageView];
    self.imageScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0,self.view.frame.size.height -185 , self.view.frame.size.width, 120)];
    [self loadAddImageView];
    [self.view addSubview:self.imageScrollView];
    [self.view bringSubviewToFront:self.uploadButton];
}

-(void)loadAddImageView
{
    self.addImageButton = [[UIButton alloc] initWithFrame:CGRectMake(15, 10, 100, 100)];
    [self.addImageButton setImage:[UIImage imageNamed:@"upload_plus"] forState:UIControlStateNormal];
    [self.addImageButton addTarget:self action:@selector(addImageButtonAction) forControlEvents:UIControlEventTouchDown];
    [self.imageScrollView addSubview:self.addImageButton];
}


#pragma mark - UIButton/Tap Action

-(void)addImageButtonAction
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Upload Document" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
                                  @"Camera",
                                  @"Gallery",
                                  nil];
    [actionSheet showInView:self.view];
}

-(void)imageViewTappedWith:(UITapGestureRecognizer *)tap
{
    UIView* view = tap.view;
    CGPoint loc = [tap locationInView:view];
    UIView* subview = [view hitTest:loc withEvent:nil];
    
    UIImageView *imageView = (UIImageView *)subview;
    [self.docsImageView setImage:imageView.image];
}

#pragma mark - UIActionSheetDelegate Method

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
            [self loadImageFromCamera];
            break;
        case 1:
            [self loadImageFromGallery];
            break;
            
        default:
            break;
    }
}

-(void)loadImageFromCamera
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePicker.mediaTypes = @[(NSString *) kUTTypeImage];
        imagePicker.allowsEditing = NO;
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
    
}

-(void)loadImageFromGallery
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
        imagePicker.sourceType =  UIImagePickerControllerSourceTypePhotoLibrary;
        imagePicker.mediaTypes = @[(NSString *) kUTTypeImage];
        imagePicker.allowsEditing = NO;
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
}

-(void)uploadImageToServer
{
    NSString *typeOfDoc = (self.isFromLicence)?@"DL":@"Address Proof";
    NSDictionary *params = @{@"doc_type":typeOfDoc,
//                             @"log_request":@"true"
                             };
    
    [self.networkManager.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@",[[NSUserDefaults standardUserDefaults] objectForKey:kToken]] forHTTPHeaderField:@"Authorization"];
 
    [self loadMBProgressCustomLoaderView];
    AFHTTPRequestOperation *operation = [self.networkManager POST:aUploadUserDocuments parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        for(int i=0; i<[self.docsImageArray count];i++)
        {
            [formData appendPartWithFileData:[self.docsImageArray objectAtIndex:i] name:[NSString stringWithFormat:@"image%d",i+1] fileName:@"image.png" mimeType:@"image/png"];
        }

    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self hideMBProgressCutomLoader];
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Image uploaded" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
        [self.delegate imageUploadedSuccessfully];
        [self.navigationController popViewControllerAnimated:YES];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self hideMBProgressCutomLoader];
        
        NSData *charlieSendData = operation.responseData;
        NSString *charlieSendString = [[NSString alloc] initWithData:charlieSendData encoding:NSUTF8StringEncoding];
        NSLog(@"%@",charlieSendString);
        

        if (![error.localizedDescription isEqualToString:kErrorMessage]) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Something went Wrong. Please try later" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
        else
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:error.localizedDescription delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
    }];
    [operation start];
}

#pragma mark - UINavigationControllerDelegate / UIImagePickerControllerDelegate Methods

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *mediaType = info[UIImagePickerControllerMediaType];
    [self dismissViewControllerAnimated:YES completion:nil];
    if ([mediaType isEqualToString:(NSString *) kUTTypeImage]) {
        UIImage *image = info[UIImagePickerControllerOriginalImage];
        [self loadImageViewWithSelectedImage:image];
        [self.docsImageView setImage:image];
        NSData *imageData = UIImageJPEGRepresentation(image, 1.0);
        [self.docsImageArray addObject:imageData];
    }
}

-(void)loadImageViewWithSelectedImage:(UIImage *)image
{
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:self.addImageButton.frame];
    imageView.userInteractionEnabled = YES;
//    imageView.contentMode = UIViewContentModeScaleAspectFit;
    UITapGestureRecognizer *imageTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageViewTappedWith:)];
    [imageView addGestureRecognizer:imageTap];
    [self.addImageButton changedXAxisTo:imageView.frame.origin.x + 115];
    [imageView setImage:image];
    self.imageTag++;
    [self.imageScrollView addSubview:imageView];
    self.imageScrollView.contentSize = CGSizeMake(self.addImageButton.frame.origin.x+115, 0);
    if (self.uploadButton.hidden == YES) {
        self.uploadButton.hidden = NO;
    }
}

/*
 #pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
