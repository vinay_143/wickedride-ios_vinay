//
//  WRGalleryViewController.h
//  WickedRide
//
//  Created by Apple on 25/12/16.
//  Copyright © 2016 Inkoniq. All rights reserved.
//

#import "WRBaseViewController.h"

@protocol WRGalleryViewControllerDelegate <NSObject>

@optional

-(void)imageDeletedSuccessfully;

@end

@interface WRGalleryViewController : WRBaseViewController

@property(nonatomic, strong) NSArray *imageArray;

@property (nonatomic, strong) id <WRGalleryViewControllerDelegate> delegate;

@end
