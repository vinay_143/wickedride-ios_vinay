//
//  WRClockViewController.h
//  WickedRide
//
//  Created by Ajith Kumar on 09/09/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRBaseViewController.h"
#import "WRBikeAvailability.h"
#import "WRArea.h"
#import "WRBike.h"
#import "WRTimeSlot.h"


@protocol WRClockViewControllerDelegate <NSObject>

@optional

-(void)selectedStartTime:(NSString *)timeStr;
-(void )selectedEndDateAndTimeDict:(NSDictionary *)endDateDict;
@end

@interface WRClockViewController : WRBaseViewController

@property (nonatomic, strong) NSDate *selectedDate;
@property (nonatomic, strong) NSDate *pickUpSelectedDate;
@property (nonatomic, strong) NSString *pickUpSelectedStr;

@property (nonatomic, strong) WRBikeAvailability *bikeAvailability;
@property (nonatomic, strong) WRArea *selectedArea;
@property (nonatomic, strong) WRBike *selectedBike;
@property (nonatomic, assign) BOOL isPickUpDateSelected;
@property (nonatomic, assign) BOOL isFromBikeListing;
@property (nonatomic, assign) BOOL isFromBikeAvailability;

@property (nonatomic, strong) NSString *lastDateStr;
@property (nonatomic, strong) WRTimeSlot *lastTimeSlot;
@property (nonatomic, strong) NSDictionary *paramsDict;

@property (nonatomic, weak) id <WRClockViewControllerDelegate> delegate;
@end
