//
//  WRCalenederViewController.m
//  MediaPlayerDemo
//
//  Created by Ajith Kumar on 08/09/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRCalenederViewController.h"
#import "RSDFDatePickerView.h"
#import "WRClockViewController.h"

@interface WRCalenederViewController ()<RSDFDatePickerViewDataSource,RSDFDatePickerViewDelegate, WRClockViewControllerDelegate>

@property (nonatomic,strong) NSString *pickUpDateStr;
@property (nonatomic,strong) NSString *dropDateStr;
@property (nonatomic, strong) NSDate *selecteDate;
@property (nonatomic, strong) WRArea *selectedArea;

@end

@implementation WRCalenederViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationController.navigationBarHidden = YES;
    RSDFDatePickerView *datePickerView = [[RSDFDatePickerView alloc] initWithFrame:CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height-64)];
    datePickerView.delegate = self;
    datePickerView.dataSource = self;
    NSDate *selectedDate;
    if (self.isPickUpDateSelected)
    {
        [self loadCustomNavigationViewWithTitle:@"PICK UP DATE"];
//        if (self.isFromBikeListing) {
//            NSString *hourStr = [self.pickDateStartTime substringToIndex:2];
//            int hour = [hourStr intValue]+1;
//            if (hour>=21) {
//                selectedDate = [self.pickDate dateByAddingTimeInterval:60*60*24*0];
//            }
//            else
//            {
//                selectedDate = [self.pickDate dateByAddingTimeInterval:60*60*24*-1];
//            }
//            
//        }
//        else
        {
            NSDate* now = [NSDate date];
            NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
            NSDateComponents *dateComponents = [gregorian components:(NSCalendarUnitHour  | NSCalendarUnitMinute | NSCalendarUnitSecond) fromDate:now];
            NSInteger hour = [dateComponents hour];
            
            int daysToAdd = -1;
            if (hour>22)
            {
                daysToAdd = 0;
            }
            if (self.pickDate)
            {
                selectedDate = [[NSDate date] dateByAddingTimeInterval:60*60*24*daysToAdd];
            }
            else
            {
                selectedDate = [[NSDate date] dateByAddingTimeInterval:60*60*24*daysToAdd];
            }
        }
    }
    else
    {
        [self loadCustomNavigationViewWithTitle:@"DROP DATE"];
        NSString *hourStr = [self.pickDateStartTime substringToIndex:2];
        int hour = [hourStr intValue]+1;
        if (hour>22) {
            selectedDate = [self.pickDate dateByAddingTimeInterval:60*60*24*0];
        }
        else
        {
            selectedDate = [self.pickDate dateByAddingTimeInterval:60*60*24*-1];
        }
    }
    datePickerView.today = selectedDate;
    [self.view addSubview:datePickerView];
}

#pragma mark - get area for API

-(void)callWebServiceToGetAllArea
{
    [self loadMBProgressCustomLoaderView];
    NSString *city_id = [NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults] valueForKey:kSelectedLocation] objectForKey:@"id"]];
    [self.networkManager startGETRequestWithAPI:aGetAllAreas(city_id) andParameters:nil withSuccess:^(NSDictionary *responseDictionary) {
        self.userManager.areaArray = [[responseDictionary objectForKey:@"result"] objectForKey:@"data"];
        if (self.selectedArea == nil) {
            self.selectedArea = [WRArea getAreaInformationFrom:[self.userManager.areaArray objectAtIndex:0]];
        }
        [self hideMBProgressCutomLoader];
        
    } andFailure:^(NSString *errorMessage) {
        [self hideMBProgressCutomLoader];
    }];
}


- (BOOL)datePickerView:(RSDFDatePickerView *)view shouldHighlightDate:(NSDate *)date
{
    return YES;
}

// Returns YES if the date should be selected or NO if it should not.
- (BOOL)datePickerView:(RSDFDatePickerView *)view shouldSelectDate:(NSDate *)date
{
    return YES;
}

// Prints out the selected date.
- (void)datePickerView:(RSDFDatePickerView *)view didSelectDate:(NSDate *)date
{
    self.selecteDate = [date dateByAddingTimeInterval:60*60*24];
    
    if (self.isPickUpDateSelected) {
        if (self.selecteDate == nil) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Please select the pick up date" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
        else
        {
            WRClockViewController *clockVC = [self.storyboard instantiateViewControllerWithIdentifier:kClockViewControllerID];
            clockVC.selectedDate = self.selecteDate;
            clockVC.isFromBikeListing = self.isFromBikeListing;
            clockVC.selectedArea = self.selectedArea;
            
            clockVC.delegate = self;
            [self.navigationController pushViewController:clockVC animated:YES];
        }
    }
    else
    {
        if (self.selecteDate == nil) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Please select the drop date" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
        else
        {
            WRClockViewController *clockVC = [self.storyboard instantiateViewControllerWithIdentifier:kClockViewControllerID];
            clockVC.selectedDate = self.selecteDate;
            clockVC.pickUpSelectedDate = self.pickDate;
            clockVC.pickUpSelectedStr = self.pickDateStartTime;
            clockVC.isFromBikeListing = self.isFromBikeListing;
            clockVC.selectedArea = self.selectedArea;
            clockVC.isPickUpDateSelected = YES;
            clockVC.delegate = self;
            [self.navigationController pushViewController:clockVC animated:YES];
        }
        
    }

}


- (BOOL)datePickerView:(RSDFDatePickerView *)view shouldMarkDate:(NSDate *)date
{
    // The date is an `NSDate` object without time components.
    // So, we need to use dates without time components.
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay;
    NSDateComponents *todayComponents = [calendar components:unitFlags fromDate:[NSDate date]];
    NSDate *today = [calendar dateFromComponents:todayComponents];
    
    return [date isEqual:today];
}

// Returns the color of the default mark image for the specified date.
- (UIColor *)datePickerView:(RSDFDatePickerView *)view markImageColorForDate:(NSDate *)date
{
    if (arc4random() % 2 == 0) {
        return [UIColor grayColor];
    } else {
        return [UIColor greenColor];
    }
}

// Returns the mark image for the specified date.
- (UIImage *)datePickerView:(RSDFDatePickerView *)view markImageForDate:(NSDate *)date
{
    if (arc4random() % 2 == 0) {
        return [UIImage imageNamed:@"img_gray_mark"];
    } else {
        return [UIImage imageNamed:@"img_green_mark"];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UIButton Action Methods


- (IBAction)closeButtonActions:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)nextButtonAction:(id)sender
{
    
    if (self.isPickUpDateSelected) {
        if (self.selecteDate == nil) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Please select the pick up date" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
        else
        {
            WRClockViewController *clockVC = [self.storyboard instantiateViewControllerWithIdentifier:kClockViewControllerID];
            clockVC.selectedDate = self.selecteDate;
            clockVC.delegate = self;
            [self.navigationController pushViewController:clockVC animated:YES];
        }
    }
    else
    {
        if (self.selecteDate == nil) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Please select the drop date" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
        else
        {
            WRClockViewController *clockVC = [self.storyboard instantiateViewControllerWithIdentifier:kClockViewControllerID];
            clockVC.selectedDate = self.selecteDate;
            clockVC.pickUpSelectedDate = self.pickDate;
            clockVC.pickUpSelectedStr = self.pickDateStartTime;
            clockVC.isPickUpDateSelected = YES;
            clockVC.delegate = self;
            [self.navigationController pushViewController:clockVC animated:YES];
        }
        
    }
}

#pragma mark - WRClockViewControllerDelegate Method

-(void)selectedStartTime:(NSString *)timeStr
{
    if (self.paramsDict == nil) {
        self.paramsDict = [NSMutableDictionary dictionary];
    }
    [self.paramsDict setObject:timeStr forKey:@"end_time"];
    [self.paramsDict setObject:[[self.selecteDate description] convertDateIntoString] forKey:@"end_date"];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationCalendarDateSelected object:nil userInfo:self.paramsDict];
//    if (self.isFromBikeListing) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationCalendarDateSelectedFromBikeListing object:nil userInfo:self.paramsDict];
//    }
    //
//    if (self.isPickUpDateSelected) {
//        [self.delegate nextButtonTappedWithPickUpDate:self.selecteDate andTheTime:timeStr];
//    }
//    else
//    {
//        [self.delegate nextButtonTappedWithDropDate:self.selecteDate andTheTime:timeStr];
//    }
}

@end
