//
//  WRClockViewController.m
//  WickedRide
//
//  Created by Ajith Kumar on 09/09/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRClockViewController.h"
#import "WRClockView.h"
#import "WRClockTableViewCell.h"
#import "WRCalenederViewController.h"
#import "WRTimeSlot.h"
#import "WRBikeAvailabilityCalendarViewController.h"

@interface WRClockViewController ()<UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UILabel *selectedDateLabel;
@property (nonatomic, strong) WRClockView *clockView;
@property (weak, nonatomic) IBOutlet UITableView *timeSlotTableView;
@property (nonatomic, strong) NSMutableArray *timeSlotArray;
@property (nonatomic, strong) NSMutableArray *selectedCellArray;

@end

@implementation WRClockViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.selectedCellArray = [NSMutableArray array];
    self.navigationController.navigationBarHidden = YES;
    [self loadCustomNavigationViewWithTitle:@"PICK TIME SLOT"];
    // Do any additional setup after loading the view.

    if (!self.isFromBikeAvailability) {
        self.selectedDateLabel.text = [[self.selectedDate description] convertDateIntoStringForClock];
        NSDate* now = [NSDate date];
        NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        NSDateComponents *dateComponents = [gregorian components:(NSCalendarUnitHour  | NSCalendarUnitMinute | NSCalendarUnitSecond) fromDate:now];
        NSInteger hour = [dateComponents hour]+1;

        

        
        if ([[[self.selectedDate description] convertDateIntoString] isEqualToString:[[self.pickUpSelectedDate description] convertDateIntoString]]||[[[self.selectedDate description] convertDateIntoString] isEqualToString:[[[NSDate date] description] convertDateIntoString]]) {
            if (self.pickUpSelectedStr==nil ) {
//                [self prepareTimeSlotArrayWithValue:hour];
                [self callWebServiceToGetFromWorkingHourDetails];
                
            }
            else
            {
                NSLog(@"%@",self.pickUpSelectedStr);
                NSString *hourStr = [self.pickUpSelectedStr substringToIndex:2];
                hour = [hourStr intValue];
//                [self prepareTimeSlotArrayWithValue:hour];
                [self callWebServiceToGetToWorkingHourDetailsWithSelectedHour:hour];
            }
        }
        else
        {
//            NSCalendar *calendar = [NSCalendar currentCalendar];
//            NSDateComponents *components = [calendar components:NSWeekdayCalendarUnit fromDate:self.selectedDate];
//            NSUInteger weekdayOfDate = [components weekday];
//            
//            if (weekdayOfDate == 1 || weekdayOfDate == 2) {
//                //the date falls somewhere on the first or last days of the week
//                hour = 18;
//            }
//            else
//            {
//                hour = 9;
//            }
//            
//            [self prepareTimeSlotArrayWithValue:hour];
            if (self.pickUpSelectedStr==nil ) {
                //                [self prepareTimeSlotArrayWithValue:hour];
                [self callWebServiceToGetFromWorkingHourDetails];
                
            }
            else
            {
//                NSLog(@"%@",self.pickUpSelectedStr);
//                NSString *hourStr = [self.pickUpSelectedStr substringToIndex:2];
                hour = 0;//[hourStr intValue]+1;
                //                [self prepareTimeSlotArrayWithValue:hour];
                [self callWebServiceToGetToWorkingHourDetailsWithSelectedHour:hour];
            }

        }

    }
    else
    {
        self.selectedDateLabel.text = [self.bikeAvailability.dateStr convertDateIntoRequiredBikationString];

        if (!self.isPickUpDateSelected) {
            [self.timeSlotTableView reloadData];
        }
        else
        {
            self.timeSlotTableView.hidden = YES;
            [self callWebServiceToGetToWorkingHourDetails];
        }
    }

}

-(void)callWebServiceToGetFromWorkingHourDetails
{
    [self loadMBProgressCustomLoaderView];
    NSString *date = [[self.selectedDate description] convertDateIntoString];
    NSString *city_id = [NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults] valueForKey:kSelectedLocation] objectForKey:@"id"]];

    
//    NSDictionary *params = @{@"date":date,
//                             @"area":(self.selectedArea ==nil)?@"1":self.selectedArea.area_id,
//                             @"city":city_id};
    
    self.timeSlotArray = [NSMutableArray array];
    NSString *urlStr = [NSString stringWithFormat:@"https://www.wickedride.com/api/from-working-hours?city_id=%@&date=%@",city_id,date];
    [self.networkManager startGETRequestWithAPI:urlStr andParameters:nil withSuccess:^(NSDictionary *responseDictionary) {
        [self hideMBProgressCutomLoader];
        
        NSArray *slotsArray = [[[responseDictionary objectForKey:@"result"] objectForKey:@"data"] objectForKey:@"slots"];
        if (slotsArray.count) {
            for (NSDictionary *slotsDict in slotsArray) {
                WRTimeSlot *slot = [WRTimeSlot getTimeSlotInfoFrom:slotsDict];
                [self.timeSlotArray addObject:slot];
            }
            self.timeSlotTableView.hidden = NO;
            [self.timeSlotTableView reloadData];
        }
        else
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Sorry! This location is closed on the date selected." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
        
    } andFailure:^(NSString *errorMessage) {
        [self hideMBProgressCutomLoader];
        if (![errorMessage isEqualToString:kErrorMessage]) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Something went Wrong. Please try later" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
        else
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:errorMessage delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }

    }];
}


-(void)callWebServiceToGetToWorkingHourDetailsWithSelectedHour:(int)hour
{
    [self loadMBProgressCustomLoaderView];
    self.timeSlotArray = [NSMutableArray array];
    NSString *date = [[self.selectedDate description] convertDateIntoString];
    NSString *city_id = [NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults] valueForKey:kSelectedLocation] objectForKey:@"id"]];
    
//    NSDictionary *params = @{@"date":date,
//                             @"city":city_id};
    NSString *urlStr = [NSString stringWithFormat:@"https://www.wickedride.com/api/to-working-hours?city_id=%@&date=%@",city_id,date];

    [self.networkManager startGETRequestWithAPI:urlStr andParameters:nil withSuccess:^(NSDictionary *responseDictionary) {
        [self hideMBProgressCutomLoader];
        NSArray *slotsArray = [[[responseDictionary objectForKey:@"result"] objectForKey:@"data"] objectForKey:@"slots"];
        if (slotsArray.count) {
                for (NSDictionary *slotsDict in slotsArray) {
                    WRTimeSlot *slot = [WRTimeSlot getTimeSlotInfoFrom:slotsDict];
                    if (hour == 0) {
                        [self.timeSlotArray addObject:slot];
                    }
                    else
                    {
                        NSString *hourStr = [slot.startTime substringToIndex:2];
                        int dropHour = [hourStr intValue];
                        if (hour<dropHour) {
                            [self.timeSlotArray addObject:slot];
                        }
                    }
                }
                self.timeSlotTableView.hidden = NO;
                [self.timeSlotTableView reloadData];

        }
        else
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Sorry! This location is closed on the date selected." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
        
        
    } andFailure:^(NSString *errorMessage) {
        [self hideMBProgressCutomLoader];
        if (![errorMessage isEqualToString:kErrorMessage]) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Something went Wrong. Please try later" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
        else
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:errorMessage delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }

        
    }];
    
}

-(void)callWebServiceToGetToWorkingHourDetails
{
    [self loadMBProgressCustomLoaderView];
    self.timeSlotArray = [NSMutableArray array];
    NSString *date = self.bikeAvailability.dateStr;
    NSString *city_id = [NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults] valueForKey:kSelectedLocation] objectForKey:@"id"]];
    
//    NSDictionary *params = @{@"date":date,
//                             @"area":self.selectedArea.area_id,
//                             @"city":city_id};
    NSString *urlStr = [NSString stringWithFormat:@"https://www.wickedride.com/api/to-working-hours?city_id=%@&date=%@",city_id,date];
    
    [self.networkManager startGETRequestWithAPI:urlStr andParameters:nil withSuccess:^(NSDictionary *responseDictionary) {
        [self hideMBProgressCutomLoader];
        NSArray *slotsArray = [[[responseDictionary objectForKey:@"result"] objectForKey:@"data"] objectForKey:@"slots"];
        if (slotsArray.count) {
            [self prepareTimeSlotArrayForBikeAvailabiltyWith:slotsArray];
            
        }
        else
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Sorry! This location is closed on the date selected." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
        
        
    } andFailure:^(NSString *errorMessage) {
        [self hideMBProgressCutomLoader];
        if (![errorMessage isEqualToString:kErrorMessage]) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Something went Wrong. Please try later" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
        else
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:errorMessage delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
        
        
    }];
}
-(void)prepareTimeSlotArrayForBikeAvailabiltyWith:(NSArray *)resultArray
{
    
    if ([self.bikeAvailability.dateStr isEqualToString:self.lastDateStr]) {
        int lastHour = [[self.lastTimeSlot.startTime substringToIndex:2] intValue];
        
        for (NSDictionary *slotsDict in resultArray) {
            WRTimeSlot *slot = [WRTimeSlot getTimeSlotInfoFrom:slotsDict];
            int slotHour = [[slot.startTime substringToIndex:2] intValue];
            if (slotHour<=lastHour) {
                [self.timeSlotArray addObject:slot];
            }
        }
    }
    else
    {
        
        if ([self.bikeAvailability.dateStr isEqualToString:[self.paramsDict objectForKey:@"start_date"]]) {
            
            int lastHour = [[[self.paramsDict objectForKey:@"start_time"] substringToIndex:2] intValue];
            
            for (NSDictionary *slotsDict in resultArray) {
                WRTimeSlot *slot = [WRTimeSlot getTimeSlotInfoFrom:slotsDict];
                int slotHour = [[slot.startTime substringToIndex:2] intValue];
                if (slotHour>lastHour) {
                    [self.timeSlotArray addObject:slot];
                }
            }
        }
        else
        {
            for (NSDictionary *slotsDict in resultArray) {
                WRTimeSlot *slot = [WRTimeSlot getTimeSlotInfoFrom:slotsDict];
                [self.timeSlotArray addObject:slot];
            }
        }
    }
    self.bikeAvailability.slotsArray = self.timeSlotArray;
    self.timeSlotTableView.hidden = NO;
    [self.timeSlotTableView reloadData];
}

-(void)prepareTimeSlotArrayWithValue:(NSInteger )hour
{
    NSString *hourStr;
    self.timeSlotArray = [NSMutableArray array];
    
    for (int i =0; i<(23-hour); i++) {
        if ((hour+i)<10) {
            hourStr = [NSString stringWithFormat:@"0%ld",(long)hour+i];
        }
        else
        {
            hourStr = [NSString stringWithFormat:@"%ld",(long)hour+i];
        }
        NSString *slots = [NSString stringWithFormat:@"%@:00",hourStr];
        [self.timeSlotArray addObject:slots];
    }
    [self.timeSlotTableView reloadData];
}

#pragma mark - UITableViewDataSource, UITableViewDelegate Methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (!self.isFromBikeAvailability) {
        return self.timeSlotArray.count;
    }
    
    return self.bikeAvailability.slotsArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    WRClockTableViewCell *cell = (WRClockTableViewCell *)[tableView dequeueReusableCellWithIdentifier:kClockTableViewCellID];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (!self.isFromBikeAvailability) {
        WRTimeSlot *timeSlot = [self.timeSlotArray objectAtIndex:indexPath.row];
        cell.timeLabel.text = timeSlot.startTime;

    }
    else
    {
        WRTimeSlot *timeSlot = [self.bikeAvailability.slotsArray objectAtIndex:indexPath.row];
        cell.timeLabel.text = timeSlot.startTime;

    }
    if (self.selectedCellArray.count) {
        NSIndexPath *selectedIndexPath = [self.selectedCellArray objectAtIndex:0];
        if (selectedIndexPath.row==indexPath.row)
        {
            [cell.checkButton setImage:[UIImage imageNamed:@"agree"] forState:UIControlStateNormal];
        }
        else
        {
            [cell.checkButton setImage:[UIImage imageNamed:@"circle"] forState:UIControlStateNormal];
        }
    }
    else
    {
        [cell.checkButton setImage:[UIImage imageNamed:@"circle"] forState:UIControlStateNormal];
    }

    return cell;
}

-(CGFloat )tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.0;
}

-(void )tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (self.selectedCellArray.count) {
        if ([self.selectedCellArray objectAtIndex:0]==indexPath) {
            [self.selectedCellArray removeObject:indexPath];
        }
        else
        {
            [self.selectedCellArray replaceObjectAtIndex:0 withObject:indexPath];
        }
    }
    else
    {
        [self.selectedCellArray addObject:indexPath];
    }
    
    [self.timeSlotTableView reloadData];
    
    NSIndexPath *selectedIndexPath = [self.selectedCellArray objectAtIndex:0];
    
    NSString *timeStr;
    if (self.bikeAvailability == nil) {
        WRTimeSlot *timeSlot = [self.timeSlotArray objectAtIndex:selectedIndexPath.row];
        timeStr = timeSlot.startTime;
    }
    else
    {
        WRTimeSlot *timeSlot = [self.bikeAvailability.slotsArray objectAtIndex:selectedIndexPath.row];
        timeStr = timeSlot.startTime;
    }
    if (self.isFromBikeAvailability) {
        if (!self.isPickUpDateSelected) {
            NSDictionary *params = @{@"start_date":self.bikeAvailability.dateStr,
                                     @"start_time":timeStr};
            
            WRBikeAvailabilityCalendarViewController *availabilityVC = [self.storyboard instantiateViewControllerWithIdentifier:kBikeAvailabilityCalendarViewControllerID];
            availabilityVC.selectedArea = self.selectedArea;
            availabilityVC.selectedBike = self.selectedBike;
            //        availabilityVC.isPickDateSelected = YES;
            availabilityVC.paramsDict = [NSMutableDictionary dictionaryWithDictionary:params];
            [self.navigationController pushViewController:availabilityVC animated:YES];
        }
        else
        {
            NSDictionary *params = @{@"end_date":self.bikeAvailability.dateStr,
                                     @"end_time":timeStr};
            
            [self.delegate selectedEndDateAndTimeDict:params];
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }
    else
    {
        if (!self.isPickUpDateSelected) {
            NSString *dateStr = [[self.selectedDate description] convertDateIntoString];
            NSDictionary *params = @{@"start_date":dateStr,
                                     @"start_time":timeStr};
            WRCalenederViewController *calenderVC = [self.storyboard instantiateViewControllerWithIdentifier:@"WRCalenederViewController"];
            calenderVC.pickDate = self.selectedDate;
            calenderVC.pickDateStartTime = timeStr;
            calenderVC.isFromBikeListing = self.isFromBikeListing;
            calenderVC.paramsDict = [NSMutableDictionary dictionaryWithDictionary:params];
            [self.navigationController pushViewController:calenderVC animated:YES];
        }
        else
        {
            [self.delegate selectedStartTime:timeStr];
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

-(void)addContainerScrollView
{
//    self.containerView.translatesAutoresizingMaskIntoConstraints = YES;
//    UIScrollView *mainScrollView = [[UIScrollView alloc] initWithFrame:self.view.frame];
//    self.containerView.frame = CGRectMake(0, 0, self.view.frame.size.width, 568.0);
//    mainScrollView.contentSize = CGSizeMake(320,  568.0);
//    [mainScrollView addSubview:self.containerView];
//    [self.view addSubview:mainScrollView];
//    [self.view bringSubviewToFront:self.customNavView];
}




#pragma mark - UIButton actions

- (IBAction)doneButtonActions:(id)sender
{
    if (!self.selectedCellArray.count) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Please select a time slot" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
    }
    else
    {
        NSIndexPath *selectedIndexPath = [self.selectedCellArray objectAtIndex:0];
        
        NSString *timeStr;
        if (self.bikeAvailability == nil) {
            timeStr = [self.timeSlotArray objectAtIndex:selectedIndexPath.row];
        }
        else
        {
            WRTimeSlot *timeSlot = [self.bikeAvailability.slotsArray objectAtIndex:selectedIndexPath.row];
            timeStr = timeSlot.startTime;
        }
        if (self.isFromBikeAvailability) {
            if (!self.isPickUpDateSelected) {
                NSDictionary *params = @{@"start_date":self.bikeAvailability.dateStr,
                                         @"start_time":timeStr};
                
                WRBikeAvailabilityCalendarViewController *availabilityVC = [self.storyboard instantiateViewControllerWithIdentifier:kBikeAvailabilityCalendarViewControllerID];
                availabilityVC.selectedArea = self.selectedArea;
                availabilityVC.selectedBike = self.selectedBike;
                //        availabilityVC.isPickDateSelected = YES;
                availabilityVC.paramsDict = [NSMutableDictionary dictionaryWithDictionary:params];
                [self.navigationController pushViewController:availabilityVC animated:YES];
            }
            else
            {
                NSDictionary *params = @{@"end_date":self.bikeAvailability.dateStr,
                                         @"end_time":timeStr};
                
                [self.delegate selectedEndDateAndTimeDict:params];
                [self dismissViewControllerAnimated:YES completion:nil];
            }
        }
        else
        {
            if (!self.isPickUpDateSelected) {
                NSString *dateStr = [[self.selectedDate description] convertDateIntoString];
                NSDictionary *params = @{@"start_date":dateStr,
                                         @"start_time":timeStr};
                WRCalenederViewController *calenderVC = [self.storyboard instantiateViewControllerWithIdentifier:@"WRCalenederViewController"];
                calenderVC.pickDate = self.selectedDate;
                calenderVC.pickDateStartTime = timeStr;
                calenderVC.paramsDict = [NSMutableDictionary dictionaryWithDictionary:params];
                [self.navigationController pushViewController:calenderVC animated:YES];
            }
            else
            {
                [self.delegate selectedStartTime:timeStr];
                [self dismissViewControllerAnimated:YES completion:nil];
            }
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
