//
//  WRTimeSlot.h
//  WickedRide
//
//  Created by Ajith Kumar on 26/11/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WRTimeSlot : NSObject
@property (nonatomic, strong) NSString *endTime;
@property (nonatomic, strong) NSString *startTime;
@property (nonatomic, strong) NSString *timeSlotId;
@property (nonatomic, assign) BOOL timeSlotStatus;

+(WRTimeSlot *)getTimeSlotInfoFrom:(NSDictionary *)resultDict;

@end
