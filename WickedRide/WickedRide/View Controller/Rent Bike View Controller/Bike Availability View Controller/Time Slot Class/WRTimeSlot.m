//
//  WRTimeSlot.m
//  WickedRide
//
//  Created by Ajith Kumar on 26/11/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRTimeSlot.h"

@implementation WRTimeSlot

+(WRTimeSlot *)getTimeSlotInfoFrom:(NSDictionary *)resultDict
{
    WRTimeSlot *timeSlot = [WRTimeSlot new];
    
    timeSlot.endTime = [NSString stringWithFormat:@"%@",[[resultDict objectForKey:@"end_time"] substringToIndex:5]];
    NSString *startTime;
    NSInteger time = [[[resultDict objectForKey:@"start_time"] substringToIndex:1] integerValue];
    if (time >2) {
        startTime = [NSString stringWithFormat:@"0%d:00",time];
    }
    else
    {
        startTime = [[resultDict objectForKey:@"start_time"] substringToIndex:5];
    }
    timeSlot.startTime = [NSString stringWithFormat:@"%@",startTime];
    timeSlot.timeSlotId = [NSString stringWithFormat:@"%@",[resultDict objectForKey:@"id"]];
    timeSlot.timeSlotStatus = [[resultDict objectForKey:@"status"] boolValue];

    return timeSlot;
}
@end
