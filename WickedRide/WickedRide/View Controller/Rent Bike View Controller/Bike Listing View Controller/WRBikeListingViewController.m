//
//  WRBikeListingViewController.m
//  WickedRide
//
//  Created by Ajith Kumar on 11/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRBikeListingViewController.h"
#import "WRBikeListingCollectionViewCell.h"
#import "WRBikeDetailsViewController.h"
#import "WRRentBikeFilterViewController.h"
#import "WRBikeCalenderCollectionViewCell.h"
#import "WRBike.h"
#import "WRCalenederViewController.h"

#define convertToString(value) [NSString stringWithFormat:@"%@",value]

@interface WRBikeListingViewController ()<WRCalenederViewControllerDelegate,WRRentBikeFilterViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *bikeListingCollectionView;
@property (weak, nonatomic) IBOutlet UIView *pickUpView;
@property (weak, nonatomic) IBOutlet UIView *dropView;
@property (weak, nonatomic) IBOutlet WRLabel *pickUpDateLabel;
@property (weak, nonatomic) IBOutlet WRLabel *dropDateLabel;

@property (nonatomic, strong) NSDate *pickUpSelectedDate;
@property (nonatomic, strong) NSMutableArray *bikeListingArray;
@property (nonatomic, strong) NSString *pickUpSelectedStr;
@property (nonatomic, strong) NSMutableDictionary *paramsDict;
@property (nonatomic, strong) NSMutableArray *makeIdArray;
@property (nonatomic, strong) WRArea *selectedArea;
@end

@implementation WRBikeListingViewController

-(void)viewDidLoad
{
    [super viewDidLoad];
    if (![[NSUserDefaults standardUserDefaults] boolForKey:IS_HIDE_LOCATION_VIEW])
    {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:IS_HIDE_LOCATION_VIEW];
    }
    [self addGesturToViews];
    [self callWebServiceToGetBikeListingDetails:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(callWebServiceToGetBikeListingFromLeftMenu) name:kNotificationDropDownValueSelected object:nil];
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(calendarSelectedDate:) name:kNotificationCalendarDateSelected object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(calendarSelectedDateFromBikeListing:) name:kNotificationCalendarDateSelectedFromBikeListing object:nil];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [self showNavigationMenuForFilterScreenWithTitle:@"YOUR RIDES" andIsFromBikeList:YES];
}


#pragma mark - Custom Methods

-(void)addGesturToViews
{
    UITapGestureRecognizer *pickUpTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pickUpCalenderButtonAction:)];
    [self.pickUpView addGestureRecognizer:pickUpTap];
    
    UITapGestureRecognizer *dropTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dropDownCalenderButtonAction:)];
    [self.dropView addGestureRecognizer:dropTap];
}


#pragma mark Web Service Methods

-(void)callWebServiceToGetBikeListingDetails:(NSMutableDictionary *)params
{
    self.bikeListingCollectionView.scrollsToTop = YES;
    NSString *city_id = [NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults] valueForKey:kSelectedLocation] objectForKey:@"id"]];
    self.bikeListingArray = [NSMutableArray array];
    self.bikeListingCollectionView.hidden = YES;
    [self loadMBProgressCustomLoaderView];
    
    [self.networkManager startGETRequestWithAPI:aGetBikeList(city_id) andParameters:params withSuccess:^(NSDictionary *responseDictionary) {

        for (NSDictionary *bikeDict in [[responseDictionary objectForKey:@"result"] objectForKey:@"data"]) {
            WRBike *bikeObj = [WRBike getBikeInformationFrom:bikeDict];
            [self.bikeListingArray addObject:bikeObj];

        }
        self.bikeListingCollectionView.hidden = NO;
        [self.bikeListingCollectionView reloadData];
        [self hideMBProgressCutomLoader];
    } andFailure:^(NSString *errorMessage) {
        [self hideMBProgressCutomLoader];
        if (![errorMessage isEqualToString:kErrorMessage]) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Something went Wrong. Please try later" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
        else
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:errorMessage delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
        
    }];
    
}

-(void)callWebServiceToGetBikeListingFromLeftMenu
{
    self.bikeListingCollectionView.scrollsToTop = YES;
    NSString *city_id = [NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults] valueForKey:kSelectedLocation] objectForKey:@"id"]];
    self.bikeListingArray = [NSMutableArray array];
    self.bikeListingCollectionView.hidden = YES;
    [self loadMBProgressCustomLoaderView];
    [self.networkManager startGETRequestWithAPI:aGetBikeList(city_id) andParameters:nil withSuccess:^(NSDictionary *responseDictionary) {
        
        for (NSDictionary *bikeDict in [[responseDictionary objectForKey:@"result"] objectForKey:@"data"]) {
            WRBike *bikeObj = [WRBike getBikeInformationFrom:bikeDict];
            [self.bikeListingArray addObject:bikeObj];
            
        }
        self.bikeListingCollectionView.hidden = NO;
        [self.bikeListingCollectionView reloadData];
        [self hideMBProgressCutomLoader];
    } andFailure:^(NSString *errorMessage) {
        [self hideMBProgressCutomLoader];
        if (![errorMessage isEqualToString:kErrorMessage]) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Something went Wrong. Please try later" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
        else
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:errorMessage delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
        
    }];
    
}


#pragma mark NSDate custom methods

-(NSString *)returnDateWithProperFormat:(NSDate *)date
{
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MMM-YYYY"];
    
    return [dateFormatter stringFromDate:date];
}


#pragma mark - UICollectionViewDataSource/UICollectionViewDelegate Methods

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView;
{
    return 1;
}

-(NSInteger )collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
//    if (section == 0) {
//        return 1;
//    }
    return self.bikeListingArray.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
//    if (indexPath.section == 0) {
//        WRBikeCalenderCollectionViewCell *cell = (WRBikeCalenderCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:kBikeCalenderCollectionViewCellID forIndexPath:indexPath];
//        return cell;
//    }
    WRBikeListingCollectionViewCell *cell = (WRBikeListingCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:kbikeListingCollectionViewCellID forIndexPath:indexPath];
    [cell configureCellWithBikeListing:[self.bikeListingArray objectAtIndex:indexPath.row]];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
//    if (indexPath.section == 0) {
//        return CGSizeMake(collectionView.frame.size.width-40, 40.0);
//    }
    float aspectRatio = 245.0/335.0; //cell height/width;
    WRBike *bikeObj = [self.bikeListingArray objectAtIndex:indexPath.row];
    
    if (bikeObj.isSoldOut) {
        return CGSizeMake(collectionView.frame.size.width-40, 25 + (collectionView.frame.size.width-40)*aspectRatio);
    }
    return CGSizeMake(collectionView.frame.size.width-40, (collectionView.frame.size.width-40)*aspectRatio);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    WRBike *bikeObj = [self.bikeListingArray objectAtIndex:indexPath.row];
    if (!bikeObj.isSoldOut) {
        [self.paramsDict addEntriesFromDictionary:[[NSUserDefaults standardUserDefaults] objectForKey:kDateDictionary]];
        WRBikeDetailsViewController *bikeDetailVC = [self.storyboard instantiateViewControllerWithIdentifier:kBikeDetailsViewControllerID];
        bikeDetailVC.selectedBikeObj = [self.bikeListingArray objectAtIndex:indexPath.row];
        bikeDetailVC.paramsDict = self.paramsDict;
        [self.navigationController pushViewController:bikeDetailVC animated:YES];
    }
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -- YSLTransitionAnimatorDataSource

- (UIImageView *)pushTransitionImageView
{
    WRBikeListingCollectionViewCell *cell = (WRBikeListingCollectionViewCell *)[self.bikeListingCollectionView cellForItemAtIndexPath:[[self.bikeListingCollectionView indexPathsForSelectedItems] firstObject]];
    return cell.bikeImageView;
}

- (UIImageView *)popTransitionImageView
{
    return nil;
}

#pragma mark - UIButton / Tap Actions

- (IBAction)filterNextBarBtnTapped:(id)sender
{
//    if (![self.pickUpDateLabel.text isEqualToString:@"Pick Up Date"]) {
//        NSString *startDate = self.pickUpDateLabel.text;
//        NSString* result = [self.pickUpDateLabel.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
//
//        NSString *startTime = self.pickUpDateLabel.text;
//        NSString *endDate = self.dropDateLabel.text;
//        NSString *endTime = self.dropDateLabel.text;
//        [self.paramsDict setObject:startDate forKey:@"start_date"];
//        [self.paramsDict setObject:startTime forKey:@"start_time"];
//        [self.paramsDict setObject:endDate forKey:@"end_date"];
//        [self.paramsDict setObject:endTime forKey:@"end_time"];
//    }
    [self.paramsDict addEntriesFromDictionary:[[NSUserDefaults standardUserDefaults] objectForKey:kDateDictionary]];
    WRRentBikeFilterViewController *filterVC = [self.storyboard instantiateViewControllerWithIdentifier:kRentBikeFilterViewControllerID];
    filterVC.delegate = self;
    filterVC.paramsDict = self.paramsDict;
    filterVC.makeIdArray = self.makeIdArray;
    filterVC.selectedArea = self.selectedArea;
    [self.navigationController pushViewController:filterVC animated:YES];
}

- (IBAction)pickUpCalenderButtonAction:(id)sender
{
    WRCalenederViewController *calenderVC = [self.storyboard instantiateViewControllerWithIdentifier:@"WRCalenederViewController"];
    UINavigationController *navController=[[UINavigationController alloc] initWithRootViewController:calenderVC];
    if (self.pickUpSelectedDate != nil) {
        calenderVC.pickDate = self.pickUpSelectedDate;
        calenderVC.pickDateStartTime = self.pickUpSelectedStr;
    }
    calenderVC.isFromBikeListing = YES;
    calenderVC.isPickUpDateSelected = YES;
    calenderVC.delegate = self;
    [self.navigationController presentViewController:navController animated:YES completion:nil];
}

- (IBAction)dropDownCalenderButtonAction:(id)sender
{
    if ([self.paramsDict objectForKey:@"start_date"]==nil) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Please select pick up date" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
    }
    else
    {
        WRCalenederViewController *calenderVC = [self.storyboard instantiateViewControllerWithIdentifier:@"WRCalenederViewController"];
        UINavigationController *navController=[[UINavigationController alloc] initWithRootViewController:calenderVC];
        calenderVC.pickDate = [self returnDateFromString:[self.paramsDict objectForKey:@"start_date"]];
        calenderVC.pickDateStartTime = [self.paramsDict objectForKey:@"start_time"];
        calenderVC.delegate = self;
        calenderVC.isFromBikeListing = YES;
        [self.navigationController presentViewController:navController animated:YES completion:nil];
    }
}


//-(void)calendarSelectedDate:(NSNotification *)notificationObj
//{
//    NSDictionary *params = notificationObj.userInfo;
//    if (self.paramsDict == nil) {
//        self.paramsDict = [NSMutableDictionary dictionary];
//    }
//    [self.paramsDict addEntriesFromDictionary:params];
//    [[NSUserDefaults standardUserDefaults] setObject:self.paramsDict forKey:kDateDictionary];
//
//    self.pickUpDateLabel.text = [NSString stringWithFormat:@"%@\n%@",[[self.paramsDict objectForKey:@"start_date"] convertDateIntoRequiredBikationString],[self.paramsDict objectForKey:@"start_time"]];
//    self.dropDateLabel.text = [NSString stringWithFormat:@"%@\n%@",[[self.paramsDict objectForKey:@"end_date"] convertDateIntoRequiredBikationString],[self.paramsDict objectForKey:@"end_time"]];
//    [self callWebServiceToGetBikeListingDetails:self.paramsDict];
//}

-(void)calendarSelectedDateFromBikeListing:(NSNotification *)notificationObj
{
    NSDictionary *params = notificationObj.userInfo;
    if (self.paramsDict == nil) {
        self.paramsDict = [NSMutableDictionary dictionary];
    }
    [self.paramsDict addEntriesFromDictionary:params];
    [[NSUserDefaults standardUserDefaults] setObject:self.paramsDict forKey:kDateDictionary];
    
    self.pickUpDateLabel.text = [NSString stringWithFormat:@"%@\n%@",[[self.paramsDict objectForKey:@"start_date"] convertDateIntoRequiredBikationString],[self.paramsDict objectForKey:@"start_time"]];
    self.dropDateLabel.text = [NSString stringWithFormat:@"%@\n%@",[[self.paramsDict objectForKey:@"end_date"] convertDateIntoRequiredBikationString],[self.paramsDict objectForKey:@"end_time"]];
    [self callWebServiceToGetBikeListingDetails:self.paramsDict];
}

#pragma mark - WRCalenederViewControllerDelegate Methods

-(void)nextButtonTappedWithPickUpDate:(NSDate *)selectedDate andTheTime:(NSString *)timeStr
{
    self.dropDateLabel.text = @"Drop Date";
    self.pickUpSelectedDate = selectedDate;
    self.pickUpSelectedStr = timeStr;
    NSString *pickDateTimeString = [self.pickUpSelectedStr substringToIndex:8];
    self.pickUpDateLabel.text = [NSString stringWithFormat:@"%@\n%@",[[[selectedDate description] convertDateIntoString] convertDateIntoRequiredBikationString], pickDateTimeString];
    
}

-(void)nextButtonTappedWithDropDate:(NSDate *)selectedDate andTheTime:(NSString *)timeStr
{
    NSString *dropDateTimeString = [timeStr substringToIndex:8];
    NSString *pickDateTimeString = [self.pickUpSelectedStr substringToIndex:8];
    
    self.dropDateLabel.text = [NSString stringWithFormat:@"%@\n%@",[[[selectedDate description] convertDateIntoString] convertDateIntoRequiredBikationString], dropDateTimeString];
    
    //    make_id, area_id, start_date & start_time, end_date & end_time
    NSMutableDictionary *params =[NSMutableDictionary dictionaryWithDictionary: @{@"start_date":[[self.pickUpSelectedDate description] convertDateIntoString],
                             @"start_time":pickDateTimeString,
                             @"end_date":[[selectedDate description] convertDateIntoString],
                             @"end_time":dropDateTimeString}];
    [self callWebServiceToGetBikeListingDetails:params];
}

#pragma mark - WRRentBikeFilterViewControllerDelegate methods

-(void)searchButtonClickedWithParams:(NSMutableDictionary *)params andMakeIdsArray:(NSArray *)makeIdArrays andSelectedArea:(WRArea *)selectedArea
{
    if ([params objectForKey:@"start_date"] !=nil ) {
        self.pickUpDateLabel.text = [NSString stringWithFormat:@"%@\n%@",[[params objectForKey:@"start_date"] convertDateIntoRequiredBikationString],[params objectForKey:@"start_time"]];
        self.dropDateLabel.text = [NSString stringWithFormat:@"%@\n%@",[[params objectForKey:@"end_date"] convertDateIntoRequiredBikationString],[params objectForKey:@"end_time"]];
        NSDictionary *dateDict = @{@"start_date":[params objectForKey:@"start_date"],
                                   @"start_time":[params objectForKey:@"start_time"],
                                   @"end_date":[params objectForKey:@"end_date"],
                                   @"end_time":[params objectForKey:@"end_time"]};
        [[NSUserDefaults standardUserDefaults] setObject:dateDict forKey:kDateDictionary];
    }
    else
    {
        self.pickUpSelectedDate = nil;
        self.pickUpDateLabel.text = @"Pick Up Date";
        self.dropDateLabel.text = @"Drop Date";
        self.pickUpSelectedStr = nil;
    }
    self.selectedArea = selectedArea;
    if (makeIdArrays.count) {
        self.makeIdArray = [NSMutableArray arrayWithArray:makeIdArrays];
    }
    [self callWebServiceToGetBikeListingDetails:params];
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


@end
