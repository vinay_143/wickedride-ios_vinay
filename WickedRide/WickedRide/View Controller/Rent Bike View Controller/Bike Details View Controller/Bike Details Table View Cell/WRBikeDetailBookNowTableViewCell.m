//
//  WRBikeDetailBookNowTableViewCell.m
//  WickedRide
//
//  Created by Ajith Kumar on 21/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRBikeDetailBookNowTableViewCell.h"
#import "WRRentalView.h"
#import "WRConstants.h"
#import "WRArea.h"
#import "NSString+DataValidator.h"

@interface WRBikeDetailBookNowTableViewCell ()<UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UIButton *leftButton;
@property (weak, nonatomic) IBOutlet UIButton *rightButton;
@property (nonatomic, assign) NSInteger daysSelected;
@property (nonatomic, strong) NSArray *rentalArray;

@end

@implementation WRBikeDetailBookNowTableViewCell

- (void)awakeFromNib {
    // Initialization code
    self.rentScrollView.pagingEnabled = YES;
    self.rentScrollView.delegate = self;
    self.leftButton.enabled = NO;
    self.daysSelected = 0;
}

#pragma mark - customise view

-(void)configureViewWithRentalArray:(WRArea *)areaObj withViewWidth:(CGFloat )width
{
//    self.rentalArray = rentalArray;
//    
//    for (int i=0; i<rentalArray.count; i++) {
//        WRRentSummary *rentSummaryObj = [rentalArray objectAtIndex:i];
//        WRRentalView *rentalView = [[[NSBundle mainBundle] loadNibNamed:kRentalViewID owner:self options:nil] firstObject];
//        rentalView.frame = CGRectMake(i*(width-80), 0, self.rentScrollView.frame.size.width, self.rentScrollView.frame.size.height);
//        rentalView.tag = i;
//        rentalView.daysLabel.text = [NSString stringWithFormat:@"%@, rental per day",rentSummaryObj.numberOfDays];
//        rentalView.currencyLabel.text = [NSString stringWithFormat:@"%@",[rentSummaryObj.rentAmount getFormattedPrice]];
//        [self.rentScrollView addSubview:rentalView];
//
//    }
//    self.rentScrollView.contentSize = CGSizeMake(rentalArray.count*(width-80), self.rentScrollView.frame.size.height);

//    self.effectivePriceLabel.text = [NSString stringWithFormat:@"Effective per hour price : Rs.%@/-",[areaObj.effectivePerHourPrice getFormattedPrice]];
//    self.rentalPriceLabel.text = [NSString stringWithFormat:@"Rental price : Rs.%@/-",[areaObj.rentalAmount getFormattedPrice]];
}


#pragma mark - UIButton/ Tap Actions

- (IBAction)bookNowButtonAction:(id)sender
{
    [self.delegate bookNowButtonTappedWithDaysSelected:self.daysSelected];
}

- (IBAction)leftButtonAction:(id)sender
{
    self.rightButton.enabled = YES;
    if (self.daysSelected>0) {
        self.daysSelected--;
        [self.rentScrollView setContentOffset:CGPointMake(self.daysSelected*self.rentScrollView.frame.size.width, 0) animated:YES];
    }
    if (self.daysSelected ==0)
    {
        self.leftButton.enabled = NO;
    }
}

- (IBAction)rightButtonAction:(id)sender
{
    self.leftButton.enabled = YES;
    if (self.daysSelected<self.rentalArray.count-1) {
        self.daysSelected++;
        [self.rentScrollView setContentOffset:CGPointMake(self.daysSelected*self.rentScrollView.frame.size.width, 0) animated:YES];
    }
    if (self.daysSelected ==self.rentalArray.count-1)
    {
        self.rightButton.enabled = NO;
    }
}

#pragma mark - UIScrollViewDelegate Methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == self.rentScrollView )
    {
        int page = scrollView.contentOffset.x/self.rentScrollView.frame.size.width;
        self.daysSelected = page;
    }
    
    if (self.daysSelected == 0) {
        self.leftButton.enabled = NO;
        self.rightButton.enabled = YES;
    }
    if (self.daysSelected == self.rentalArray.count-1) {
        self.leftButton.enabled = YES;
        self.rightButton.enabled = NO;
    }
    if (self.daysSelected>0 && self.daysSelected<self.rentalArray.count-1) {
        self.leftButton.enabled = YES;
        self.rightButton.enabled = YES;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
