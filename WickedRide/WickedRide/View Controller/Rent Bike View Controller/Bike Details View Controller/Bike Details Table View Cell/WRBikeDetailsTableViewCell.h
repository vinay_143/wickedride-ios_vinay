//
//  WRBikeDetailsTableViewCell.h
//  WickedRide
//
//  Created by Ajith Kumar on 21/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WRBikeDetails.h"
@interface WRBikeDetailsTableViewCell : UITableViewCell

-(void)configureCellWithBikeDetails:(WRBikeDetails *)bikeDetailsObj;
@end
