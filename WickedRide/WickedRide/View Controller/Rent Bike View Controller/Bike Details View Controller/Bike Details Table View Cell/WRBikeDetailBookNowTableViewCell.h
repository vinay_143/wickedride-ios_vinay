//
//  WRBikeDetailBookNowTableViewCell.h
//  WickedRide
//
//  Created by Ajith Kumar on 21/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol WRBikeDetailBookNowTableViewCellDelegate <NSObject>

@optional

-(void)bookNowButtonTappedWithDaysSelected:(NSInteger )daysSelected;

@end

@interface WRBikeDetailBookNowTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIScrollView *rentScrollView;
@property (weak, nonatomic) IBOutlet UILabel *effectivePriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *rentalPriceLabel;

@property (nonatomic, weak) id <WRBikeDetailBookNowTableViewCellDelegate> delegate;

-(void)configureViewWithRentalArray:(NSArray *)rentalArray withViewWidth:(CGFloat )width;

@end
