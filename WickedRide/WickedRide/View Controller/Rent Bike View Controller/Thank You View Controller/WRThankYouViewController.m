//
//  WRThankYouViewController.m
//  WickedRide
//
//  Created by Ajith Kumar on 31/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRThankYouViewController.h"
#import "WRWebViewController.h"

@interface WRThankYouViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *thankYouImageView;
@property (weak, nonatomic) IBOutlet UIView *faqView;

@end

@implementation WRThankYouViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    CGFloat aspectRatio = 275.0/375.0;
    self.thankYouImageView.translatesAutoresizingMaskIntoConstraints = YES;
    self.thankYouImageView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.width*aspectRatio);
    
    UITapGestureRecognizer *faqTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(faqViewTapped)];
    [self.faqView addGestureRecognizer:faqTap];
    
}



-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = YES;
}

#pragma mark - UIButton/ Tap Actions

- (IBAction)closeButtonAction:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)faqViewTapped
{
    WRWebViewController *webViewVC = [self.storyboard instantiateViewControllerWithIdentifier:kWebViewControllerID];
    webViewVC.isFromThankYouVC = YES;
    [self.navigationController pushViewController:webViewVC animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
