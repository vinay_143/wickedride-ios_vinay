 //
//  WRReservationViewController.m
//  WickedRide
//
//  Created by Ajith Kumar on 24/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRReservationViewController.h"
#import "WRAccessoriesViewController.h"
#import "WRBikeAvailabilityCalendarViewController.h"
#import "QGSdk.h"


@interface WRReservationViewController ()<UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate, WRBikeAvailabilityCalendarViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UILabel *bikeLabel;
@property (weak, nonatomic) IBOutlet UILabel *daysLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;

@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *mobileTextField;

@property (weak, nonatomic) IBOutlet WRLabel *pickUpDateLabel;
@property (weak, nonatomic) IBOutlet WRLabel *dropDateLabel;
@property (weak, nonatomic) IBOutlet WRLabel *locationLabel;
@property (weak, nonatomic) IBOutlet UIButton *locationDropDownButton;

@property (nonatomic, strong) NSMutableDictionary *reservationDetailDict;
@end

@implementation WRReservationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.reservationDetailDict = [NSMutableDictionary dictionary];
    
    if ([self.paramsDict objectForKey:@"start_date"]!=nil) {
        [self bikeAvailabilityWithSelctedDate:self.paramsDict];
    }
    else
    {
        self.daysLabel.hidden = YES;
        self.priceLabel.hidden = YES;
        self.bikeLabel.hidden = YES;
        self.pickUpDateLabel.text = @"Pick Up Date";
        self.dropDateLabel.text = @"Drop Date";
    }
    
    self.locationLabel.text = self.selectedArea.areaName;
    self.bikeLabel.text = self.selectedBike.bikeName;
    
    if (self.selectedArea.totalAmount !=nil) {
        self.daysLabel.text = [NSString stringWithFormat:@"%@ hours",self.selectedArea.noOfHour];
        self.priceLabel.text = [NSString stringWithFormat:@"Rs. %@/-",[self.selectedArea.totalAmount getFormattedPrice]];
        [self.reservationDetailDict setObject:self.selectedArea.noOfHour forKey:@"no_of_hours"];
        [self.reservationDetailDict setObject:self.selectedArea.totalAmount forKey:@"total_price"];
    }
    if ([self.userManager hasPreviousLoggedInUser]) {
        self.nameTextField.text = [NSString stringWithFormat:@"%@ %@", self.userManager.user.firstName,self.userManager.user.lastName];
        self.emailTextField.text = self.userManager.user.emailId;
        if (![self.userManager.user.mobileNumber isEmptyString]) {
            self.mobileTextField.text = self.userManager.user.mobileNumber;
        }
    }
    [self showNavigationViewWithBackButtonWithAttributeTitle:self.selectedBike.bikeName];
    
    if (IS_IPHONE_4) {
        [self addContainerScrollView];
    }
    
    self.pickUpDateLabel.userInteractionEnabled = YES;
    self.dropDateLabel.userInteractionEnabled = YES;
    UITapGestureRecognizer *pickUpTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pickUpCalenderButtonAction:)];
    [self.pickUpDateLabel addGestureRecognizer:pickUpTap];
    UITapGestureRecognizer *dropTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dropCalenderButtonAction:)];
    [self.dropDateLabel addGestureRecognizer:dropTap];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(bikeAvailableDatesSelected:) name:kNotificationBikeAvailableDateSelected object:nil];

}

-(void)addContainerScrollView
{
    self.containerView.translatesAutoresizingMaskIntoConstraints = YES;
    UIScrollView *mainScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(self.view.frame.origin.x , self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height-164)];
    self.containerView.frame = CGRectMake(0, 0, self.view.frame.size.width, 468.0);
    mainScrollView.contentSize = CGSizeMake(320, 468.0);
    [mainScrollView addSubview:self.containerView];
    [self.view addSubview:mainScrollView];
     [self.view bringSubviewToFront:self.customNavView];
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = YES;
}
#pragma mark - UIButton/Tap Actions

- (IBAction)pickUpCalenderButtonAction:(id)sender
{
    WRBikeAvailabilityCalendarViewController *availabilityVC = [self.storyboard instantiateViewControllerWithIdentifier:kBikeAvailabilityCalendarViewControllerID];
    availabilityVC.selectedArea = self.selectedArea;
    availabilityVC.selectedBike = self.selectedBike;
    availabilityVC.delegate = self;
    UINavigationController *navController=[[UINavigationController alloc] initWithRootViewController:availabilityVC];
    [self.navigationController presentViewController:navController animated:YES completion:nil];
}


- (IBAction)dropCalenderButtonAction:(id)sender
{
    if (self.paramsDict == nil) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Please select pick up date" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
    }
    else
    {
        WRBikeAvailabilityCalendarViewController *availabilityVC = [self.storyboard instantiateViewControllerWithIdentifier:kBikeAvailabilityCalendarViewControllerID];
        availabilityVC.selectedArea = self.selectedArea;
        availabilityVC.selectedBike = self.selectedBike;
        availabilityVC.delegate = self;
        availabilityVC.paramsDict= self.paramsDict;
        UINavigationController *navController=[[UINavigationController alloc] initWithRootViewController:availabilityVC];
        [self.navigationController presentViewController:navController animated:YES completion:nil];
    }
}

- (IBAction)locationDropDownButtonAction:(id)sender
{
//    if (self.locationTableView.hidden) {
//        [self.view bringSubviewToFront:self.locationTableView];
//        self.locationTableView.hidden = NO;
//        CABasicAnimation *rotation;
//        rotation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
//        rotation.fromValue = [NSNumber numberWithFloat:0];
//        rotation.toValue = [NSNumber numberWithFloat:(M_PI)];
//        rotation.duration = 0.2; // Speed
//        rotation.repeatCount = 1; // Repeat forever. Can be a finite number.
//        [self.locationDropDownButton.layer addAnimation:rotation forKey:@"Spin"];
//        [self.locationDropDownButton.layer setZPosition:100];
//        self.locationDropDownButton.transform = CGAffineTransformMakeRotation(M_PI);
//
//    }
//    else
//    {
//        self.locationTableView.hidden = YES;
//        CABasicAnimation *rotation;
//        rotation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
//        rotation.fromValue = [NSNumber numberWithFloat:(M_PI)];
//        rotation.toValue = [NSNumber numberWithFloat:(-0)];
//        rotation.duration = 0.2; // Speed
//        rotation.repeatCount = 1; // Repeat forever. Can be a finite number.
//        [self.locationDropDownButton.layer addAnimation:rotation forKey:@"Spin"];
//        [self.locationDropDownButton.layer setZPosition:100];
//        self.locationDropDownButton.transform = CGAffineTransformIdentity;
//    }
}

-(void)bikeAvailableDatesSelected:(NSNotification *)notficationObj
{
    [self bikeAvailabilityWithSelctedDate:[NSMutableDictionary dictionaryWithDictionary:notficationObj.userInfo]];
}


- (IBAction)accessoriesButtonActions:(id)sender
{
    if ([self.nameTextField.text isEmptyString]||[self.emailTextField.text isEmptyString]||[self.mobileTextField.text isEmptyString]) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"All fields are Mandatory" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
    }
    else if (![self.emailTextField.text validateEmail])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Please enter valid email id" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
    }
    else if (self.mobileTextField.text.length<10)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Please enter valid mobile number" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
    }
    else if([self.paramsDict objectForKey:@"start_date"] == nil)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Select your pick up date and drop date" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
    }
    else
    {
        NSMutableDictionary *userInfo = [NSMutableDictionary dictionaryWithDictionary:@{@"name":self.nameTextField.text,
                                                                                      @"email":self.emailTextField.text,
                                                                                      @"mobile":self.mobileTextField.text}];
        [self.reservationDetailDict addEntriesFromDictionary:userInfo];
        WRAccessoriesViewController *accessoriesVC = [self.storyboard instantiateViewControllerWithIdentifier:kAccessoriesViewControllerID];
        accessoriesVC.paramsDict = self.paramsDict;
        accessoriesVC.userDetailDict = self.reservationDetailDict;
        accessoriesVC.selectedBike = self.selectedBike;
        accessoriesVC.selectedArea = self.selectedArea;
        [self.navigationController pushViewController:accessoriesVC animated:YES];
       
        NSMutableDictionary *searchDetails = [[NSMutableDictionary alloc] init];
        [searchDetails setObject:self.selectedBike.bikeName forKey:@"bike_name"];
        [searchDetails setObject:self.priceLabel.text forKey:@"price"];
        [searchDetails setObject:[self.paramsDict valueForKey:@"start_date"] forKey:@"start_date"];
        [searchDetails setObject:[self.paramsDict valueForKey:@"end_date"] forKey:@"end_date"];
        [searchDetails setObject:self.selectedArea.areaName forKey:@"location"];
        [searchDetails setObject:[[[NSUserDefaults standardUserDefaults] objectForKey:kSelectedLocation] objectForKey:kCityKey] forKey:@"city"];

        [self logEventForUserSerachDetails:searchDetails];
    
    }
    
}


-(void)doneClicked
{
    [self.view endEditing:YES];
}

#pragma mark - QGraph log user_search_details
-(void)logEventForUserSerachDetails:(NSMutableDictionary *)searchDetails
{

    [[QGSdk getSharedInstance] logEvent:@"user_search_details" withParameters:searchDetails];
}

#pragma mark - UITableViewDataSource, UITableViewDelegate Methods

-(NSInteger )tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 10;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.font = AVENIR_BOOK(15.0);
        cell.textLabel.tag = indexPath.row;
        cell.textLabel.textColor = [UIColor colorWithRed:0.64 green:0.64 blue:0.64 alpha:1];
    }
    NSString *string = [NSString stringWithFormat:@"Banglore, Jayanagara %ld",(long)indexPath.row];
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:string];
    CGFloat spacing = 0.5f;
    [attributedString addAttribute:NSKernAttributeName
                             value:@(spacing)
                             range:NSMakeRange(0, [string length])];
    
    cell.textLabel.attributedText = attributedString;
    
    return cell;
}


#pragma mark - UITextFieldDelegate Methods

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField == self.mobileTextField)
    {
        //        [self.delegate moveAddressScrollViewWithTextFied:textField];
        UIToolbar* keyboardDoneButtonView = [[UIToolbar alloc] init];
        [keyboardDoneButtonView sizeToFit];
        UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                       style:UIBarButtonItemStylePlain target:self
                                                                      action:@selector(doneClicked)];
        //    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:doneButton, nil]];
        
        [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                          [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                          doneButton,
                                          nil]];
        keyboardDoneButtonView.tintColor = [UIColor blackColor];
        textField.inputAccessoryView = keyboardDoneButtonView;
    }

    return YES;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.nameTextField) {
        [self.emailTextField becomeFirstResponder];
    }
    else if (textField == self.emailTextField)
    {
        [self.mobileTextField becomeFirstResponder];
    }
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == self.mobileTextField) {
        NSString *resultingString = [textField.text stringByReplacingCharactersInRange: range withString: string];
        if (resultingString.length>10) {
            return NO;
        }
        else
        {
            return YES;
        }
    }
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - WRCalenederViewControllerDelegate Methods

-(void)nextButtonTappedWithPickUpDate:(NSString *)selectedDate
{
    self.pickUpDateLabel.text = selectedDate;
}

-(void)nextButtonTappedWithDropDate:(NSString *)selectedDate
{
    self.dropDateLabel.text = selectedDate;
}

#pragma mark - WRBikeAvailabilityCalendarViewControllerDelegate Method

-(void)bikeAvailabilityWithSelctedDate:(NSMutableDictionary *)params
{
    self.pickUpDateLabel.text = [NSString stringWithFormat:@"%@\n%@",[[params objectForKey:@"start_date"] convertDateIntoRequiredBikationString],[params objectForKey:@"start_time"]];
    self.dropDateLabel.text = [NSString stringWithFormat:@"%@\n%@",[[params objectForKey:@"end_date"] convertDateIntoRequiredBikationString],[params objectForKey:@"end_time"]];
    
    if (self.paramsDict == nil) {
        self.paramsDict = [NSMutableDictionary dictionary];
    }
    [self.paramsDict addEntriesFromDictionary:params];
    if ([self.paramsDict objectForKey:@"city_id"]==nil||[self.paramsDict objectForKey:@"model_id"]==nil) {
        [self.paramsDict setObject:[[[NSUserDefaults standardUserDefaults] objectForKey:kSelectedLocation] objectForKey:@"id"] forKey:@"city_id"];
        [self.paramsDict setObject:self.selectedBike.bikeId forKey:@"model_id"];
    }
    [self loadMBProgressCustomLoaderView];
    
    [self.networkManager startGETRequestWithAPI:aGetTotalPrice andParameters:self.paramsDict withSuccess:^(NSDictionary *responseDictionary) {
        [self hideMBProgressCutomLoader];
        int statusCode = [[[responseDictionary objectForKey:@"result"] objectForKey:@"status_code"] intValue];
        if (statusCode == 200) {
            [self configureBottomViewWith:[[responseDictionary objectForKey:@"result"] objectForKey:@"data"]];
        }
        else
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:[[responseDictionary objectForKey:@"result"] objectForKey:@"message"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
        
    } andFailure:^(NSString *errorMessage) {
        [self hideMBProgressCutomLoader];
        if (![errorMessage isEqualToString:kErrorMessage]) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Something went Wrong. Please try later" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
        else
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:errorMessage delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
        
        self.daysLabel.hidden = YES;
        self.priceLabel.hidden = YES;
        self.bikeLabel.hidden = YES;
    }];
}

-(void)configureBottomViewWith:(NSDictionary *)dataDict
{
    self.daysLabel.hidden = NO;
    self.priceLabel.hidden = NO;
    self.bikeLabel.hidden = NO;
    [self.reservationDetailDict addEntriesFromDictionary:dataDict];
    
    NSString *timeStr;
//    if ([[dataDict objectForKey:@"no_of_hours"] intValue] <=4) {
//        timeStr = @"Minimum 4";
//    }
//    else
    {
        timeStr = [dataDict objectForKey:@"no_of_hours"];
    }
    self.daysLabel.text = [NSString stringWithFormat:@"%@ hours",timeStr];
    NSString *totalPriceString = [NSString stringWithFormat:@"%@",[dataDict objectForKey:@"total_price"]];
    self.priceLabel.text = [NSString stringWithFormat:@"Rs. %@/-",[totalPriceString getFormattedPrice]];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
