//
//  WRAccessoriesTableViewCell.h
//  WickedRide
//
//  Created by Ajith Kumar on 24/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WRAccessories.h"

@protocol WRAccessoriesTableViewCellDelegate <NSObject>

@optional

-(void)checkBoxButtonTappedWith:(NSInteger )selectedIndex andIsCheckBoxSelected:(BOOL )isCheckBoxSelected;

@end

@interface WRAccessoriesTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *accessoryImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UIButton *checkBoxButton;

@property (nonatomic, weak) id <WRAccessoriesTableViewCellDelegate> delegate;

-(void)configureAccessoryCellWith:(WRAccessories *)accessory;
@end
