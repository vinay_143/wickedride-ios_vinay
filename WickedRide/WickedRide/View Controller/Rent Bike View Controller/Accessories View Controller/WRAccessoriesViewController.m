//
//  WRAccessoriesViewController.m
//  WickedRide
//
//  Created by Ajith Kumar on 24/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRAccessoriesViewController.h"
#import "WRAccessoriesTableViewCell.h"
#import "WRSummaryViewController.h"
#import "WRAccessories.h"

@interface WRAccessoriesViewController ()<UITableViewDataSource, UITableViewDelegate, WRAccessoriesTableViewCellDelegate,UIWebViewDelegate>
{
    int totalAmount;
}
@property (weak, nonatomic) IBOutlet UITableView *accessoriesTableView;
@property (weak, nonatomic) IBOutlet UILabel *bikeLabel;
@property (weak, nonatomic) IBOutlet UILabel *daysLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *alertLabel;

@property (weak, nonatomic) IBOutlet UIWebView *documnetsWebV;
@property (nonatomic, strong) NSMutableArray *accessoriesArray;

@property (nonatomic, strong) NSMutableArray *selectedAccessoryArray;
@end

@implementation WRAccessoriesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = NO;
    self.selectedAccessoryArray = [NSMutableArray array];
    [self showNavigationViewWithBackButtonWithTitle:@"DOCUMENTS & ACCESSORIES"];
    self.accessoriesArray = [NSMutableArray array];
  //  [self callWebServiceToGetAvailableAccessories];
    self.bikeLabel.text = self.selectedBike.bikeName;
    self.documnetsWebV.delegate = self;
    
    NSString *timeStr;
    if ([[self.userDetailDict objectForKey:@"no_of_hours"] intValue] <=4) {
        timeStr = @"Minimum 4";
    }
    else
    {
        timeStr = [self.userDetailDict objectForKey:@"no_of_hours"];
    }

    self.daysLabel.text = [NSString stringWithFormat:@"%@ hours",timeStr];
    NSString *totalPriceString = [NSString stringWithFormat:@"%@",[self.userDetailDict objectForKey:@"total_price"]];
    self.priceLabel.text = [NSString stringWithFormat:@"Rs. %@/-",[totalPriceString getFormattedPrice]];
    totalAmount = [[self.userDetailDict objectForKey:@"total_price"] intValue];

    [self.alertLabel changeWidthTo:self.view.frame.size.width-40 andHeightTo:270];
    [self.alertLabel changedXAxisTo:30 andYAxisTo:0];
    [self loadDocumentUrlOnWebView];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
}

#pragma mark - Call Web Service

-(void)callWebServiceToGetAvailableAccessories
{
    self.alertLabel.hidden = YES;
    self.accessoriesTableView.hidden = YES;
    [self loadMBProgressCustomLoaderView];
    [self.networkManager startGETRequestWithAPI:aBikeAccessories andParameters:self.paramsDict withSuccess:^(NSDictionary *responseDictionary) {
        [self hideMBProgressCutomLoader];
        for (NSDictionary *accessoryDict in [[responseDictionary objectForKey:@"result"] objectForKey:@"data"])
        {
            WRAccessories *accessory = [WRAccessories getAccessoriesInformationFrom:accessoryDict];
            [self.accessoriesArray addObject:accessory];
        }
        if (self.accessoriesArray.count) {
            self.accessoriesTableView.hidden = NO;
            [self.accessoriesTableView reloadData];
        }
        else
        {
            self.alertLabel.hidden = NO;
        }
        
    } andFailure:^(NSString *errorMessage) {
        [self hideMBProgressCutomLoader];
        if (![errorMessage isEqualToString:kErrorMessage]) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Something went Wrong. Please try later" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
        else
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:errorMessage delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }

    }];
}
#pragma mark - UIButton/ Tap Actions

- (IBAction)reserveButtonAction:(id)sender
{
    [self.userDetailDict setObject:[NSString stringWithFormat:@"%d",totalAmount] forKey:@"final_price"];
    WRSummaryViewController *summaryVC = [self.storyboard instantiateViewControllerWithIdentifier:kSummaryViewControllerID];
    [self.paramsDict setObject:[self.paramsDict objectForKey:@"start_time"] forKey:@"start_time"];
    [self.paramsDict setObject:[self.paramsDict objectForKey:@"end_time"]  forKey:@"end_time"];
    summaryVC.paramsDict = self.paramsDict;
    [self.userDetailDict removeObjectForKey:@"discount"];
    summaryVC.userDetailDict = self.userDetailDict;
    summaryVC.selectedBike = self.selectedBike;
    summaryVC.selectedArea = self.selectedArea;
    summaryVC.accessoriesArray = self.selectedAccessoryArray;
    [self.navigationController pushViewController:summaryVC animated:YES];
}


#pragma mark - Load document URL

-(void)loadDocumentUrlOnWebView
{
    [self loadMBProgressCustomLoaderView];
    NSURL *url = [NSURL URLWithString:aDocRequest];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [self.documnetsWebV loadRequest:requestObj];
    [self.documnetsWebV setBackgroundColor:[UIColor clearColor]];
    [self.documnetsWebV setOpaque:NO];
    self.documnetsWebV.delegate = self;

}

#pragma mark - webviewDelegate Methods
-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self hideMBProgressCutomLoader];

}


#pragma mark - UITableViewDataSource, UITableViewDelegate Methods

-(NSInteger )tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.accessoriesArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    WRAccessories *accessory = [self.accessoriesArray objectAtIndex:indexPath.row];
    WRAccessoriesTableViewCell *cell = (WRAccessoriesTableViewCell *)[tableView dequeueReusableCellWithIdentifier:kAccessoriesTableViewCellID];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.tag = indexPath.row;
    cell.delegate = self;
    [cell configureAccessoryCellWith:accessory];
    return cell;
}

-(CGFloat )tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 125.0;
}

#pragma mark - WRAccessoriesTableViewCellDelegate Methods

-(void)checkBoxButtonTappedWith:(NSInteger)selectedIndex andIsCheckBoxSelected:(BOOL)isCheckBoxSelected
{
    WRAccessories *accessories = [self.accessoriesArray objectAtIndex:selectedIndex];
    int numberOfHour = [[self .userDetailDict objectForKey:@"no_of_hours"] intValue];
    int accessoryAmount = [accessories.rentalAmount intValue];
    if (isCheckBoxSelected) {
        totalAmount = totalAmount + (numberOfHour*accessoryAmount);
        [self.selectedAccessoryArray addObject:accessories];
    }
    else
    {
        if (self.selectedAccessoryArray.count) {
            totalAmount = totalAmount - (numberOfHour*accessoryAmount);
            [self.selectedAccessoryArray removeObject:accessories];
        }
    }
    NSString *totalAmountStr = [NSString stringWithFormat:@"%d", totalAmount];
    self.priceLabel.text = [NSString stringWithFormat:@"Rs. %@/-",[totalAmountStr getFormattedPrice]];
    
    if (self.selectedAccessoryArray.count) {
        if (self.selectedAccessoryArray.count>1) {
            self.bikeLabel.text = [NSString stringWithFormat:@"%@ + %lu more itmes",self.selectedBike.bikeName,(unsigned long)self.selectedAccessoryArray.count];
        }
        else
        {
            WRAccessories *accessory = [self.selectedAccessoryArray objectAtIndex:0];
            self.bikeLabel.text = [NSString stringWithFormat:@"%@ + %@",self.selectedBike.bikeName,accessory.name];
        }
    }
    else
    {
        self.bikeLabel.text = self.selectedBike.bikeName;
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
