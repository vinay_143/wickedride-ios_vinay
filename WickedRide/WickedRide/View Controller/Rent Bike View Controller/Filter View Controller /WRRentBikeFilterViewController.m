//
//  WRRentBikeFilterViewController.m
//  WickedRide
//
//  Created by Ajith Kumar on 10/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRRentBikeFilterViewController.h"
#import "WRBikeListingViewController.h"
#import "WRCommonFilterCollectionViewCell.h"
#import "WRBikesBrandCollectionViewCell.h"
#import "WRPickDateCollectionViewCell.h"
#import "WRNoteFilterCollectionViewCell.h"
#import "WRCalenederViewController.h"
#import "WRArea.h"
#import "WRBikeMake.h"
#import "QGSdk.h"



@interface WRRentBikeFilterViewController () <UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout, UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate, WRPickDateCollectionViewCellDelegate, WRCalenederViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *filterCollectionView;
@property (weak, nonatomic) IBOutlet UITableView *locationTableView;

@property (nonatomic,strong) NSString *pickUpDateStr;
@property (nonatomic,strong) NSString *dropDateStr;

@property (nonatomic, strong) NSMutableArray *makeArray;
@property (nonatomic, strong) NSMutableArray *allAreaArray;

@property (nonatomic, strong) WRBikeMake *selectedBikeMake;

@property (nonatomic, strong) NSDate *pickUpSelectedDate;
@property (nonatomic, strong) NSString *pickUpSelectedTimeStr;
@property (nonatomic, strong) NSDate *dropSelectedDate;
@property (nonatomic, strong) NSString *dropDateSelectedTimeStr;

@property (nonatomic, strong) NSMutableArray *selectedCellArray;
@property (nonatomic, assign) BOOL isMakeIdSelected;
@property (nonatomic, assign) BOOL isAreaSelected;

@end

@interface WRFilterHeaderReusableView : UICollectionReusableView

@end

@implementation WRFilterHeaderReusableView

@end

@implementation WRRentBikeFilterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.isAreaSelected = NO;
    self.isMakeIdSelected = NO;
    self.allAreaArray = [NSMutableArray array];
    self.makeArray = [NSMutableArray array];
    self.selectedCellArray = [NSMutableArray array];
    if (self.paramsDict == nil) {
        self.paramsDict = [NSMutableDictionary dictionary];
    }
    [self showNavigationViewWithBackButtonWithTitle:@"PICK YOUR RIDE" ];
    // Do any additional setup after loading the view.
    [self.filterCollectionView registerClass:[WRFilterHeaderReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView"];
    self.locationTableView.layer.borderWidth = 1.0;
    self.locationTableView.layer.borderColor = [[UIColor colorWithRed:0.93 green:0.93 blue:0.93 alpha:1] CGColor];
    
//    self.makeArray = [NSMutableArray arrayWithArray:@[@"ducati",@"harley-Davidon",@"indian",@"Kawasaki",@"re",@"triumph"]];
    [self callWebServiceToGetAllArea];
    [self callWebServiceToGetallMakes];
    self.pickUpDateStr = @"Pick UP Date";
    self.dropDateStr = @"Drop Date";
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(calendarSelectedDate:) name:kNotificationCalendarDateSelected object:nil];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

#pragma mark - Web Services methods

-(void)callWebServiceToGetAllArea
{
    if (self.userManager.areaArray == nil) {
        self.filterCollectionView.hidden = YES;
        NSString *city_id = [NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults] valueForKey:kSelectedLocation] objectForKey:@"id"]];
//        NSString *url = [NSString stringWithFormat:@"%@/%@/areas",aGetAllAreas,city_id];
        [self.networkManager startGETRequestWithAPI:aGetAllAreas(city_id) andParameters:nil withSuccess:^(NSDictionary *responseDictionary) {
            NSLog(@"%@",responseDictionary);
            
            self.isAreaSelected = YES;
            for (NSDictionary *areaDict in [[responseDictionary objectForKey:@"result"] objectForKey:@"data"]) {
                WRArea *areaObj = [WRArea getAreaInformationFrom:areaDict];
                [self.allAreaArray addObject:areaObj];
            }
            self.userManager.areaArray = [[responseDictionary objectForKey:@"result"] objectForKey:@"data"];
            if (self.selectedArea == nil) {
                self.selectedArea = [self.allAreaArray objectAtIndex:0];
            }
            self.filterCollectionView.hidden = NO;
            [self.filterCollectionView reloadData];
            [self.locationTableView reloadData];
            
        } andFailure:^(NSString *errorMessage) {
        }];
    }
    else
    {
        for (NSDictionary *areaDict in self.userManager.areaArray) {
            WRArea *areaObj = [WRArea getAreaInformationFrom:areaDict];
            [self.allAreaArray addObject:areaObj];
        }
        if (self.selectedArea == nil) {
            self.selectedArea = [self.allAreaArray objectAtIndex:0];
        }
        self.isAreaSelected = YES;
        self.filterCollectionView.hidden = NO;
        [self.filterCollectionView reloadData];
        [self.locationTableView reloadData];
    }
}

-(void)callWebServiceToGetallMakes
{
    [self loadMBProgressCustomLoaderView];
    NSString *city_id = [NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults] valueForKey:kSelectedLocation] objectForKey:@"id"]];
    NSDictionary *params = @{@"city_id":city_id};
    
    [self.networkManager startGETRequestWithAPI:aGetAllMakeLogo andParameters:params withSuccess:^(NSDictionary *responseDictionary) {
        [self hideMBProgressCutomLoader];
        for (NSDictionary *makeDict in [[responseDictionary objectForKey:@"result"] objectForKey:@"data"])
        {
            WRBikeMake *bikeMakeObj = [WRBikeMake getInformationOfBikeMakesFrom:makeDict];
            [self.makeArray addObject:bikeMakeObj];
        }
        self.isMakeIdSelected = YES;
        
        [self.filterCollectionView reloadData];
    } andFailure:^(NSString *errorMessage) {
        [self hideMBProgressCutomLoader];
        if (![errorMessage isEqualToString:kErrorMessage]) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Something went Wrong. Please try later" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
        else
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:errorMessage delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
    }];
    
}

#pragma mark - UICollectionViewDataSource/ UICollectionViewDelegate Methods

-(NSInteger )numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 2;
}

-(NSInteger )collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (section == 0) {
        return 2;
    }
    return self.makeArray.count;
}



-(CGSize) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    if (section == 1) {
        return CGSizeMake(collectionView.frame.size.width-40, 75);
    }
    return CGSizeMake(0.0, 0.0);
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    if (kind == UICollectionElementKindSectionHeader) {
        if (indexPath.section == 1) {
            WRFilterHeaderReusableView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:
                                                      UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView" forIndexPath:indexPath];
            WRLabel *headerLabel = [[WRLabel alloc] initWithFrame:CGRectMake(20, 0, headerView.frame.size.width-40, 75)];
            headerLabel.font = MONTSERRAT_BOLD(12.0);
            headerLabel.text = @"PICK YOUR BRAND";

            [headerView addSubview:headerLabel];
            
            return headerView;
        }
    }
    return nil;

}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            WRPickDateCollectionViewCell *cell = (WRPickDateCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:kPickDateFilterCollectionViewCellID forIndexPath:indexPath];
            [cell configureViewWithSelectedDates:self.paramsDict];
            cell.delegate = self;
            return cell;
        }
        WRCommonFilterCollectionViewCell *cell = (WRCommonFilterCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:kRentBikeCommonFilterCollectionViewCellID forIndexPath:indexPath];
        if (self.isAreaSelected) {
            if (self.selectedArea !=nil) {
                cell.valueLabel.text = self.selectedArea.areaName;
            }
            else
            {
                if (self.allAreaArray.count) {
                    WRArea *firstArea = [self.allAreaArray objectAtIndex:0];
                    cell.valueLabel.text = firstArea.areaName;
                }
            }
        }
        return cell;
    }
    if (self.isMakeIdSelected) {
        if (self.makeIdArray.count) {
            self.selectedCellArray = self.makeIdArray;
            self.makeIdArray = nil;
        }    }
    WRBikeMake *bikeMakeObj = [self.makeArray objectAtIndex:indexPath.row];
    WRBikesBrandCollectionViewCell *cell = (WRBikesBrandCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:kBikesBrandFilterCollectionViewCellID forIndexPath:indexPath];
    cell.tag = indexPath.row;
    if ([self.selectedCellArray containsObject:indexPath]) {
        cell.checkImageView.hidden = NO;
    }
    else
    {
        cell.checkImageView.hidden = YES;
    }
    [cell configureBeandCellWith:bikeMakeObj];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        if (indexPath.row == 0)
        {
            return CGSizeMake(collectionView.frame.size.width-40, 90);
        }
        return CGSizeMake(collectionView.frame.size.width-40, 80);
    }
    float aspectRatio = (self.view.frame.size.width-40)*0.305;
    return CGSizeMake(aspectRatio, aspectRatio);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        if (indexPath.row == 1)
        {
            WRCommonFilterCollectionViewCell *cell = (WRCommonFilterCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
            CGFloat tableViewYAxix = 188 - self.filterCollectionView.contentOffset.y;
            if (self.allAreaArray.count<5) {
                self.locationTableView.frame = CGRectMake(20, tableViewYAxix, self.view.frame.size.width-40, self.allAreaArray.count*35);
            }
            else
            {
                self.locationTableView.frame = CGRectMake(20, tableViewYAxix, self.view.frame.size.width-40, 175);
            }

            if (self.locationTableView.hidden) {
                self.locationTableView.hidden = NO;
            }
            else
            {
                self.locationTableView.hidden = YES;
            }
            [cell configureDropDownButtonWith:self.locationTableView.hidden];
        }
    }
    else
    {
        if (self.selectedCellArray.count)
        {
            if ([self.selectedCellArray containsObject:indexPath]) {
                [self.selectedCellArray removeObject:indexPath];
            }
            else
            {
                [self.selectedCellArray addObject:indexPath];
            }
        }
        else
        {
            [self.selectedCellArray addObject:indexPath];
        }
        WRBikesBrandCollectionViewCell *selectedCell = (WRBikesBrandCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
        if (selectedCell.checkImageView.hidden == YES) {
            selectedCell.checkImageView.hidden = NO;
        }
        else
        {
            selectedCell.checkImageView.hidden = YES;
        }
        
    }
}

#pragma mark - UITableViewDataSource, UITableViewDelegate Methods

-(NSInteger )tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.allAreaArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.font = AVENIR_BOOK(15.0);
        cell.textLabel.tag = indexPath.row;
        cell.textLabel.textColor = [UIColor colorWithRed:0.64 green:0.64 blue:0.64 alpha:1];
    }
    WRArea *areaObj = [self.allAreaArray objectAtIndex:indexPath.row];
    NSString *string = [NSString stringWithFormat:@"%@",areaObj.areaName];
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:string];
    CGFloat spacing = 0.5f;
    [attributedString addAttribute:NSKernAttributeName
                             value:@(spacing)
                             range:NSMakeRange(0, [string length])];

    cell.textLabel.attributedText = attributedString;

    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSIndexPath *index = [NSIndexPath indexPathForRow:1 inSection:0];
    WRCommonFilterCollectionViewCell *cell = (WRCommonFilterCollectionViewCell *)[self.filterCollectionView cellForItemAtIndexPath:index];
    self.selectedArea = [self.allAreaArray objectAtIndex:indexPath.row];
    self.locationTableView.hidden = YES;
    [cell configureDropDownButtonWith:self.locationTableView.hidden];
    NSIndexPath *collectionIndexPath = [NSIndexPath indexPathForItem:1 inSection:0];
    [self.filterCollectionView reloadItemsAtIndexPaths:@[collectionIndexPath]];
}


#pragma mark - UIScrollViewDelegate Methods

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == self.filterCollectionView) {
        self.locationTableView.hidden = YES;
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UIButton/ Tap Actions

- (IBAction)filterNextBarBtnTapped:(id)sender
{
    WRBikeListingViewController *bikeListingVC = [self.storyboard instantiateViewControllerWithIdentifier:kBikeListingViewControllerID];
    [self.navigationController pushViewController:bikeListingVC animated:YES];
}

-(void)calendarSelectedDate:(NSNotification *)notificationObj
{
    [self.paramsDict addEntriesFromDictionary:notificationObj.userInfo];
    
    self.dropDateStr = [self.paramsDict objectForKey:@"end_date"];
    self.dropDateSelectedTimeStr = [self.paramsDict objectForKey:@"end_time"];
    self.pickUpDateStr = [self.paramsDict objectForKey:@"start_date"];
    self.pickUpSelectedTimeStr = [self.paramsDict objectForKey:@"start_time"];
    [self.filterCollectionView reloadData];

//    self.pickUpDateStr.text = [NSString stringWithFormat:@"%@\n%@",[[params objectForKey:@"start_date"] convertDateIntoRequiredBikationString],[params objectForKey:@"start_time"]];
//    self.dropDateLabel.text = [NSString stringWithFormat:@"%@\n%@",[[params objectForKey:@"end_date"] convertDateIntoRequiredBikationString],[params objectForKey:@"end_time"]];
//    [self callWebServiceToGetBikeListingDetails:[NSMutableDictionary dictionaryWithDictionary:params]];
}


- (IBAction)searchButtonAction:(id)sender
{
    NSMutableArray *makeArray = [NSMutableArray array];
    NSMutableArray *makerNamesArray = [NSMutableArray array];
    for (NSIndexPath *indexPath in self.selectedCellArray) {
        WRBikeMake *bikeMake = [self.makeArray objectAtIndex:indexPath.row];
        [makeArray addObject:bikeMake.make_id];
        [makerNamesArray addObject:bikeMake.bikeModel];
    }
    NSMutableDictionary *params;
    NSMutableDictionary *searchKeysDict;
    if (self.selectedArea.area_id != nil) {
        params = [NSMutableDictionary dictionaryWithDictionary:@{@"area_id":self.selectedArea.area_id}];
        searchKeysDict = [NSMutableDictionary dictionaryWithDictionary:@{@"area_name":self.selectedArea.areaName}];
        
    }
    if (makeArray.count) {
        [params setObject:[makeArray componentsJoinedByString:@","] forKey:@"make_ids"];
        [searchKeysDict setObject:[makerNamesArray componentsJoinedByString:@","] forKey:@"bike_brand"];
    }

//    if (![self.dropDateStr isEqualToString:@"Drop Date"]) {
//        NSRange range = [self.pickUpSelectedTimeStr rangeOfString:@" -"];
//        NSString *pickDateTimeString = [self.pickUpSelectedTimeStr substringToIndex:range.location];
//        NSString *dropDateTimeString = [self.dropDateSelectedTimeStr substringToIndex:range.location];
//
//        NSDictionary *dateDict = @{@"start_date":self.pickUpDateStr,
//                                   @"start_time":pickDateTimeString,
//                                   @"end_date":self.dropDateStr,
//                                   @"end_time":dropDateTimeString,
//                                   @"start_date_value":self.pickUpSelectedDate};
//        [params addEntriesFromDictionary:dateDict];
//    }
    [params addEntriesFromDictionary:self.paramsDict];
    [searchKeysDict addEntriesFromDictionary:self.paramsDict];
    [self logEventForBikeSearchWithKeyWords:searchKeysDict];
    [self.delegate searchButtonClickedWithParams:params andMakeIdsArray:self.selectedCellArray andSelectedArea:self.selectedArea];
    [self.navigationController popViewControllerAnimated:YES];
    
    
}

#pragma mark - WRPickDateCollectionViewCellDelegate Methods

-(void)pickUpBikeCalenderTapped
{
    WRCalenederViewController *calenderVC = [self.storyboard instantiateViewControllerWithIdentifier:@"WRCalenederViewController"];
    UINavigationController *navController=[[UINavigationController alloc] initWithRootViewController:calenderVC];
    calenderVC.isPickUpDateSelected = YES;
    calenderVC.delegate = self;
    [self.navigationController presentViewController:navController animated:YES completion:nil];
}

-(void)dropBikeCalenderTapped
{
    if ([self.paramsDict objectForKey:@"start_date"] == nil) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Please select pick up date" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
    }
    else
    {
        WRCalenederViewController *calenderVC = [self.storyboard instantiateViewControllerWithIdentifier:@"WRCalenederViewController"];
        UINavigationController *navController=[[UINavigationController alloc] initWithRootViewController:calenderVC];
        
        calenderVC.pickDate = [self returnDateFromString:[self.paramsDict objectForKey:@"start_date"]];
        calenderVC.pickDateStartTime = [self.paramsDict objectForKey:@"start_time"];
        calenderVC.delegate = self;
        [self.navigationController presentViewController:navController animated:YES completion:nil];
    }

}


#pragma mark - QGraph log search
-(void)logEventForBikeSearchWithKeyWords:(NSMutableDictionary *)searchKeys
{


    [[QGSdk getSharedInstance] logEvent:@"Search" withParameters:searchKeys];


}

#pragma mark - WRCalenederViewControllerDelegate Methods

-(void)nextButtonTappedWithPickUpDate:(NSDate *)selectedDate andTheTime:(NSString *)timeStr
{
    self.dropDateStr = @"Drop Date";
    self.pickUpSelectedDate = selectedDate;
    self.pickUpDateStr = [[selectedDate description] convertDateIntoString];
    self.pickUpSelectedTimeStr = timeStr;
    [self.filterCollectionView reloadData];
}

-(void)nextButtonTappedWithDropDate:(NSDate *)selectedDate andTheTime:(NSString *)timeStr
{
    self.dropSelectedDate  = selectedDate;
    self.dropDateStr = [[selectedDate description] convertDateIntoString];
    self.dropDateSelectedTimeStr = timeStr;
    [self.filterCollectionView reloadData];
    
}


-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
