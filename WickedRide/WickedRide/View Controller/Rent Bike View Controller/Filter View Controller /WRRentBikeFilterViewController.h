//
//  WRRentBikeFilterViewController.h
//  WickedRide
//
//  Created by Ajith Kumar on 10/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WRBaseViewController.h"
#import "WRArea.h"

@protocol WRRentBikeFilterViewControllerDelegate <NSObject>

@optional

-(void)searchButtonClickedWithParams:(NSMutableDictionary *)params andMakeIdsArray:(NSArray *)makeIdArrays andSelectedArea:(WRArea *)selectedArea;

@end

@interface WRRentBikeFilterViewController : WRBaseViewController

@property (nonatomic, strong) NSMutableDictionary *paramsDict;
@property (nonatomic, strong) NSMutableArray *makeIdArray;
@property (nonatomic, strong) WRArea *selectedArea;

@property (nonatomic, weak) id <WRRentBikeFilterViewControllerDelegate> delegate;

@end
