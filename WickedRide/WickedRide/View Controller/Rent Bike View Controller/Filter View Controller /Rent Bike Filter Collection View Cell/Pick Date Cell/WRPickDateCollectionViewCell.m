//
//  WRPickDateCollectionViewCell.m
//  WickedRide
//
//  Created by Ajith Kumar on 18/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRPickDateCollectionViewCell.h"
#import "NSString+DataValidator.h"


@implementation WRPickDateCollectionViewCell

-(void)awakeFromNib
{
    [super awakeFromNib];
    self.pickUpDateLabel.userInteractionEnabled = YES;
    self.dropDateLabel.userInteractionEnabled = YES;
    UITapGestureRecognizer *pickUpTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pickUpCalenderButtonAction:)];
    [self.pickUpDateLabel addGestureRecognizer:pickUpTap];
    UITapGestureRecognizer *dropTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dropCalenderButtonAction:)];
    [self.dropDateLabel addGestureRecognizer:dropTap];

}

-(void)configureViewWithSelectedDates:(NSMutableDictionary *)dataDict;
{
    if (dataDict.count == 0) {
        self.pickUpDateLabel.text = @"Pick Up Date";
        self.dropDateLabel.text = @"Drop Date";
    }
    else
    {
        self.pickUpDateLabel.text = [NSString stringWithFormat:@"%@\n%@",[[dataDict objectForKey:@"start_date"] convertDateIntoRequiredBikationString],[dataDict objectForKey:@"start_time"]];
        self.dropDateLabel.text = [NSString stringWithFormat:@"%@\n%@",[[dataDict objectForKey:@"end_date"] convertDateIntoRequiredBikationString],[dataDict objectForKey:@"end_time"]];
    }
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
}

#pragma mark - UIButtonAction Methods

- (IBAction)pickUpCalenderButtonAction:(id)sender
{
    [self.delegate pickUpBikeCalenderTapped];
}

- (IBAction)dropCalenderButtonAction:(id)sender
{
    [self.delegate dropBikeCalenderTapped];
}

@end
