//
//  WRBikationBrandCollectionViewCell.m
//  WickedRide
//
//  Created by Ajith Kumar on 12/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRBikesBrandCollectionViewCell.h"
#import <UIImageView+AFNetworking.h>
#import "UIImage+colors.h"


@interface WRBikesBrandCollectionViewCell ()
@property (weak, nonatomic) IBOutlet UIImageView *makeIconImageView;

@end

@implementation WRBikesBrandCollectionViewCell

-(void)configureBeandCellWith:(WRBikeMake *)bikeMakeObj
{
    NSString *imageUrl = [NSString stringWithFormat:@"%@",bikeMakeObj.bikeMakeLogo];
    UIImage *placeholderImg = [UIImage imageNamed:@"place_holder"];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:imageUrl]];
    
    __weak UIImageView *weakImg = self.makeIconImageView;
    
    [self.makeIconImageView setImageWithURLRequest:request placeholderImage:placeholderImg success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
     {
         
         dispatch_async(dispatch_get_main_queue(), ^{
             weakImg.image = image;
         });
         
         
     } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
         
     }];
}

@end
