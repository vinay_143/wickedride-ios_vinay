//
//  WRCommonFilterCollectionViewCell.h
//  WickedRide
//
//  Created by Ajith Kumar on 12/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface WRCommonFilterCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *headerLabel;
@property (weak, nonatomic) IBOutlet UILabel *valueLabel;
@property (weak, nonatomic) IBOutlet UIButton *dropDownButton;
@property (weak, nonatomic) IBOutlet UIImageView *dropDownImageView;

-(void)configureDropDownButtonWith:(BOOL )dropDownSelected;

@end
