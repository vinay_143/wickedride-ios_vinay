//
//  WRCommonFilterCollectionViewCell.m
//  WickedRide
//
//  Created by Ajith Kumar on 12/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRCommonFilterCollectionViewCell.h"
#import "WRSwitchView.h"
#import "WRConstants.h"
#import "UIView+FrameChange.h"
#import <QuartzCore/QuartzCore.h>

@interface WRCommonFilterCollectionViewCell () <WRSwitchViewDelegate>



@end

@implementation WRCommonFilterCollectionViewCell

-(void)awakeFromNib
{
    [super awakeFromNib];
}

-(void)configureDropDownButtonWith:(BOOL)dropDownSelected
{
    if (!dropDownSelected) {
        CABasicAnimation *rotation;
        rotation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
        rotation.fromValue = [NSNumber numberWithFloat:0];
        rotation.toValue = [NSNumber numberWithFloat:(M_PI)];
        rotation.duration = 0.2; // Speed
        rotation.repeatCount = 1; // Repeat forever. Can be a finite number.
        [self.dropDownButton.layer addAnimation:rotation forKey:@"Spin"];
        [self.dropDownButton.layer setZPosition:100];
        self.dropDownButton.transform = CGAffineTransformMakeRotation(M_PI);
    }
    else
    {
        CABasicAnimation *rotation;
        rotation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
        rotation.fromValue = [NSNumber numberWithFloat:(M_PI)];
        rotation.toValue = [NSNumber numberWithFloat:(-0)];
        rotation.duration = 0.2; // Speed
        rotation.repeatCount = 1; // Repeat forever. Can be a finite number.
        [self.dropDownButton.layer addAnimation:rotation forKey:@"Spin"];
        [self.dropDownButton.layer setZPosition:100];
        self.dropDownButton.transform = CGAffineTransformIdentity;

    }
}

#pragma mark - UIButton/Tap Action


- (IBAction)dropDownButtonAction:(id)sender
{
    
}



@end
