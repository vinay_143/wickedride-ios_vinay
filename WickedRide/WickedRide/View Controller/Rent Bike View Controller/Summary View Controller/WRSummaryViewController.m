//
//  WRSummaryViewController.m
//  WickedRide
//
//  Created by Ajith Kumar on 24/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRSummaryViewController.h"
#import "WRSummaryBikeDetailTableViewCell.h"
#import "WRSummaryAccessoryTableViewCell.h"
#import "WRSummaryTableViewCell.h"
#import "WRThankYouViewController.h"
#import "WRAccessories.h"
#import "WRPromoCodeTableViewCell.h"
#import "WRSummaryTermsTableViewCell.h"
#import "WRWebViewController.h"
#import <Razorpay/Razorpay.h>
#import "QGSdk.h"

#define convertToString(value) [NSString stringWithFormat:@"%@",value]


@interface WRSummaryViewController () <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, WRPromoCodeTableViewCellDelegate, WRSummaryTermsTableViewCellDelegate, RazorpayPaymentCompletionProtocol>{
    BOOL isviewgotBack;
    Razorpay *razorpay;
    BOOL isRedeemApplied;
    float actual_price;
}

@property (weak, nonatomic) IBOutlet UILabel *bikeLabel;
@property (weak, nonatomic) IBOutlet UILabel *daysLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *vatLabel;

@property (weak, nonatomic) IBOutlet UITableView *summaryTableView;
@property (nonatomic, strong) NSDictionary *bookingDict;
@property (nonatomic, strong) NSString *orderId;
@property (nonatomic, strong) NSString *promoCode;
@property (nonatomic, strong) NSString *addtionalComment;
@property (weak, nonatomic) IBOutlet UILabel *discountLbl;

@property (nonatomic, strong) NSMutableDictionary *termsDict;
@end

@implementation WRSummaryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.termsDict = [NSMutableDictionary dictionary];
    
    // Do any additional setup after loading the view.
    [self showNavigationViewWithBackButtonWithTitle:@"SUMMARY"];
    
    [self callWebServiceToCheckBalance];
    
    if (self.accessoriesArray.count) {
        if (self.accessoriesArray.count>1) {
            self.bikeLabel.text = [NSString stringWithFormat:@"%@ + %lu more itmes",self.selectedBike.bikeName,(unsigned long)self.accessoriesArray.count];
        }
        else
        {
            WRAccessories *accessory = [self.accessoriesArray objectAtIndex:0];
            self.bikeLabel.text = [NSString stringWithFormat:@"%@ + %@",self.selectedBike.bikeName,accessory.name];
        }
    }
    else
    {
        self.bikeLabel.text = self.selectedBike.bikeName;
    }
    razorpay = [Razorpay initWithKey:kRazorPayPublicKey andDelegate:self];

    [self.summaryTableView registerNib:[UINib nibWithNibName:kPromoCodeTableViewCellID bundle:nil] forCellReuseIdentifier:kPromoCodeTableViewCellID];
    
    self.daysLabel.text = [NSString stringWithFormat:@"%@ hours",[self.userDetailDict objectForKey:@"no_of_hours"]];
    
    NSString *totalPriceString = [NSString stringWithFormat:@"%@",[self.userDetailDict objectForKey:@"final_price"]];
    
    CGFloat price = [totalPriceString floatValue] + 0.055* [totalPriceString floatValue];
    
    [self.userDetailDict setObject:[NSString stringWithFormat:@"%0.2f",price] forKey:@"final_price"];
    
    actual_price = price;
    
    self.priceLabel.text = [NSString stringWithFormat:@"Rs. %0.2f/-",price];
    
}




#pragma mark - UITableViewDataSource, UITableViewDelegate Methods

-(NSInteger )numberOfSectionsInTableView:(UITableView *)tableView
{
    if (self.accessoriesArray.count) {
        return 5;
    }
    return 4;
}

-(NSInteger )tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

-(CGFloat )tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat ratio = 220.0/375.0;
    
    if (indexPath.section == 0) {
        return ratio*self.view.frame.size.width;
    }
    else if (indexPath.section == 1)
    {
        if (self.accessoriesArray.count) {
            return 140.0;
        }
        return 90.0;
    }
    else if (indexPath.section == 2)
    {
        if (self.accessoriesArray.count) {
            return 90.0;
        }
        return 182.0;
    }
    else if (indexPath.section == 3)
    {
        if (self.accessoriesArray.count) {
            return 75.0;
        }
        return 100.0;
    }
    return 100.0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        WRSummaryBikeDetailTableViewCell *cell = (WRSummaryBikeDetailTableViewCell *)[tableView dequeueReusableCellWithIdentifier:kSummaryBikeDetailsTableViewCellID];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell configureViewWithUserDict:self.userDetailDict andBikeObj:self.selectedBike];
        return cell;
    }
    else if (indexPath.section == 1)
    {
        if (self.accessoriesArray.count)
        {
            WRSummaryAccessoryTableViewCell *cell = (WRSummaryAccessoryTableViewCell *)[tableView dequeueReusableCellWithIdentifier:kSummaryAccessoryTableViewCellID];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            [cell configureCellWithAccessoryDetails:self.accessoriesArray];
            return cell;
        }
        WRSummaryTableViewCell *cell = (WRSummaryTableViewCell *)[tableView dequeueReusableCellWithIdentifier:kSummaryTableViewCellID];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell configureCellWithSelectedData:self.paramsDict andSelectedArea:self.selectedArea];
        return cell;
    }
    else if (indexPath.section == 2)
    {
        if (self.accessoriesArray.count)
        {
            WRSummaryTableViewCell *cell = (WRSummaryTableViewCell *)[tableView dequeueReusableCellWithIdentifier:kSummaryTableViewCellID];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            [cell configureCellWithSelectedData:self.paramsDict andSelectedArea:self.selectedArea];
            return cell;
        }
        WRPromoCodeTableViewCell *cell = (WRPromoCodeTableViewCell *)[tableView dequeueReusableCellWithIdentifier:kPromoCodeTableViewCellID];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.promoCodeTextField.delegate = self;
        cell.addtionalCommentTextField.delegate = self;
        cell.redeemTextField.delegate = self;
        cell.redeemButton.tag = indexPath.row;
        
        cell.redeemTextField.tag = 100;
        
        if([self.userDetailDict objectForKey:@"wallet_balance"] != nil)
        {
            NSString* balance = [NSString stringWithFormat:@"%@", [self.userDetailDict objectForKey:@"wallet_balance"]];
            cell.availableAmountLabel.text = balance;
        }
        
        
        cell.delegate = self;
        return cell;
    }
    else if (indexPath.section == 3)
    {
        if (self.accessoriesArray.count)
        {
            WRPromoCodeTableViewCell *cell = (WRPromoCodeTableViewCell *)[tableView dequeueReusableCellWithIdentifier:kPromoCodeTableViewCellID];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.promoCodeTextField.delegate = self;
            cell.redeemTextField.delegate = self;
            cell.redeemTextField.tag = 100;
            cell.addtionalCommentTextField.delegate = self;
            cell.redeemButton.tag = indexPath.row;

            
            NSString* balance = [NSString stringWithFormat:@"%@", [self.userDetailDict objectForKey:@"wallet_balance"]];
            cell.availableAmountLabel.text = balance;
            
            cell.delegate = self;
            return cell;
        }
        WRSummaryTermsTableViewCell *cell = (WRSummaryTermsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:kSummaryTermsTableViewCellID];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.delegate = self;
        return cell;
    }
    WRSummaryTermsTableViewCell *cell = (WRSummaryTermsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:kSummaryTermsTableViewCellID];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.delegate = self;
    return cell;
}

- (void) reloadSection:(NSInteger)section withRowAnimation:(UITableViewRowAnimation)rowAnimation {
    NSRange range = NSMakeRange(section, 1);
    NSIndexSet *sectionToReload = [NSIndexSet indexSetWithIndexesInRange:range];
    [self.summaryTableView reloadSections:sectionToReload withRowAnimation:rowAnimation];
}

#pragma mark - UIButtpm/Tap Action methods

- (IBAction)confirmButtonAction:(id)sender
{
    NSIndexPath *indexPath;
    if (self.accessoriesArray.count) {
        indexPath = [NSIndexPath indexPathForRow:0 inSection:3];
    }
    else
    {
        indexPath = [NSIndexPath indexPathForRow:0 inSection:2];
    }
    
    WRPromoCodeTableViewCell *cell = [self.summaryTableView cellForRowAtIndexPath:indexPath];
    if (cell.addtionalCommentTextField.text == nil || [cell.addtionalCommentTextField.text isEmptyString]) {
        self.addtionalComment = @"";
    }
    else
    {
        self.addtionalComment = cell.addtionalCommentTextField.text;
    }
    
    if (self.termsDict.count<3) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Please tick the checkboxes to continue" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
    }
    else
    {
        isviewgotBack = NO;
        
        float total_amount =[[self.userDetailDict objectForKey:@"final_price"] floatValue];
        
        if(total_amount>0)
        {
          [self loadPaytmPaymentGateway];
        }
        else
        {
            [self loadPaymentForZeroAmount];
        }
        
        
        NSString *promoCode= self.promoCode ? self.promoCode : @"";
        NSString *amount;
        if ([self.userDetailDict objectForKey:@"discount"]==nil || [[self.userDetailDict objectForKey:@"discount"] isEmptyString]) {
            
            amount= [self.userDetailDict objectForKey:@"final_price"];
        }
        else
        {
            amount = [self.userDetailDict objectForKey:@"discount"];
        }
        


        //Sent trnsaction to know server that how many users are confimed.///////////
        NSDictionary *sentTransaction = @{
                                        @"user_id" :self.userManager.user.userId,
                                        @"model_id":[self.paramsDict objectForKey:@"model_id"],
                                        @"area_id":[self.paramsDict objectForKey:@"area_id"],
                                        @"start_date":[self dateFormatterddMMyyyy:[self.paramsDict objectForKey:@"start_date"]],
                                        @"start_time":[self.paramsDict objectForKey:@"start_time"],
                                        @"end_date":[self dateFormatterddMMyyyy:[self.paramsDict objectForKey:@"end_date"]],
                                        @"end_time":[self.paramsDict objectForKey:@"end_time"],
                                        @"payment_gateway" : @"paytm",
                                        @"order_id" : [WRSummaryViewController generateOrderIDWithPrefix:@""],
                                        @"amount" :amount,
                                        @"promocode" : promoCode
                                        };
        
        
        [self.networkManager.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@",[[NSUserDefaults standardUserDefaults] objectForKey:kToken]] forHTTPHeaderField:@"Authorization"];
        
        [self.networkManager POST:@"https://wickedride.com/api/sent-transaction" parameters:sentTransaction success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
//            NSDictionary *responseDictionary = (NSDictionary *)responseObject;
//            NSLog(@"%@",responseDictionary);
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//            NSLog(@"%@",error);
        }];
        ////////////////////////////
    }
}

-(void)loadPaymentForZeroAmount
{
    NSMutableDictionary *bookingParams = [NSMutableDictionary dictionaryWithDictionary:@{@"start_date":convertToString([self.paramsDict objectForKey:@"start_date"]),
                                                                                         @"start_time":convertToString([self.paramsDict objectForKey:@"start_time"]),
                                                                                         @"end_date":convertToString([self.paramsDict objectForKey:@"end_date"]),
                                                                                         @"end_time":convertToString([self.paramsDict objectForKey:@"end_time"]),
                                                                                         @"model_id":convertToString([self.paramsDict objectForKey:@"model_id"]),
                                                                                         @"area_id":[self.paramsDict objectForKey:@"area_id"],
                                                                                         @"order_id": @"",
                                                                                         @"status":@"COMPLETED",
                                                                                         @"payment_method":@"",
                                                                                         @"service_provider":@""
                                                                                         }];
    [bookingParams setObject:@"0" forKey:@"total_price"];
    [bookingParams setObject:@"" forKey:@"bank_txn_id"];
    [bookingParams setObject:@"0" forKey:@"pg_txn_amount"];
    [bookingParams setObject:@"" forKey:@"pg_txn_id"];
    [bookingParams setObject:convertToString([self.userDetailDict objectForKey:@"coupon_code"]) forKey:@"coupon"];
    [bookingParams setObject:@"" forKey:@"pg_mode"];
    [bookingParams setObject:self.addtionalComment forKey:@"note"];
    
    [self postPaytmPaymentResponseToServerWith:bookingParams];

    
}

-(void)loadPaytmPaymentGateway
{
        NSString *mobile = [self.userDetailDict objectForKey:@"mobile"];
        NSString *email = [self.userDetailDict objectForKey:@"email"];
        NSInteger amout;
        NSString* wallet_amt;
    
//        if ([self.userDetailDict objectForKey:@"discount"]==nil || [[self.userDetailDict objectForKey:@"discount"] isEmptyString]) {
            amout = [[self.userDetailDict objectForKey:@"final_price"] floatValue]*100;
//        }
//        else
//        {
//            amout = [[self.userDetailDict objectForKey:@"discount"] floatValue]*100;
//        }
    
    if([self.userDetailDict objectForKey:@"applied_wallet"])
    {
        wallet_amt = [self.userDetailDict objectForKey:@"applied_wallet"];
    }
    else
    {
        wallet_amt = @"0";
    }
    
    NSDictionary *options = @{
                              @"amount": [NSString stringWithFormat:@"%ld",(long)amout], // mandatory, in paise
                              // all optional other than amount.
                              @"name": [self.userDetailDict objectForKey:@"name"],
                              @"description": @"purchase description",
                              @"prefill" : @{
                                      @"email": email,
                                      @"contact": mobile
                                      },
                              
                              @"notes" : @{@"BIKE_NAME":self.selectedBike.bikeName,
                                           @"location":self.selectedArea.areaName,
                                           @"from_date":[_paramsDict objectForKey:@"start_date"],
                                           @"to_date":[_paramsDict objectForKey:@"end_date"],
                                           @"to_time":[_paramsDict objectForKey:@"end_time"],
                                           @"price":[NSString stringWithFormat:@"%.2f",[[self.userDetailDict objectForKey:@"final_price"] floatValue]],
                                           @"wallet_amount":wallet_amt
                                      },
                              
                              @"theme": @{
                                      @"color": @"#FF4500"
                                      }
                              };
    
    [razorpay open:options];
}


+(NSString*)generateOrderIDWithPrefix:(NSString *)prefix
{
    srand ( (unsigned)time(NULL) );
    int randomNo = rand(); //just randomizing the number
    NSString *orderID = [NSString stringWithFormat:@"%@%d", prefix, randomNo];
    return orderID;
}

-(NSString*)dateFormatterddMMyyyy:(NSString *)datestring
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [dateFormat dateFromString:datestring];
    
    NSDateFormatter *systeDateFormatter = [[NSDateFormatter alloc] init];
    [systeDateFormatter setDateFormat:@"dd-MM-yyyy"];
    NSString *convertedDate = [systeDateFormatter stringFromDate:date];
    return convertedDate;
}


-(void)postPaytmPaymentResponseToServerWith:(NSMutableDictionary *)paramsDict
{
    NSLog(@"%@",[NSString stringWithFormat:@"Bearer %@",[[NSUserDefaults standardUserDefaults] objectForKey:kToken]]);
    [self loadMBProgressCustomLoaderView];
    
    [self.networkManager.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@",[[NSUserDefaults standardUserDefaults] objectForKey:kToken]] forHTTPHeaderField: @"Authorization"];
    
    AFHTTPRequestOperation *operation = [self.networkManager POST:aRentBikeBookingApi parameters:paramsDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"%@",responseObject);
        [self hideMBProgressCutomLoader];
        WRThankYouViewController *thanksVC = [self.storyboard instantiateViewControllerWithIdentifier:kThankYouViewControllerID];
        [self.navigationController pushViewController:thanksVC animated:YES];
        
        [self logEventsForBooking:true];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"%@",error.localizedDescription);
        [self hideMBProgressCutomLoader];
        WRThankYouViewController *thanksVC = [self.storyboard instantiateViewControllerWithIdentifier:kThankYouViewControllerID];
        [self.navigationController pushViewController:thanksVC animated:YES];
        
    }];
    [operation start];
    
}


#pragma mark - UITextFieldDelegate Methods

-(BOOL )textFieldShouldBeginEditing:(UITextField *)textField
{
    
    CGPoint pointInTable = [textField.superview convertPoint:textField.frame.origin toView:self.summaryTableView];
    CGPoint contentOffset = self.summaryTableView.contentOffset;
    
    contentOffset.y = (pointInTable.y - textField.inputAccessoryView.frame.size.height);
    
    NSLog(@"contentOffset is: %@", NSStringFromCGPoint(contentOffset));
    
    
    
    if(textField.tag == 100)
    {
        
        UIToolbar* keyboardDoneButtonView = [[UIToolbar alloc] init];
        [keyboardDoneButtonView sizeToFit];
        UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                       style:UIBarButtonItemStylePlain target:self
                                                                      action:@selector(doneClicked)];
        //    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:doneButton, nil]];
        
        [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                          [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                          doneButton,
                                          nil]];
        keyboardDoneButtonView.tintColor = [UIColor blackColor];
        textField.inputAccessoryView = keyboardDoneButtonView;
        
    }
    
    
   if (self.accessoriesArray.count || IS_IPHONE_5_OR_BELLOW) {
        [UIView animateWithDuration:0.2 animations:^{
            [self.view changedYAxisTo:-60];
        }];
    }
    
    return YES;
}

-(BOOL )textFieldShouldEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    [UIView animateWithDuration:0.2 animations:^{
        [self.view changedYAxisTo:64];
    }];
    return YES;
}


-(BOOL )textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    [UIView animateWithDuration:0.2 animations:^{
        [self.view changedYAxisTo:64];
    }];
    return YES;
}

-(void)doneClicked
{
    [self.view endEditing:YES];
}

#pragma mark - WRPromoCodeTableViewCellDelegate Methods

-(void)promoCodeApplyButtonTappedWith:(NSString *)promoCode
{
    [self.view endEditing:YES];
    if ([promoCode isEmptyString]) {
        if (NSClassFromString(@"UIAlertController")) {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please enter the promo code" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alertController addAction:ok];
            [self presentViewController:alertController animated:YES completion:nil];
        }
        else
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Please enter the promo code" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }

    }
    else
    {
        
        //Check if wallet balance is applied, if reset it to actual amount
        
        if([self.userDetailDict objectForKey:@"price_after_applying_amount"])
        {
           
            [self.userDetailDict removeObjectForKey:@"price_after_applying_amount"];
            
            NSString* amount = [NSString stringWithFormat:@"%0.2f",actual_price];
            [self.userDetailDict setObject:amount forKey:@"final_price"];
            
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:2];
            WRPromoCodeTableViewCell *cell = [self.summaryTableView cellForRowAtIndexPath:indexPath];
            [cell.redeemButton setTitle:@"Redeem" forState:UIControlStateNormal];
            cell.redeemTextField.userInteractionEnabled = true;
            cell.redeemTextField.text = @"";
            cell.isReedem = false;
        }
        
        
        [self applyPromoCodeToFinalPrice:promoCode];
        self.promoCode = promoCode;
    }
}

-(void)redeemButtonTappedWith:(NSString *)amount
{
    [self.view endEditing:YES];
    

    long wallet_balance =  [[self.userDetailDict objectForKey:@"wallet_balance"] longLongValue];
    long redeem_amount = [amount longLongValue];
    
    
    
    if(redeem_amount>0 && redeem_amount<=wallet_balance)
    {
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:2];
        WRPromoCodeTableViewCell *cell = [self.summaryTableView cellForRowAtIndexPath:indexPath];
        
        if(cell.isReedem == false)
        {
            
            cell.redeemTextField.text = @"";
            
           
            if([self.userDetailDict objectForKey:@"discount"])
            {
                NSString* dicount = [self.userDetailDict objectForKey:@"discount"];
                CGFloat price = [dicount floatValue] + 0.055* [dicount floatValue];
                
                self.priceLabel.text = [NSString stringWithFormat:@"Rs. %0.2f/-",price];
                
                [self.userDetailDict setObject:[NSString stringWithFormat:@"%0.2f",price] forKey:@"final_price"];
                
                NSIndexPath *index_Path = [NSIndexPath indexPathForRow:0 inSection:0];
                WRSummaryBikeDetailTableViewCell *detailCell = [self.summaryTableView cellForRowAtIndexPath:index_Path];
                detailCell.discountLabel.text =[NSString stringWithFormat:@"Rs. %@/-",dicount];
                
                
                NSAttributedString *strPrice = [[NSAttributedString alloc]initWithString:detailCell.priceLabel.text attributes:@{NSStrikethroughStyleAttributeName:@(NSUnderlineStyleSingle),NSStrikethroughColorAttributeName: [UIColor redColor]}];
                
                detailCell.priceLabel.attributedText = strPrice;

                
            }
            else
            {
                NSString* actual_amt = [NSString stringWithFormat:@"%0.2f",actual_price];
                
                self.priceLabel.text = [NSString stringWithFormat:@"%@",actual_amt];
                
                [self.userDetailDict setObject:[NSString stringWithFormat:@"%@",actual_amt] forKey:@"final_price"];
                
                NSIndexPath *index_Path = [NSIndexPath indexPathForRow:0 inSection:0];
                WRSummaryBikeDetailTableViewCell *detailCell = [self.summaryTableView cellForRowAtIndexPath:index_Path];
                detailCell.discountLabel.text = @"";
                
                detailCell.priceLabel.text = detailCell.priceLabel.text;

                
            }
          
            return;
        }
        else
        {
           [self callWebServiceToRedeemAmount: amount];
        }
     
    }
    else
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"You have entered more points than actual" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
        [self presentViewController:alertController animated:YES completion:nil];
        
    }
}

#pragma mark - Promo Code Method

-(void)applyPromoCodeToFinalPrice:(NSString *)promoCode
{
    [self loadMBProgressCustomLoaderView];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params addEntriesFromDictionary:self.paramsDict];
    [params setObject:promoCode forKey:@"code"];
    [params setObject:self.userManager.user.userId forKey:@"user_id"];
    [params setObject:[NSNumber numberWithFloat:[[self.userDetailDict objectForKey:@"total_price"] floatValue]] forKey:@"price"];
    NSMutableArray *accessoryIdArray = [NSMutableArray array];
    for (WRAccessories *accessory in self.accessoriesArray) {
        [accessoryIdArray addObject:accessory.accessoryId];
    }
    if (accessoryIdArray.count) {
        [params setObject:[accessoryIdArray componentsJoinedByString:@","] forKey:@"accessory_ids"];
    }
    [params setObject:promoCode forKey:@"code"];

    [self.networkManager startGETRequestWithAPI:aPromoCode andParameters:params withSuccess:^(NSDictionary *responseDictionary) {
        [self hideMBProgressCutomLoader];
        BOOL isSuccess = [[[[responseDictionary objectForKey:@"result"] objectForKey:@"data"] objectForKey:@"status"] boolValue];
        if (isSuccess) {
           
            NSString *totalStr = [NSString stringWithFormat:@"%@",[[[responseDictionary objectForKey:@"result"] objectForKey:@"data"] objectForKey:@"final_price"]];
            
            [self.userDetailDict setObject:totalStr forKey:@"discount"];
            
            [self.userDetailDict setObject:[[[responseDictionary objectForKey:@"result"] objectForKey:@"data"] objectForKey:@"code"] forKey:@"coupon_code"];
            
            
            CGFloat price = [totalStr floatValue] + 0.055* [totalStr floatValue];
            
            
            NSIndexPath *index_Path = [NSIndexPath indexPathForRow:0 inSection:0];
             WRSummaryBikeDetailTableViewCell *detailCell = [self.summaryTableView cellForRowAtIndexPath:index_Path];
             NSAttributedString *strPrice = [[NSAttributedString alloc]initWithString:detailCell.priceLabel.text attributes:@{NSStrikethroughStyleAttributeName:@(NSUnderlineStyleSingle),NSStrikethroughColorAttributeName: [UIColor redColor]}];
            
            detailCell.priceLabel.attributedText = strPrice;
            detailCell.discountLabel.text = [NSString stringWithFormat:@"Rs. %@/-",totalStr];
            
            
            [self.userDetailDict setObject:[NSString stringWithFormat:@"%0.2f",price] forKey:@"final_price"];
            
            self.priceLabel.text = [NSString stringWithFormat:@"Rs. %0.2f/-",price];
            
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:2];
            WRPromoCodeTableViewCell *cell = [self.summaryTableView cellForRowAtIndexPath:indexPath];
            
            cell.applyButton.hidden = true;
            cell.applyButton.userInteractionEnabled = false;
      
            
            if (NSClassFromString(@"UIAlertController")) {
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:[[[responseDictionary objectForKey:@"result"] objectForKey:@"data"] objectForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alertController addAction:ok];
                [self presentViewController:alertController animated:YES completion:nil];
            }
            else
            {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:[[[responseDictionary objectForKey:@"result"] objectForKey:@"data"] objectForKey:@"message"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alertView show];
            }

        }
        else
        {
            if (NSClassFromString(@"UIAlertController")) {
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:[[[responseDictionary objectForKey:@"result"] objectForKey:@"data"] objectForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alertController addAction:ok];
                [self presentViewController:alertController animated:YES completion:nil];
            }
            else
            {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:[[[responseDictionary objectForKey:@"result"] objectForKey:@"data"] objectForKey:@"message"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alertView show];
            }

        }
        
    } andFailure:^(NSString *errorMessage) {
        [self hideMBProgressCutomLoader];
        if (![errorMessage isEqualToString:kErrorMessage]) {
            
            if (NSClassFromString(@"UIAlertController")) {
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Something went Wrong. Please try later" preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alertController addAction:ok];
                [self presentViewController:alertController animated:YES completion:nil];
            }
            else
            {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Something went Wrong. Please try later" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alertView show];
            }

        }
        else
        {
            if (NSClassFromString(@"UIAlertController")) {
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:errorMessage preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alertController addAction:ok];
                [self presentViewController:alertController animated:YES completion:nil];
            }
            else
            {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:errorMessage delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alertView show];
            }

        }
        
    }];
}

#pragma mark - WRSummaryTermsTableViewCellDelegate Methods

-(void)ageLimitButtonTappedWithBool:(BOOL)isSelected
{
    if (isSelected) {
        [self.termsDict setObject:[NSNumber numberWithBool:isSelected] forKey:@"ageLimit"];
    }
    else
    {
        [self.termsDict removeObjectForKey:@"ageLimit"];
    }
}

-(void)drivingLicenceButtonTappedWithBool:(BOOL)isSelected
{
    if (isSelected) {
        [self.termsDict setObject:[NSNumber numberWithBool:isSelected] forKey:@"drivingLicence"];
    }
    else
    {
        [self.termsDict removeObjectForKey:@"drivingLicence"];
    }
}

-(void)termsConditionButtonTappedWithBool:(BOOL)isSelected
{
    if (isSelected) {
        [self.termsDict setObject:[NSNumber numberWithBool:isSelected] forKey:@"termsCondition"];
    }
    else
    {
        [self.termsDict removeObjectForKey:@"termsCondition"];
    }
}

-(void)termsAndCondtionButtonTapped
{
    WRWebViewController *webViewVC = [self.storyboard instantiateViewControllerWithIdentifier:kWebViewControllerID];
    webViewVC.isFromSummarryTermsVC = YES;
    [self.navigationController pushViewController:webViewVC animated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Web Service Methods

-(void)callWebServiceToCheckBalance
{
    [self loadMBProgressCustomLoaderView];
    
    
    NSString *url = [NSString stringWithFormat:@"%@",aCheckBalance];
    
    [self.networkManager.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@",[[NSUserDefaults standardUserDefaults] objectForKey:kToken]] forHTTPHeaderField: @"Authorization"];
    
    [self.networkManager startGETRequestWithAPI:url andParameters:nil withSuccess:^(NSDictionary *responseDictionary) {
        
        [self.userDetailDict setObject:[[[responseDictionary objectForKey:@"result"] objectForKey:@"data"] objectForKey:@"balance"] forKey:@"wallet_balance"];
        
        [self hideMBProgressCutomLoader];
        
        [self reloadSection:2 withRowAnimation:UITableViewRowAnimationNone];
        
    } andFailure:^(NSString *errorMessage) {
        [self hideMBProgressCutomLoader];
        if (![errorMessage isEqualToString:kErrorMessage]) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Something went Wrong. Please try later" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
        else
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:errorMessage delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
        
    }];
    
}

-(void)callWebServiceToRedeemAmount:(NSString*)amount
{
    [self loadMBProgressCustomLoaderView];
    
    [self.networkManager.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@",[[NSUserDefaults standardUserDefaults] objectForKey:kToken]] forHTTPHeaderField: @"Authorization"];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    [params setObject:[NSNumber numberWithFloat:[amount floatValue]] forKey:@"amount_to_apply"];
    
    if([self.userDetailDict objectForKey:@"discount"])
    {
        [params setObject:[NSNumber numberWithFloat:[[self.userDetailDict objectForKey:@"discount"] floatValue]] forKey:@"price"];
    }
    else
    {
        [params setObject:[NSNumber numberWithFloat:[[self.userDetailDict objectForKey:@"total_price"] floatValue]] forKey:@"price"];
    }
    
    [params setObject:@"MOBILE" forKey:@"device"];
    [params setObject:@"AtoA" forKey:@"category"];
    

    AFHTTPRequestOperation *operation = [self.networkManager POST:aApplyAmount parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"%@",responseObject);
        [self hideMBProgressCutomLoader];
        
        
        [self.userDetailDict setObject:[NSString stringWithFormat:@"%@",[[[responseObject objectForKey:@"result"] objectForKey:@"data"] objectForKey:@"applied_amount"]] forKey:@"applied_wallet"];
        
        [self.userDetailDict setObject:[[[responseObject objectForKey:@"result"] objectForKey:@"data"] objectForKey:@"price_after_applying_amount"] forKey:@"price_after_applying_amount"];
        
        NSString *totalStr = [NSString stringWithFormat:@"%@",[[[responseObject objectForKey:@"result"] objectForKey:@"data"] objectForKey:@"price_after_applying_amount"]];
      
        NSIndexPath *index_Path = [NSIndexPath indexPathForRow:0 inSection:0];
        WRSummaryBikeDetailTableViewCell *detailCell = [self.summaryTableView cellForRowAtIndexPath:index_Path];
        detailCell.discountLabel.text = [NSString stringWithFormat:@"Rs. %@/-",totalStr];
        NSAttributedString *strPrice = [[NSAttributedString alloc]initWithString:detailCell.priceLabel.text attributes:@{NSStrikethroughStyleAttributeName:@(NSUnderlineStyleSingle),NSStrikethroughColorAttributeName: [UIColor redColor]}];
        
        detailCell.priceLabel.attributedText = strPrice;
        
        CGFloat price = [totalStr floatValue] + 0.055* [totalStr floatValue];
        
        self.priceLabel.text = [NSString stringWithFormat:@"Rs. %0.2f/-",price];
        
        [self.userDetailDict setObject:[NSString stringWithFormat:@"%0.2f",price] forKey:@"final_price"];
        
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:[[[responseObject objectForKey:@"result"] objectForKey:@"data"] objectForKey:@"message"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"%@",error.localizedDescription);
        [self hideMBProgressCutomLoader];
        
    }];
    
    [operation start];
}




#pragma mark - Razor Pay Delegate methods

- (void)onPaymentSuccess:(nonnull NSString*)payment_id {
//    [[[UIAlertView alloc] initWithTitle:@"Payment Successful" message:payment_id delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    
    NSString *finalPrice = ([self.userDetailDict objectForKey:@"discount"]==nil)?[self.userDetailDict objectForKey:@"final_price"]: [self.userDetailDict objectForKey:@"discount"];
    //( pg_mode (“RazorPayAppiOS”)
    // pg_txn_id, bank_txn_id, order_id can all be the single id RazorPay returns
    NSMutableDictionary *bookingParams = [NSMutableDictionary dictionaryWithDictionary:@{@"start_date":convertToString([self.paramsDict objectForKey:@"start_date"]),
                                                                                         @"start_time":convertToString([self.paramsDict objectForKey:@"start_time"]),
                                                                                         @"end_date":convertToString([self.paramsDict objectForKey:@"end_date"]),
                                                                                         @"end_time":convertToString([self.paramsDict objectForKey:@"end_time"]),
                                                                                         @"model_id":convertToString([self.paramsDict objectForKey:@"model_id"]),
                                                                                         @"area_id":[self.paramsDict objectForKey:@"area_id"],
                                                                                         @"order_id": payment_id,
                                                                                         @"status":@"COMPLETED",
                                                                                         @"payment_method":@"RazorPayAppiOS",
                                                                                         @"service_provider":@"RazorPayAppiOS"
                                                                                         }];
    [bookingParams setObject:convertToString(finalPrice) forKey:@"total_price"];
    [bookingParams setObject:convertToString(payment_id) forKey:@"bank_txn_id"];
    [bookingParams setObject:convertToString(finalPrice) forKey:@"pg_txn_amount"];
    [bookingParams setObject:convertToString(payment_id) forKey:@"pg_txn_id"];
    [bookingParams setObject:convertToString([self.userDetailDict objectForKey:@"coupon_code"]) forKey:@"coupon"];
    [bookingParams setObject:@"RazorPayAppiOS" forKey:@"pg_mode"];
    [bookingParams setObject:self.addtionalComment forKey:@"note"];
   
    [self postPaytmPaymentResponseToServerWith:bookingParams];
}

- (void)onPaymentError:(int)code description:(nonnull NSString *)str {
    [[[UIAlertView alloc] initWithTitle:@"Error" message:str delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    
    [self logEventsForBooking:false];
}

#pragma mark - QGraphLoggingEventsForBooking

-(void)logEventsForBooking:(BOOL)isSuccess
{
    
    NSMutableDictionary *bikeDetails = [[NSMutableDictionary alloc] init];
    [bikeDetails setObject:[self.paramsDict objectForKey:@"start_date"] forKey:@"start_date"];
    [bikeDetails setObject:[self.paramsDict objectForKey:@"start_time"] forKey:@"start_time"];
    [bikeDetails setObject:[self.paramsDict objectForKey:@"end_date"] forKey:@"end_date"];
    [bikeDetails setObject:[self.paramsDict objectForKey:@"end_time"] forKey:@"end_time"];
    [bikeDetails setObject:[self.paramsDict objectForKey:@"model_id"] forKey:@"model_id"];
    
    if(isSuccess)
    {
        [bikeDetails setObject:@"success" forKey:@"status"];
        [bikeDetails setObject:@"YES" forKey:@"Zero Amount"];
        [[QGSdk getSharedInstance] logEvent:@"confirming_booking_successful" withParameters:bikeDetails];
    }
    else
    {
        [bikeDetails setObject:@"failure" forKey:@"status"];
        [bikeDetails setObject:@"NO" forKey:@"Zero Amount"];
        [[QGSdk getSharedInstance] logEvent:@"confirming_booking_unsuccessful" withParameters:bikeDetails];
    }
    
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
