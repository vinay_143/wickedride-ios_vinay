//
//  WRSummaryBikeDetailTableViewCell.h
//  WickedRide
//
//  Created by Ajith Kumar on 24/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WRBike.h"

@interface WRSummaryBikeDetailTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *bikeImageView;
@property (weak, nonatomic) IBOutlet UILabel *bikeNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *discountLabel;

-(void)configureViewWithUserDict:(NSMutableDictionary *)userInfo andBikeObj :(WRBike *)selectedBike;

@end
