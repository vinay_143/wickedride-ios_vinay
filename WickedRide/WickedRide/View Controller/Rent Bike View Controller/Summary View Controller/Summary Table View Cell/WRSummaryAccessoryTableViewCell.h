//
//  WRSummaryAccessoryTableViewCell.h
//  WickedRide
//
//  Created by Ajith Kumar on 24/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WRSummaryAccessoryTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIScrollView *accessoryScrollView;
-(void)configureCellWithAccessoryDetails:(NSMutableArray *)accessoryArray;

@end
