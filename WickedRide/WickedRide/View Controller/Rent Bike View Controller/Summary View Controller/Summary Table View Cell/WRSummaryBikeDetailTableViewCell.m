//
//  WRSummaryBikeDetailTableViewCell.m
//  WickedRide
//
//  Created by Ajith Kumar on 24/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRSummaryBikeDetailTableViewCell.h"
#import <UIImageView+AFNetworking.h>
#import "UIImage+colors.h"
#import "NSString+DataValidator.h"

@implementation WRSummaryBikeDetailTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

-(void)configureViewWithUserDict:(NSMutableDictionary *)userInfo andBikeObj :(WRBike *)selectedBike
{
    NSString *imageUrl = [selectedBike.imageDict objectForKey:@"full"];
    UIImage *placeholderImg = [UIImage imageNamed:@"place_holder"];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:imageUrl]];
    
    __weak UIImageView *weakImg = self.bikeImageView;
    
    [self.bikeImageView setImageWithURLRequest:request placeholderImage:placeholderImg success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
     {
         
         dispatch_async(dispatch_get_main_queue(), ^{
             weakImg.image = image;
         });
         
         
     } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
         
     }];
    self.bikeNameLabel.text = selectedBike.bikeName;
    NSString *totalPrice = [NSString stringWithFormat:@"%@",[userInfo objectForKey:@"total_price"]];
    self.priceLabel.text = [NSString stringWithFormat:@"Rs. %@/-",[totalPrice getFormattedPrice]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
