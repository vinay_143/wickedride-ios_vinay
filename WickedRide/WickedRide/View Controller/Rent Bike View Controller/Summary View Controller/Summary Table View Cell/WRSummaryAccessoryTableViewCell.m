//
//  WRSummaryAccessoryTableViewCell.m
//  WickedRide
//
//  Created by Ajith Kumar on 24/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRSummaryAccessoryTableViewCell.h"
#import "WRAccessoryView.h"
#import "WRConstants.h"
#import "WRAccessories.h"

@implementation WRSummaryAccessoryTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

-(void)configureCellWithAccessoryDetails:(NSMutableArray *)accessoryArray
{
    int i = 0;
    for (WRAccessories *accessory in accessoryArray) {

        WRAccessoryView *accessoryView = [[[NSBundle mainBundle] loadNibNamed:kAccessoryViewID owner:self options:nil] firstObject];
        accessoryView.frame = CGRectMake(i, 0, accessoryView.frame.size.width, accessoryView.frame.size.height);
        [accessoryView configureViewWithAccesspry:accessory];
        i+=accessoryView.frame.size.width;
        [self.accessoryScrollView addSubview:accessoryView];
    }
    self.accessoryScrollView.contentSize = CGSizeMake(i, 0);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
