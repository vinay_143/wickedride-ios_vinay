//
//  WRBikationDetailsViewController.m
//  WickedRide
//
//  Created by Ajith Kumar on 25/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRBikationDetailsViewController.h"
#import "WRBikationGeneralInfoTableViewCell.h"
#import "WRBikationGeneralDetailsTableViewCell.h"
#import "WRBikationReviewTableViewCell.h"
#import "WRBikationMoreInfoTableViewCell.h"
#import "WRMoreInfoRuleTableViewCell.h"
#import "WRBikationConfirmViewController.h"
#import "WRBikationDetails.h"
#import "WRReview.h"
#import "WRFeatureInfo.h"
#import "WRLoginViewController.h"
#import <UIImageView+AFNetworking.h>

@interface WRBikationDetailsViewController ()<WRLoginViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UIButton *backButton;

@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UILabel *bikationNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *clubNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *ratingImageView1;
@property (weak, nonatomic) IBOutlet UIImageView *ratingImageView2;
@property (weak, nonatomic) IBOutlet UIImageView *ratingImageView3;
@property (weak, nonatomic) IBOutlet UIImageView *ratingImageView4;
@property (weak, nonatomic) IBOutlet UIImageView *ratingImageView5;



@property (weak, nonatomic) IBOutlet UIView *generalView;
@property (weak, nonatomic) IBOutlet UIView *generalViewSelectionView;
@property (weak, nonatomic) IBOutlet UIView *moreInfoView;
@property (weak, nonatomic) IBOutlet UIView *moreInfoSelectionView;
@property (weak, nonatomic) IBOutlet UIView *reviewView;
@property (weak, nonatomic) IBOutlet UIView *reviewSelectionView;

@property (weak, nonatomic) IBOutlet UITableView *bikationTableView;

@property (nonatomic, strong) UIScrollView *mainScrollView;
@property (nonatomic, strong) UIView *bottomView;
@property (nonatomic, assign) BOOL isGeneralViewSelected;
@property (nonatomic, assign) BOOL isMoreInfoViewSelected;

@property (nonatomic, strong) NSArray *rideRulesArray;
@property (nonatomic, strong) WRBikationDetails *bikationDetailObj;
@end

@implementation WRBikationDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.isGeneralViewSelected = YES;
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;

    [self.bikationTableView registerNib:[UINib nibWithNibName:kBikationMoreInfoTableViewCellID bundle:nil] forCellReuseIdentifier:kBikationMoreInfoTableViewCellID];
    [self.bikationTableView registerNib:[UINib nibWithNibName:kBikationMoreInfoRuleTableViewCellID bundle:nil] forCellReuseIdentifier:kBikationMoreInfoRuleTableViewCellID];
    self.bikationTableView.scrollEnabled = NO;
    [self addGesureToViews];
    [self addContainerScrollView];
    [self addShadeToBackButton];
    
//    self.rideRulesArray = @[@"* Helmet is must for every riders",@"* Bike stunts strictly prohibited",@"* Helmet is must for every riders Bike stunts"];

    CGRect rect = [UIScreen mainScreen].bounds;
    //    float statusHeight = [[UIApplication sharedApplication] statusBarFrame].size.height;
    //    float navigationHeight = self.navigationController.navigationBar.frame.size.height;
    
    // header
    // If you're using a xib and storyboard. Be sure to specify the frame
    self.bikeImageView.frame = CGRectMake(0,0, rect.size.width, 275);
    
    // custom navigation left item
    [self configureBikationViewDetails];
    [self callWebServiceToGetBikationDetails];
    [self loadBikationImageView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}



- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = NO;
}


-(void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    self.mainScrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
}


#pragma mark - Customise/ Add gesture

-(void)addShadeToBackButton
{
    [self.backButton.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.backButton.layer setShadowOpacity:0.5];
    [self.backButton.layer setShadowRadius:2.0];
    [self.backButton.layer setShadowOffset:CGSizeMake(1.0, 1.0)];
}

-(void)addGesureToViews
{
    UITapGestureRecognizer *generalTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(generalViewTapped)];
    [self.generalView addGestureRecognizer:generalTap];
    
    UITapGestureRecognizer *moreInfoTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(moreInfoViewTapped)];
    [self.moreInfoView addGestureRecognizer:moreInfoTap];

    UITapGestureRecognizer *reviewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(reviewViewTapped)];
    [self.reviewView addGestureRecognizer:reviewTap];
}

-(void)changeBottomViewPosition
{
    self.containerView.frame = CGRectMake(0, 0, self.view.frame.size.width,470+self.bikationTableView.contentSize.height);
    self.mainScrollView.contentSize = CGSizeMake(320, self.containerView.frame.size.height);
    [self.bottomView changedYAxisTo:335+self.bikationTableView.contentSize.height];
}

-(void)addContainerScrollView
{
    [self.bikationTableView reloadData];
    self.containerView.translatesAutoresizingMaskIntoConstraints = YES;
    self.mainScrollView = [[UIScrollView alloc] initWithFrame:self.view.frame];
    self.containerView.frame = CGRectMake(0, 0, self.view.frame.size.width,470+self.bikationTableView.contentSize.height);
    self.mainScrollView.contentSize = CGSizeMake(320, self.containerView.frame.size.height);
    [self.mainScrollView addSubview:self.containerView];
    [self.view addSubview:self.mainScrollView];
    [self addRegisterButtonToView];
}

-(void)configureBikationViewDetails
{
    self.clubNameLabel.text = self.selectedBikationObj.conductorName;
    float rating = [self.selectedBikationObj.averageRating floatValue];
    if (rating == 0) {
        [self.ratingImageView1 setImage:[UIImage imageNamed:@"Rating-transparent"]];
        [self.ratingImageView2 setImage:[UIImage imageNamed:@"Rating-transparent"]];
        [self.ratingImageView3 setImage:[UIImage imageNamed:@"Rating-transparent"]];
        [self.ratingImageView4 setImage:[UIImage imageNamed:@"Rating-transparent"]];
        [self.ratingImageView5 setImage:[UIImage imageNamed:@"Rating-transparent"]];
        
    }
    else if (rating>0 && rating<=1)
    {
        [self.ratingImageView1 setImage:[UIImage imageNamed:@"Rating"]];
        [self.ratingImageView2 setImage:[UIImage imageNamed:@"Rating-transparent"]];
        [self.ratingImageView3 setImage:[UIImage imageNamed:@"Rating-transparent"]];
        [self.ratingImageView4 setImage:[UIImage imageNamed:@"Rating-transparent"]];
        [self.ratingImageView5 setImage:[UIImage imageNamed:@"Rating-transparent"]];
    }
    else if (rating>1 && rating<=2)
    {
        [self.ratingImageView1 setImage:[UIImage imageNamed:@"Rating"]];
        [self.ratingImageView2 setImage:[UIImage imageNamed:@"Rating"]];
        [self.ratingImageView3 setImage:[UIImage imageNamed:@"Rating-transparent"]];
        [self.ratingImageView4 setImage:[UIImage imageNamed:@"Rating-transparent"]];
        [self.ratingImageView5 setImage:[UIImage imageNamed:@"Rating-transparent"]];
    }
    else if (rating>2 && rating<=3)
    {
        [self.ratingImageView1 setImage:[UIImage imageNamed:@"Rating"]];
        [self.ratingImageView2 setImage:[UIImage imageNamed:@"Rating"]];
        [self.ratingImageView3 setImage:[UIImage imageNamed:@"Rating"]];
        [self.ratingImageView4 setImage:[UIImage imageNamed:@"Rating-transparent"]];
        [self.ratingImageView5 setImage:[UIImage imageNamed:@"Rating-transparent"]];
    }
    else if (rating>3 && rating<=4)
    {
        [self.ratingImageView1 setImage:[UIImage imageNamed:@"Rating"]];
        [self.ratingImageView2 setImage:[UIImage imageNamed:@"Rating"]];
        [self.ratingImageView3 setImage:[UIImage imageNamed:@"Rating"]];
        [self.ratingImageView4 setImage:[UIImage imageNamed:@"Rating"]];
        [self.ratingImageView5 setImage:[UIImage imageNamed:@"Rating-transparent"]];
    }
    else if (rating>4 && rating<=5)
    {
        [self.ratingImageView1 setImage:[UIImage imageNamed:@"Rating"]];
        [self.ratingImageView2 setImage:[UIImage imageNamed:@"Rating"]];
        [self.ratingImageView3 setImage:[UIImage imageNamed:@"Rating"]];
        [self.ratingImageView4 setImage:[UIImage imageNamed:@"Rating"]];
        [self.ratingImageView5 setImage:[UIImage imageNamed:@"Rating"]];
    }
    
}

-(void)addRegisterButtonToView
{
    self.bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, 335+self.bikationTableView.contentSize.height, self.view.frame.size.width, 135)];
    UIButton *registerButton = [[UIButton alloc] initWithFrame:CGRectMake(20, 55, self.view.frame.size.width-40, 55)];
    [registerButton addTarget:self action:@selector(registrationButtonTapped) forControlEvents:UIControlEventTouchDown];
    registerButton.backgroundColor = [UIColor blackColor];
    [registerButton setTitle:@"REGISTER NOW" forState:UIControlStateNormal];
    registerButton.titleLabel.font = MONTSERRAT_BOLD(15.0);
    registerButton.titleLabel.textColor = [UIColor whiteColor];
    [self.bottomView addSubview:registerButton];
    [self.containerView addSubview:self.bottomView];
    
}

#pragma mark - Call web service to get bikation details

-(void)callWebServiceToGetBikationDetails
{
    self.bikationTableView.hidden = YES;
    [self loadMBProgressCustomLoaderView];
    NSString *url = [NSString stringWithFormat:@"%@/%@",aBikationList,self.selectedBikationObj.bikationId];
    [self.networkManager startGETRequestWithAPI:url andParameters:nil withSuccess:^(NSDictionary *responseDictionary) {
        [self hideMBProgressCutomLoader];
        self.bikationTableView.hidden = NO;
        self.bikationDetailObj = [WRBikationDetails getBikationDetailedInformationFrom:[[responseDictionary objectForKey:@"result"] objectForKey:@"data"]];
        [self.bikationTableView reloadData];
        
    } andFailure:^(NSString *errorMessage) {
        [self hideMBProgressCutomLoader];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:errorMessage delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
    }];
    
}

-(void)loadBikationImageView
{
    NSString *imageUrl = [self.selectedBikationObj.imageDict objectForKey:@"full"];
    UIImage *placeholderImg = [UIImage imageNamed:@"place_holder"];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:imageUrl]];
    
    __weak UIImageView *weakImg = self.bikeImageView;
    
    [self.bikeImageView setImageWithURLRequest:request placeholderImage:placeholderImg success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
     {
         
         dispatch_async(dispatch_get_main_queue(), ^{
             weakImg.image = image;
         });
         
         
     } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
         
     }];

}


#pragma mark - UIButton/Tap Actions

- (IBAction)backButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)generalViewTapped
{
    self.isGeneralViewSelected = YES;
    self.isMoreInfoViewSelected = NO;
    self.generalViewSelectionView.hidden = NO;
    self.moreInfoSelectionView.hidden = YES;
    self.reviewSelectionView.hidden = YES;
    self.bikationTableView.hidden = NO;
    [self.bikationTableView reloadData];
    [self changeBottomViewPosition];
    [UIView transitionWithView:self.bikationTableView duration:0.8 options:UIViewAnimationOptionTransitionCrossDissolve animations:nil completion:nil];
}

-(void)moreInfoViewTapped
{
    self.isGeneralViewSelected = NO;
    self.isMoreInfoViewSelected = YES;
    self.generalViewSelectionView.hidden = YES;
    self.moreInfoSelectionView.hidden = NO;
    self.reviewSelectionView.hidden = YES;
    [self.bikationTableView reloadData];
    [self changeBottomViewPosition];
    [UIView transitionWithView:self.bikationTableView duration:0.8 options:UIViewAnimationOptionTransitionCrossDissolve animations:nil completion:nil];
}

-(void)reviewViewTapped
{
    self.isGeneralViewSelected = NO;
    self.isMoreInfoViewSelected = NO;
    self.generalViewSelectionView.hidden = YES;
    self.moreInfoSelectionView.hidden = YES;
    self.reviewSelectionView.hidden = NO;
    self.bikationTableView.hidden = NO;
    [self.bikationTableView reloadData];
    [self changeBottomViewPosition];
    [UIView transitionWithView:self.bikationTableView duration:0.8 options:UIViewAnimationOptionTransitionCrossDissolve animations:nil completion:nil];
}

-(void)registrationButtonTapped
{
    if (![self.userManager hasPreviousLoggedInUser])
    {
        WRLoginViewController *loginVC = [self.storyboard instantiateViewControllerWithIdentifier:kLoginViewControllerID];
        loginVC.delegate = self;
        loginVC.isFromBiking = YES;
        [self.navigationController pushViewController:loginVC animated:YES];
    }
    else
    {
        WRBikationConfirmViewController *confirmVC = [self.storyboard instantiateViewControllerWithIdentifier:kBikationConfirmViewControllerID];
        confirmVC.selectedBikationDetailObj = self.bikationDetailObj;
        confirmVC.selectedBikation = self.selectedBikationObj;
        [self.navigationController pushViewController:confirmVC animated:YES];
    }
}

#pragma mark - UITableViewDataSource, UITableViewDelegate Methods

-(NSInteger )tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.isGeneralViewSelected) {
        return 5;
    }
    if (self.isMoreInfoViewSelected) {
        return 3;
    }
    return self.bikationDetailObj.reviewArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.isGeneralViewSelected) {
        if (indexPath.row == 0|| indexPath.row == 2|| indexPath.row == 4) {
            WRBikationGeneralInfoTableViewCell *cell = (WRBikationGeneralInfoTableViewCell *)[tableView dequeueReusableCellWithIdentifier:kBikationGeneralInfoTableViewCellID];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.tag = indexPath.row;
            [cell configureViewWithBikationDetails:self.bikationDetailObj];
            return cell;
        }
        WRBikationGeneralDetailsTableViewCell *cell = (WRBikationGeneralDetailsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:kBikationGeneralDetailsTableViewCellID];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.tag = indexPath.row;
        [cell configureViewWithBikationDetails:self.bikationDetailObj];
        return cell;
    }
    else if (self.isMoreInfoViewSelected)
    {
        if (indexPath.row == 0 || indexPath.row == 1) {
            WRBikationMoreInfoTableViewCell *cell = (WRBikationMoreInfoTableViewCell *)[tableView dequeueReusableCellWithIdentifier:kBikationMoreInfoTableViewCellID];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.tag = indexPath.row;
            [cell configureViewWithBikationDetailsWithWidth:self.view.frame.size.width andBikationObj:self.bikationDetailObj];
            return cell;
        }
        WRMoreInfoRuleTableViewCell *cell = (WRMoreInfoRuleTableViewCell *)[tableView dequeueReusableCellWithIdentifier:kBikationMoreInfoRuleTableViewCellID];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.tag = indexPath.row;
        [cell configureViewWithBikationDetailsWithWidth:self.view.frame.size.width andRideRulesArray:self.bikationDetailObj.rideRules];
        return cell;
    }
    WRBikationReviewTableViewCell *cell = (WRBikationReviewTableViewCell *)[tableView dequeueReusableCellWithIdentifier:kBikationReviewsTableViewCellID];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.tag = indexPath.row;
    [cell configureReviewCellWith:[self.bikationDetailObj.reviewArray objectAtIndex:indexPath.row]];
    return cell;
}

-(CGFloat )tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.isGeneralViewSelected) {
        return 90.0;
    }
    if (self.isMoreInfoViewSelected)
    {
        if (indexPath.row == 0) {
            return [self returnInclusionAndExclutionHeight:self.bikationDetailObj.inclusionArray];
        }
        else if(indexPath.row == 1)
        {
            return [self returnInclusionAndExclutionHeight:self.bikationDetailObj.exculusionArray];
        }
            
        return [self returnRideRuleCellHeight:self.bikationDetailObj.rideRules];
    }
    return [self returnReviewDetailsSize:[self.bikationDetailObj.reviewArray objectAtIndex:indexPath.row]];
}

-(CGFloat )returnInclusionAndExclutionHeight:(NSMutableArray *)array
{
    CGFloat height = 40;
    for (int i = 0; i<array.count; i++) {
        WRFeatureInfo *featuredInfo = [array objectAtIndex:i];
        CGSize size = [self returnTextSizeOfInclusionAndExclusionText:featuredInfo.featureDescription];
        height += size.height;
    }
    return height;
}

-(CGFloat )returnRideRuleCellHeight:(NSArray *)ruleArray
{
    CGFloat height = 40;
    for (int i = 0; i<ruleArray.count; i++) {
        CGSize size = [self returnTextSize:[ruleArray objectAtIndex:i]];
        height += size.height;
    }
    return height;
}

-(CGSize )returnTextSizeOfInclusionAndExclusionText:(NSString *)featuredDescription
{
    NSDictionary *attributes = @{NSFontAttributeName: AVENIR_ROMAN(15.0)};
    NSAttributedString *attributedtext = [[NSAttributedString alloc] initWithString:featuredDescription attributes:attributes];
    
    CGRect rect = [attributedtext boundingRectWithSize:CGSizeMake(self.view.frame.size.width-110, 10000) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
    CGSize size = rect.size;
    if (size.height<25) {
        size.height =25;
    }
    return size;
}

-(CGSize )returnTextSize:(NSString *)moreInfoStr
{
    NSDictionary *attributes = @{NSFontAttributeName: AVENIR_ROMAN(15.0)};
    NSAttributedString *attributedtext = [[NSAttributedString alloc] initWithString:moreInfoStr attributes:attributes];
    
    CGRect rect = [attributedtext boundingRectWithSize:CGSizeMake(self.view.frame.size.width-85, 10000) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
    CGSize size = rect.size;
    
    return size;
}

-(CGFloat )returnReviewDetailsSize:(WRReview *)review
{
    NSDictionary *attributes = @{NSFontAttributeName:MONTSERRAT_BLACK(12.0)};
    NSAttributedString *attributedtext = [[NSAttributedString alloc] initWithString:review.reviewTitle attributes:attributes];
    
    CGRect rect = [attributedtext boundingRectWithSize:CGSizeMake(self.view.frame.size.width-110, 10000) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
    CGSize size = rect.size;
    
    NSDictionary *reviewAttributes = @{NSFontAttributeName:AVENIR_ROMAN(12.0)};
    NSAttributedString *reviewAttributedtext = [[NSAttributedString alloc] initWithString:review.review attributes:reviewAttributes];
    
    CGRect revieRect = [reviewAttributedtext boundingRectWithSize:CGSizeMake(self.view.frame.size.width-110, 10000) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
    CGSize reviewSize = revieRect.size;

    CGFloat height = size.height+reviewSize.height + 80;
    return height;
}


#pragma mark -- YSLTransitionAnimatorDataSource
- (UIImageView *)popTransitionImageView
{
    return self.bikeImageView;
}

- (UIImageView *)pushTransitionImageView
{
    return nil;
}

#pragma mark - WRLoginViewControllerDelegate Methods

-(void)loginSuccessLoadNextViewController
{
    WRBikationConfirmViewController *confirmVC = [self.storyboard instantiateViewControllerWithIdentifier:kBikationConfirmViewControllerID];
    confirmVC.selectedBikationDetailObj = self.bikationDetailObj;
    confirmVC.selectedBikation = self.selectedBikationObj;
    [self.navigationController pushViewController:confirmVC animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
