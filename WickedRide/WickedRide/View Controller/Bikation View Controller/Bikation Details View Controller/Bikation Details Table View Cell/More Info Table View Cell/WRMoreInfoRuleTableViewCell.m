//
//  WRMoreInfoRuleTableViewCell.m
//  WickedRide
//
//  Created by Ajith Kumar on 25/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRMoreInfoRuleTableViewCell.h"
#import "WRConstants.h"

@interface WRMoreInfoRuleTableViewCell ()
{
    CGFloat yAxis;
    CGFloat cellWidth;

}

@end

@implementation WRMoreInfoRuleTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configureViewWithBikationDetailsWithWidth:(CGFloat )width andRideRulesArray:(NSArray *)rulesArray
{
    cellWidth = width;
    yAxis = 50.0;
    [self.ruleImageView setImage:[UIImage imageNamed:@"ride"]];
    self.ruleTitleLabel.text = [@"ride rules:" uppercaseString];
    for (int i = 0; i<rulesArray.count; i++) {
        CGSize size = [self returnTextSize:[NSString stringWithFormat:@"* %@",[rulesArray objectAtIndex:i]]];
        [self.contentView addSubview:[self addLabelWithRuleText:[NSString stringWithFormat:@"* %@",[rulesArray objectAtIndex:i]] withYAxix:yAxis andSize:size]];
        yAxis+=size.height;
    }
}

-(UILabel *)addLabelWithRuleText:(NSString *)text withYAxix:(CGFloat )labelYAxis andSize:(CGSize )size
{
    UILabel *ruleLabel = [[UILabel alloc] initWithFrame:CGRectMake(65, labelYAxis, size.width, size.height)];
    ruleLabel.text = [NSString stringWithFormat:@"%@",text];
    ruleLabel.font = AVENIR_ROMAN(15.0);
    ruleLabel.numberOfLines = 0;
    ruleLabel.textColor = [UIColor colorWithRed:0.47 green:0.47 blue:0.47 alpha:1];
    
    return ruleLabel;
}

-(CGSize )returnTextSize:(NSString *)moreInfoStr
{
    NSDictionary *attributes = @{NSFontAttributeName: AVENIR_ROMAN(15.0)};
    NSAttributedString *attributedtext = [[NSAttributedString alloc] initWithString:moreInfoStr attributes:attributes];
    
    CGRect rect = [attributedtext boundingRectWithSize:CGSizeMake(cellWidth-85, 10000) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
    CGSize size = rect.size;
    
    return size;
}

@end
