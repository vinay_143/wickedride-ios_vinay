//
//  WRMoreInfoRuleTableViewCell.h
//  WickedRide
//
//  Created by Ajith Kumar on 25/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WRMoreInfoRuleTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *ruleImageView;
@property (weak, nonatomic) IBOutlet UILabel *ruleTitleLabel;

-(void)configureViewWithBikationDetailsWithWidth:(CGFloat )width andRideRulesArray:(NSArray *)rulesArray;

@end
