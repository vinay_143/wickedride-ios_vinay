//
//  WRBikationMoreInfoTableViewCell.h
//  WickedRide
//
//  Created by Ajith Kumar on 25/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WRBikationDetails.h"

@interface WRBikationMoreInfoTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *moreInfoImageView;
@property (weak, nonatomic) IBOutlet UILabel *moreInfoTitleLabel;

-(void)configureViewWithBikationDetailsWithWidth:(CGFloat )width andBikationObj:(WRBikationDetails *)bikationDetails;

@end
