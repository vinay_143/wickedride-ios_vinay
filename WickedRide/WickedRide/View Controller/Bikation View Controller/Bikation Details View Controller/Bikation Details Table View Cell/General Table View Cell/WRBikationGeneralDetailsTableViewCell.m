//
//  WRBikationGeneralDetailsTableViewCell.m
//  WickedRide
//
//  Created by Ajith Kumar on 25/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRBikationGeneralDetailsTableViewCell.h"
#import "NSString+DataValidator.h"

@implementation WRBikationGeneralDetailsTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

-(void)configureViewWithBikationDetails:(WRBikationDetails *)bikationDetail
{
    switch (self.tag) {
        case 1:
        {
            [self.leftImageView setImage:[UIImage imageNamed:@"date"]];
            self.leftTitleLabel.text = [@"Date" uppercaseString];
            self.leftDescriptionLabel.text = [NSString stringWithFormat:@"%@,\n(%@)",[bikationDetail.startDate convertDateIntoRequiredBikationString],bikationDetail.duration];
            [self.rightImageView setImage:[UIImage imageNamed:@"distance"]];
            self.rightTitleLabel.text = [@"Distance" uppercaseString];
            self.rightDescriptionLabel.text = bikationDetail.distance;
        }
            break;
        case 3:
        {
            [self.leftImageView setImage:[UIImage imageNamed:@"join"]];
            self.leftTitleLabel.text = [@"Joined :" uppercaseString];
            int joined = [bikationDetail.totalSlots intValue] - [bikationDetail.availableSlot intValue];
            self.leftDescriptionLabel.text = [NSString stringWithFormat:@"%d/%@",joined,bikationDetail.totalSlots];
            [self.rightImageView setImage:[UIImage imageNamed:@"meeting"]];
            self.rightTitleLabel.text = [@"Meeting At :" uppercaseString];
            self.rightDescriptionLabel.text = bikationDetail.meetingPoint;

        }
            break;
            
        default:
            break;
    }
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
