//
//  WRBikationReviewTableViewCell.m
//  WickedRide
//
//  Created by Ajith Kumar on 25/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRBikationReviewTableViewCell.h"
#import <UIImageView+AFNetworking.h>
#import "UIImage+colors.h"
#import "NSString+DataValidator.h"

@implementation WRBikationReviewTableViewCell

- (void)awakeFromNib {
    // Initialization code
    self.userImageView.layer.cornerRadius = self.userImageView.frame.size.width/2;
    self.userImageView.clipsToBounds = YES;
}

-(void)configureReviewCellWith:(WRReview *)review
{
    self.reviewTitleLabel.text = review.reviewTitle;
    self.reviewLabel.text = review.review;
    self.userNameLabel.text = [NSString stringWithFormat:@"By : %@, %@",review.reviewerName,[review.date convertDateIntoString]];
    float rating = [review.rating floatValue];
    if (rating == 0) {
        [self.ratingImageView1 setImage:[UIImage imageNamed:@"Rating-transparent"]];
        [self.ratingImageView2 setImage:[UIImage imageNamed:@"Rating-transparent"]];
        [self.ratingImageView3 setImage:[UIImage imageNamed:@"Rating-transparent"]];
        [self.ratingImageView4 setImage:[UIImage imageNamed:@"Rating-transparent"]];
        [self.ratingImageView5 setImage:[UIImage imageNamed:@"Rating-transparent"]];
        
    }
    else if (rating>0 && rating<=1)
    {
        [self.ratingImageView1 setImage:[UIImage imageNamed:@"Rating"]];
        [self.ratingImageView2 setImage:[UIImage imageNamed:@"Rating-transparent"]];
        [self.ratingImageView3 setImage:[UIImage imageNamed:@"Rating-transparent"]];
        [self.ratingImageView4 setImage:[UIImage imageNamed:@"Rating-transparent"]];
        [self.ratingImageView5 setImage:[UIImage imageNamed:@"Rating-transparent"]];
    }
    else if (rating>1 && rating<=2)
    {
        [self.ratingImageView1 setImage:[UIImage imageNamed:@"Rating"]];
        [self.ratingImageView2 setImage:[UIImage imageNamed:@"Rating"]];
        [self.ratingImageView3 setImage:[UIImage imageNamed:@"Rating-transparent"]];
        [self.ratingImageView4 setImage:[UIImage imageNamed:@"Rating-transparent"]];
        [self.ratingImageView5 setImage:[UIImage imageNamed:@"Rating-transparent"]];
    }
    else if (rating>2 && rating<=3)
    {
        [self.ratingImageView1 setImage:[UIImage imageNamed:@"Rating"]];
        [self.ratingImageView2 setImage:[UIImage imageNamed:@"Rating"]];
        [self.ratingImageView3 setImage:[UIImage imageNamed:@"Rating"]];
        [self.ratingImageView4 setImage:[UIImage imageNamed:@"Rating-transparent"]];
        [self.ratingImageView5 setImage:[UIImage imageNamed:@"Rating-transparent"]];
    }
    else if (rating>3 && rating<=4)
    {
        [self.ratingImageView1 setImage:[UIImage imageNamed:@"Rating"]];
        [self.ratingImageView2 setImage:[UIImage imageNamed:@"Rating"]];
        [self.ratingImageView3 setImage:[UIImage imageNamed:@"Rating"]];
        [self.ratingImageView4 setImage:[UIImage imageNamed:@"Rating"]];
        [self.ratingImageView5 setImage:[UIImage imageNamed:@"Rating-transparent"]];
    }
    else if (rating>4 && rating<=5)
    {
        [self.ratingImageView1 setImage:[UIImage imageNamed:@"Rating"]];
        [self.ratingImageView2 setImage:[UIImage imageNamed:@"Rating"]];
        [self.ratingImageView3 setImage:[UIImage imageNamed:@"Rating"]];
        [self.ratingImageView4 setImage:[UIImage imageNamed:@"Rating"]];
        [self.ratingImageView5 setImage:[UIImage imageNamed:@"Rating"]];
    }
    
    NSString *imageUrl = [review.reviewerImageDict objectForKey:@"full"];
    UIImage *placeholderImg = [UIImage imageNamed:@"place_holder"];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:imageUrl]];
    
    __weak UIImageView *weakImg = self.userImageView;
    
    [self.userImageView setImageWithURLRequest:request placeholderImage:placeholderImg success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
     {
         dispatch_async(dispatch_get_main_queue(), ^{
             weakImg.image = image;
         });
     } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
         
     }];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
