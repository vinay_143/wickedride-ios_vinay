//
//  WRBikationDetailsViewController.h
//  WickedRide
//
//  Created by Ajith Kumar on 25/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRBaseViewController.h"
#import "WRBikation.h"

@interface WRBikationDetailsViewController : WRBaseViewController

@property (weak, nonatomic) IBOutlet UIImageView *bikeImageView;
@property (nonatomic, strong) WRBikation *selectedBikationObj;

@end
