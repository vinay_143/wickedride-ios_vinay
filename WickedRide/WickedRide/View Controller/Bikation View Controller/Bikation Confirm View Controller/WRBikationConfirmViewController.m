//
//  WRBikationConfirmViewController.m
//  WickedRide
//
//  Created by Ajith Kumar on 26/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRBikationConfirmViewController.h"
#import "WRThankYouViewController.h"
#import <Razorpay/Razorpay.h>

@interface WRBikationConfirmViewController ()<UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate,RazorpayPaymentCompletionProtocol>
{
    Razorpay *razorpay;
}

@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *mobileTextField;
@property (weak, nonatomic) IBOutlet UITextField *bikesTextField;

@property (weak, nonatomic) IBOutlet UILabel *destinationLabel;
@property (weak, nonatomic) IBOutlet UILabel *daysLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UIView *seatsView;
@property (nonatomic, strong) UIView *blackView;
@property (nonatomic, strong) UITableView *availableSeatTableView;
@property (weak, nonatomic) IBOutlet UITextField *availableSeatTextField;

@property (nonatomic, strong) NSString *finalPriceStr;

@end

@implementation WRBikationConfirmViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.finalPriceStr = self.selectedBikationDetailObj.price;
    [self showNavigationViewWithBackButtonWithTitle:self.selectedBikation.bikationTitle];
    // Do any additional setup after loading the view.
    self.dateLabel.text = [NSString stringWithFormat:@"%@ (%@)",[self.selectedBikationDetailObj.startDate convertDateIntoRequiredBikationString],self.selectedBikationDetailObj.duration];
    self.daysLabel.text = self.selectedBikation.duration;
    self.destinationLabel.text = self.selectedBikation.bikationTitle;
    self.priceLabel.text =[NSString stringWithFormat:@"Rs. %@/-",[self.finalPriceStr getFormattedPrice]];
    
    if ([self.userManager hasPreviousLoggedInUser]) {
        self.nameTextField.text = [NSString stringWithFormat:@"%@ %@", self.userManager.user.firstName,self.userManager.user.lastName];
        self.emailTextField.text = self.userManager.user.emailId;
        if (![self.userManager.user.mobileNumber isEmptyString]) {
            self.mobileTextField.text = self.userManager.user.mobileNumber;
        }
    }
    UITapGestureRecognizer *seatTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(seatsViewTapped)];
    [self.seatsView addGestureRecognizer:seatTap];
    
    razorpay = [Razorpay initWithKey:kRazorPayPublicKey andDelegate:self];

}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
//    [self showNavigationViewWithBackButtonWithTitle:self.selectedBikation.bikationTitle];
}

#pragma mark - UITextFieldDelegate Methods

-(BOOL )textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField == self.mobileTextField || textField == self.bikesTextField)
    {
        if (IS_IPHONE_5_OR_BELLOW) {
            [UIView animateWithDuration:0.2 animations:^{
                [self.view changedYAxisTo:-100];
            }];
        }
        //        [self.delegate moveAddressScrollViewWithTextFied:textField];
        if (textField == self.mobileTextField) {
            UIToolbar* keyboardDoneButtonView = [[UIToolbar alloc] init];
            [keyboardDoneButtonView sizeToFit];
            UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                           style:UIBarButtonItemStylePlain target:self
                                                                          action:@selector(doneClicked)];
            //    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:doneButton, nil]];
            
            [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                              [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                              doneButton,
                                              nil]];
            keyboardDoneButtonView.tintColor = [UIColor blackColor];
            textField.inputAccessoryView = keyboardDoneButtonView;

        }
    }
    return YES;
}

-(BOOL )textFieldShouldEndEditing:(UITextField *)textField
{
    if (textField == self.bikesTextField)
    {
        if (IS_IPHONE_5_OR_BELLOW) {
            [UIView animateWithDuration:0.2 animations:^{
                [self.view changedYAxisTo:64];
            }];
        }
    }
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.nameTextField)
    {
        [self.emailTextField becomeFirstResponder];
    }
    else if (textField == self.emailTextField)
    {
        [self.mobileTextField becomeFirstResponder];
    }
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == self.mobileTextField) {
        NSString *resultingString = [textField.text stringByReplacingCharactersInRange: range withString: string];
        if (resultingString.length>10) {
            return NO;
        }
        else
        {
            return YES;
        }
    }
    return YES;
}


#pragma mark - UIButton/Tap Actions

-(void)seatsViewTapped
{
    [self.view endEditing:YES];
    [self addBlackView];
}

-(void )doneClicked
{
    [UIView animateWithDuration:0.2 animations:^{
        [self.view changedYAxisTo:64];
    }];
    [self.view endEditing:YES];
}

- (IBAction)bikationConfirmButtonAction:(id)sender
{
    if ([self.nameTextField.text isEmptyString]||[self.emailTextField.text isEmptyString]||[self.mobileTextField.text isEmptyString]||[self.bikesTextField.text isEmptyString]) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"All fields are Mandatory" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
    }
    else if (![self.emailTextField.text validateEmail])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Please enter valid email id" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
    }
    else if (self.mobileTextField.text.length<10)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Please enter valid mobile number" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
    }
    else
    {
        [self loadPaytmPaymentGateway];
    }
}


-(void)loadPaytmPaymentGateway
{
    NSDictionary *options = @{
                              @"amount": @"1000", // mandatory, in paise
                              // all optional other than amount.
                              @"image": @"https://url-to-image.png",
                              @"name": @"business or product name",
                              @"description": @"purchase description",
                              @"prefill" : @{
                                      @"email": @"pranav@razorpay.com",
                                      @"contact": @"8879524924"
                                      },
                              @"theme": @{
                                      @"color": @"#F37254"
                                      }
                              };
    [razorpay open:options];

//    //Step 1: Create a default merchant config object
//    PGMerchantConfiguration *mc = [PGMerchantConfiguration defaultConfiguration];
//    
//    //Step 2: If you have your own checksum generation and validation url set this here. Otherwise use the default Paytm urls
//    mc.checksumGenerationURL = kPaytmGenerateCheckSumUrl;
//    mc.checksumValidationURL =kPaytmVerifyCheckSumUrl;
//    
//    
//    //Step 3: Create the order with whatever params you want to add. But make sure that you include the merchant mandatory params
//    
//    NSMutableDictionary *orderDict = [NSMutableDictionary new];
//    //Merchant configuration in the order object
//    orderDict[@"MID"] = @"Wicked38054624802526";
//    orderDict[@"CHANNEL_ID"] = @"WAP";
//    orderDict[@"INDUSTRY_TYPE_ID"] = @"Retail";
//    orderDict[@"WEBSITE"] = @"Wickedridewap";
//    //Order configuration in the order object
//    orderDict[@"TXN_AMOUNT"] = self.finalPriceStr;
//    orderDict[@"ORDER_ID"] = [WRBikationConfirmViewController generateOrderIDWithPrefix:@""];
//    orderDict[@"REQUEST_TYPE"] = @"DEFAULT";
//    orderDict[@"CUST_ID"] = self.emailTextField.text;
//    
//    
//    PGOrder *order = [PGOrder orderWithParams:orderDict];
//    
//    //Step 4: Choose the PG server. In your production build dont call selectServerDialog. Just create a instance of the
//    //PGTransactionViewController and set the serverType to eServerTypeProduction
//    //    [PGServerEnvironment selectServerDialog:self.view completionHandler:^(ServerType type)
//    //     {
//    PGTransactionViewController *txnController = [[PGTransactionViewController alloc] initTransactionForOrder:order];
//    //         if (type != eServerTypeNone) {
//    txnController.serverType = eServerTypeStaging;
//    txnController.merchant = mc;
//    txnController.delegate = self;
//    [self showController:txnController];
    //         }
    //     }];

}

#pragma mark Payment test methods


-(void)showController:(PGTransactionViewController *)controller
{
    if (self.navigationController != nil)
        [self.navigationController pushViewController:controller animated:YES];
    else
        [self presentViewController:controller animated:YES
                         completion:^{
                             
                         }];
}

-(void)removeController:(PGTransactionViewController *)controller
{
    if (self.navigationController != nil)
        [self.navigationController popViewControllerAnimated:YES];
    else
        [controller dismissViewControllerAnimated:YES
                                       completion:^{
                                       }];
}

+(NSString*)generateOrderIDWithPrefix:(NSString *)prefix
{
    srand ( (unsigned)time(NULL) );
    int randomNo = rand(); //just randomizing the number
    NSString *orderID = [NSString stringWithFormat:@"%@%d", prefix, randomNo];
    return orderID;
}


#pragma mark PGTransactionDelegate Methods

#pragma mark PGTransactionViewController delegate

- (void)didSucceedTransaction:(PGTransactionViewController *)controller
                     response:(NSDictionary *)response
{
    DEBUGLOG(@"ViewController::didSucceedTransactionresponse= %@", response);
    NSString *title = [NSString stringWithFormat:@"Your order  was completed successfully. \n %@", response[@"ORDERID"]];
    [[[UIAlertView alloc] initWithTitle:title message:[response description] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    
    [self removeController:controller];
}

- (void)didFailTransaction:(PGTransactionViewController *)controller error:(NSError *)error response:(NSDictionary *)response
{
    DEBUGLOG(@"ViewController::didFailTransaction error = %@ response= %@", error, response);
    if (response)
    {
        [[[UIAlertView alloc] initWithTitle:error.localizedDescription message:[response description] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    }
    else if (error)
    {
        [[[UIAlertView alloc] initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    }
    [self removeController:controller];
}

- (void)didCancelTransaction:(PGTransactionViewController *)controller error:(NSError*)error response:(NSDictionary *)response
{
//    DEBUGLOG(@"ViewController::didCancelTransaction error = %@ response= %@", error, response);
//    NSString *msg = nil;
//    if (!error) msg = [NSString stringWithFormat:@"Successful"];
//    else msg = [NSString stringWithFormat:@"UnSuccessful"];
    
//    [[[UIAlertView alloc] initWithTitle:@"Transaction Cancelled" message:@"" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    [self removeController:controller];
}

- (void)didFinishCASTransaction:(PGTransactionViewController *)controller response:(NSDictionary *)response
{
    DEBUGLOG(@"ViewController::didFinishCASTransaction:response = %@", response);
}



#pragma mark - Black View methods

-(void)addBlackView
{
    self.blackView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    self.blackView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.2];
    
    self.availableSeatTableView = [[UITableView alloc] initWithFrame:CGRectMake(40, 40, 100, self.view.frame.size.height - 80)];
    if ([self.selectedBikationDetailObj.availableSlot integerValue]*30 >self.view.frame.size.height - 80) {
        self.availableSeatTableView.scrollEnabled = YES;
    }
    else
    {
        [self.availableSeatTableView changeHeightTo:[self.selectedBikationDetailObj.availableSlot integerValue]*30];
        self.availableSeatTableView.scrollEnabled = NO;
    }
    self.availableSeatTableView.center = self.blackView.center;
    self.availableSeatTableView.dataSource = self;
    self.availableSeatTableView.delegate = self;
    self.availableSeatTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.blackView addSubview:self.availableSeatTableView];
    UIButton *closeButton = [[UIButton alloc] initWithFrame:CGRectMake(self.blackView.frame.size.width-40, 0, 40, 40)];
    [closeButton setImage:[UIImage imageNamed:@"cross"] forState:UIControlStateNormal];
    [closeButton addTarget:self action:@selector(blackViewTapped) forControlEvents:UIControlEventTouchDown];
    [self.blackView addSubview:closeButton];
    [self.view addSubview:self.blackView];
    [self.availableSeatTableView reloadData];
}

-(void)removeBlackView
{
    [self.blackView removeFromSuperview];
    self.blackView = nil;
    self.availableSeatTableView = nil;
}


-(void)blackViewTapped
{
    [self.blackView removeFromSuperview];
    self.availableSeatTableView = nil;
    self.blackView = nil;
}

#pragma mark - UITableViewDataSource, UITableViewDelegate Methods

-(NSInteger )tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.selectedBikationDetailObj.availableSlot integerValue];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    cell.textLabel.text = [NSString stringWithFormat:@"%ld",indexPath.row+1];
    cell.textLabel.font = AVENIR_BOOK(12.0);
    UIView *seperator = [[UIView alloc] initWithFrame:CGRectMake(20, 28, tableView.frame.size.width-40, 1)];
    seperator.backgroundColor = [UIColor lightGrayColor];
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell.contentView addSubview:seperator];
    return cell;
}

-(CGFloat )tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 30;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.availableSeatTextField.text = [NSString stringWithFormat:@"%ld",(long)indexPath.row+1];
    NSInteger noOfSeats = indexPath.row+1;
    NSInteger price = [self.selectedBikationDetailObj.price integerValue];
    NSInteger finalPrice = noOfSeats*price;
    self.finalPriceStr = [NSString stringWithFormat:@"%ld",(long)finalPrice];
    self.priceLabel.text =[NSString stringWithFormat:@"Rs. %@/-",[self.finalPriceStr getFormattedPrice]];
    [self removeBlackView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Razor Pay Delegate methods

- (void)onPaymentSuccess:(nonnull NSString*)payment_id {
    [[[UIAlertView alloc] initWithTitle:@"Payment Successful" message:payment_id delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
}

- (void)onPaymentError:(int)code description:(nonnull NSString *)str {
    [[[UIAlertView alloc] initWithTitle:@"Error" message:str delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
