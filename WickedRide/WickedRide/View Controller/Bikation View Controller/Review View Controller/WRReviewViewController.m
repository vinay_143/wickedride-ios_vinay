//
//  WRReviewViewController.m
//  WickedRide
//
//  Created by Ajith kumar on 12/29/15.
//  Copyright © 2015 Inkoniq. All rights reserved.
//

#import "WRReviewViewController.h"

@interface WRReviewViewController ()<UITextViewDelegate, UITextFieldDelegate>
{
    NSInteger rating;
}
@property (weak, nonatomic) IBOutlet UITextField *reviewTitleTextField;
@property (weak, nonatomic) IBOutlet UIButton *ratingButton1;
@property (weak, nonatomic) IBOutlet UIButton *ratingButton2;
@property (weak, nonatomic) IBOutlet UIButton *ratingButton3;
@property (weak, nonatomic) IBOutlet UIButton *ratingButton4;
@property (weak, nonatomic) IBOutlet UIButton *ratingButton5;
@property (weak, nonatomic) IBOutlet UITextView *reviewTextView;

@end

@implementation WRReviewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadCustomNavigationViewWithTitle:@"Write a Review"];
    rating =0;
    self.reviewTextView.autocorrectionType = UITextAutocorrectionTypeNo;
    self.reviewTextView.layer.cornerRadius = 5.0;
    self.reviewTextView.clipsToBounds = YES;
    self.reviewTextView.layer.borderWidth = 1.0;
    self.reviewTextView.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    self.reviewTitleTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.reviewTitleTextField.layer.cornerRadius = 3.0;
    self.reviewTitleTextField.clipsToBounds = YES;
    self.reviewTitleTextField.layer.borderWidth = 1.0;
    self.reviewTitleTextField.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    self.reviewTitleTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Review Title" attributes:@{NSForegroundColorAttributeName: [UIColor blackColor]}];

    // Do any additional setup after loading the view.
}

#pragma mark - UIButton/ Tap action

- (IBAction)ratingButton1Action:(id)sender
{
    rating = 1;
    [self.ratingButton1 setImage:[UIImage imageNamed:@"Rating"] forState:UIControlStateNormal];
    [self.ratingButton2 setImage:[UIImage imageNamed:@"Rating-transparent"] forState:UIControlStateNormal];
    [self.ratingButton3 setImage:[UIImage imageNamed:@"Rating-transparent"] forState:UIControlStateNormal];
    [self.ratingButton4 setImage:[UIImage imageNamed:@"Rating-transparent"] forState:UIControlStateNormal];
    [self.ratingButton5 setImage:[UIImage imageNamed:@"Rating-transparent"] forState:UIControlStateNormal];
}

- (IBAction)ratingButton2Action:(id)sender
{
    rating = 2;
    [self.ratingButton1 setImage:[UIImage imageNamed:@"Rating"] forState:UIControlStateNormal];
    [self.ratingButton2 setImage:[UIImage imageNamed:@"Rating"] forState:UIControlStateNormal];
    [self.ratingButton3 setImage:[UIImage imageNamed:@"Rating-transparent"] forState:UIControlStateNormal];
    [self.ratingButton4 setImage:[UIImage imageNamed:@"Rating-transparent"] forState:UIControlStateNormal];
    [self.ratingButton5 setImage:[UIImage imageNamed:@"Rating-transparent"] forState:UIControlStateNormal];
}

- (IBAction)ratingButton3Action:(id)sender
{
    rating = 3;
    [self.ratingButton1 setImage:[UIImage imageNamed:@"Rating"] forState:UIControlStateNormal];
    [self.ratingButton2 setImage:[UIImage imageNamed:@"Rating"] forState:UIControlStateNormal];
    [self.ratingButton3 setImage:[UIImage imageNamed:@"Rating"] forState:UIControlStateNormal];
    [self.ratingButton4 setImage:[UIImage imageNamed:@"Rating-transparent"] forState:UIControlStateNormal];
    [self.ratingButton5 setImage:[UIImage imageNamed:@"Rating-transparent"] forState:UIControlStateNormal];
}

- (IBAction)ratingButton4Action:(id)sender
{
    rating = 4;
    [self.ratingButton1 setImage:[UIImage imageNamed:@"Rating"] forState:UIControlStateNormal];
    [self.ratingButton2 setImage:[UIImage imageNamed:@"Rating"] forState:UIControlStateNormal];
    [self.ratingButton3 setImage:[UIImage imageNamed:@"Rating"] forState:UIControlStateNormal];
    [self.ratingButton4 setImage:[UIImage imageNamed:@"Rating"] forState:UIControlStateNormal];
    [self.ratingButton5 setImage:[UIImage imageNamed:@"Rating-transparent"] forState:UIControlStateNormal];
}

- (IBAction)ratingButton5Action:(id)sender
{
    rating = 5;
    [self.ratingButton1 setImage:[UIImage imageNamed:@"Rating"] forState:UIControlStateNormal];
    [self.ratingButton2 setImage:[UIImage imageNamed:@"Rating"] forState:UIControlStateNormal];
    [self.ratingButton3 setImage:[UIImage imageNamed:@"Rating"] forState:UIControlStateNormal];
    [self.ratingButton4 setImage:[UIImage imageNamed:@"Rating"] forState:UIControlStateNormal];
    [self.ratingButton5 setImage:[UIImage imageNamed:@"Rating"] forState:UIControlStateNormal];
}

-(void)doneClicked
{
    [self.view endEditing:YES];
}

- (IBAction)sendButtonAction:(id)sender
{
    if ([self.reviewTitleTextField.text isEmptyString]) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Please enter review title" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
    }
    else if ([self.reviewTextView.text isEmptyString]) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Review cannot be empty" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
    }
    else if (rating == 0)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Rating cannot be empty" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
    }
    else
    {
        [self postReviewToServer];
    }
}


#pragma mark - Call web service to post review

-(void)postReviewToServer
{
    [self.view endEditing:YES];
    [self loadMBProgressCustomLoaderView];
    NSDictionary *params = @{@"rating":[NSNumber numberWithInteger:rating],
                             @"review_title":self.reviewTitleTextField.text,
                             @"review_text":self.reviewTextView.text};
    [self.networkManager.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@",[[NSUserDefaults standardUserDefaults] objectForKey:kToken]] forHTTPHeaderField:@"Authorization"];

    [self.networkManager POST:aReviewOnBikation(self.selectedBikationObj.bikationId) parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self hideMBProgressCutomLoader];
        [self dismissViewControllerAnimated:YES completion:nil];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self hideMBProgressCutomLoader];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:error.localizedDescription delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
    }];
}

#pragma mark - UITextViewDelegate Methods

-(BOOL )textViewShouldBeginEditing:(UITextView *)textView
{
    UIToolbar* keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                   style:UIBarButtonItemStylePlain target:self
                                                                  action:@selector(doneClicked)];
    //    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:doneButton, nil]];
    
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                      [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                      doneButton,
                                      nil]];
    keyboardDoneButtonView.tintColor = [UIColor blackColor];
    textView.inputAccessoryView = keyboardDoneButtonView;
    return YES;
}

-(BOOL )textViewShouldEndEditing:(UITextView *)textView
{
    [textView resignFirstResponder];
    return YES;
}

#pragma mark - UITextFieldDelegate Methods

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
