//
//  WRLoginViewController.m
//  WickedRide
//
//  Created by Ajith Kumar on 14/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRLoginViewController.h"
#import "WRSignUpViewController.h"
#import "WRForgotPasswordViewController.h"
#import "WRotpViewController.h"
#import "QGSdk.h"

@interface WRLoginViewController () <UITextFieldDelegate, WRSignUpViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UILabel *signUpLabel;
@property (weak, nonatomic) IBOutlet UITextField *userNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UILabel *forgotPasswordLabel;

@end

@implementation WRLoginViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self showNavigationViewWithBackButtonWithTitle:@""];
    self.userNameTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    [self configureViewPropertyAndAddGestures];
    if (IS_IPHONE_4) {
        [self addContainerScrollView];
    }
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(otpRegistrationIsSuccessfull)
                                                 name:kDismissLoginView
                                               object:nil];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;

}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = YES;
}


-(void)configureViewPropertyAndAddGestures
{
    UITapGestureRecognizer *forgotPasswordTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(forgotPasswordLabelTapped)];
    [self.forgotPasswordLabel addGestureRecognizer:forgotPasswordTap];
    UITapGestureRecognizer *signUpTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(signUpButtonTapped)];
    [self.signUpLabel addGestureRecognizer:signUpTap];
}


-(void)addContainerScrollView
{
    self.containerView.translatesAutoresizingMaskIntoConstraints = YES;
    UIScrollView *mainScrollView = [[UIScrollView alloc] initWithFrame:self.view.frame];
    self.containerView.frame = CGRectMake(0, 0, self.view.frame.size.width, 425.0);
    mainScrollView.contentSize = CGSizeMake(320, 480.0);
    [mainScrollView addSubview:self.containerView];
    [self.view addSubview:mainScrollView];
    [self.view bringSubviewToFront:self.customNavView];
}



#pragma mark - UIButton/ Tap Actions

-(void)signUpButtonTapped
{
    WRSignUpViewController *signUpVC = [self.storyboard instantiateViewControllerWithIdentifier:kSignUpViewControllerID];
    signUpVC.delegate = self;
    [self presentViewController:signUpVC animated:YES completion:nil];
}

-(void)forgotPasswordLabelTapped
{
    WRForgotPasswordViewController *forgotPasswordVC = [self.storyboard instantiateViewControllerWithIdentifier:kForgorPasswordViewControllerID];
    [self.navigationController presentViewController:forgotPasswordVC animated:YES completion:nil];
}

- (IBAction)signInButtonAction:(id)sender
{
    [self.view endEditing:YES];
    if ([self.userNameTextField.text isEmptyString] || [self.passwordTextField.text isEmptyString]) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Both the fields are mandatory" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
    }
    else if(![self.userNameTextField.text validateEmail])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Please enter valid email id" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
    }
    else
    {
        NSDictionary *params = @{@"email":self.userNameTextField.text,
                                 @"password":self.passwordTextField.text};
        [self loginUserWithUrl:params];
    }
}


-(void)loginUserWithUrl:(NSDictionary *)params
{
    [self loadMBProgressCustomLoaderView];
    
    [self.networkManager startPOSTRequestWithAPI:aGetLoginDetails andParameters:params withSuccess:^(NSDictionary *responseDictionary) {

        [self hideMBProgressCutomLoader];
        NSString *messageStr = [[responseDictionary objectForKey:@"result"] objectForKey:@"message"];
        if ([messageStr isEqualToString:@"Success!"]) {
            
            self.userManager.user = [WRUser getLoggedInUserValueWith:[[responseDictionary objectForKey:@"result"] objectForKey:@"data"]];
            [[NSUserDefaults standardUserDefaults] setObject:[[[responseDictionary objectForKey:@"result"] objectForKey:@"data"] objectForKey:@"token"] forKey:kToken];
            [[NSUserDefaults standardUserDefaults] setObject:[[[responseDictionary objectForKey:@"result"] objectForKey:@"data"] objectForKey:@"referral_code"] forKey:@kReferelCode];
            
            if (self.isFromBiking)
            {
                [self.navigationController popViewControllerAnimated:NO];
                [self.delegate loginSuccessLoadNextViewController];
            }
            else
            {
                [self.navigationController popViewControllerAnimated:YES];
            }
            
         
            [[QGSdk getSharedInstance] setFirstName:[[[responseDictionary objectForKey:@"result"] objectForKey:@"data"] objectForKey:@"first_name"]];
            
            [[QGSdk getSharedInstance] setEmail:[[[responseDictionary objectForKey:@"result"] objectForKey:@"data"] objectForKey:@"email"]];
            [[QGSdk getSharedInstance] setCustomKey:@"user_token" withValue:[[[responseDictionary objectForKey:@"result"] objectForKey:@"data"] objectForKey:@"token"]];
            [[QGSdk getSharedInstance] setCustomKey:@"phoneNo" withValue:[[[responseDictionary objectForKey:@"result"] objectForKey:@"data"] objectForKey:@"mobile"]];
          
           

            [self logEventForSigninSuccessWithUserName:[[[responseDictionary objectForKey:@"result"] objectForKey:@"data"] objectForKey:@"first_name"] userEmail:[[[responseDictionary objectForKey:@"result"] objectForKey:@"data"] objectForKey:@"email"] userMobileNo:[[[responseDictionary objectForKey:@"result"] objectForKey:@"data"] objectForKey:@"mobile"]];
            

            
            
        }
        else
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:messageStr delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
        
    } andFailure:^(NSString *errorMessage) {
        [self hideMBProgressCutomLoader];
        if (![errorMessage isEqualToString:kErrorMessage]) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Something went Wrong. Please try later" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
        else
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:errorMessage delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - QGraph log signin_success

-(void)logEventForSigninSuccessWithUserName:(NSString *)name userEmail:(NSString *)mail userMobileNo:(NSString *)mobileNo
{
    NSMutableDictionary *userDetails = [[NSMutableDictionary alloc] init];
    [userDetails setObject:name forKey:@"firts Name"];
    [userDetails setObject:mail forKey:@"email"];
    [userDetails setObject:mobileNo forKey:@"mobile"];

 [[QGSdk getSharedInstance] logEvent:@"signin_success" withParameters:userDetails];

    
}

#pragma mark - UITextFieldDelegate Methods

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (IS_IPHONE_4) {
        if (textField == self.passwordTextField) {
            [UIView animateWithDuration:0.2 animations:^{
                [self.containerView changedYAxisTo:-80];
            }];
        }
    }
    return YES;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [UIView animateWithDuration:0.2 animations:^{
        [self.containerView changedYAxisTo:0];
    }];
    if (textField == self.userNameTextField) {
        [self.passwordTextField becomeFirstResponder];
    }
    else
    {
//        [self signInButtonAction:self];
        [textField resignFirstResponder];
    }
    return YES;
}

#pragma mark - WRSignUpViewControllerDelegate Method

-(void)registrationIsSuccessfull
{
    if (self.isFromBiking) {
        [self.navigationController popViewControllerAnimated:NO];
        [self.delegate loginSuccessLoadNextViewController];
    }
    else
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(void)otpRegistrationIsSuccessfull
{
    [self.navigationController popViewControllerAnimated:YES];

    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center removeObserver:self name:kDismissLoginView object:nil];

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
