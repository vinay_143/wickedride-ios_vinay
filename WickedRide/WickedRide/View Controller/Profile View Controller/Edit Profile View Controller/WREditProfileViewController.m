//
//  WREditProfileViewController.m
//  WickedRide
//
//  Created by Ajith Kumar on 10/09/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WREditProfileViewController.h"
#import "WRCropImageViewController.h"
#import "WRResetViewController.h"
#import "WRCalenderPopUpView.h"
#import <UIImageView+AFNetworking.h>

@interface WREditProfileViewController ()<UITextFieldDelegate, UIActionSheetDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate, WRCropImageViewControllerDelegate, WRCalenderViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;
@property (weak, nonatomic) IBOutlet UIButton *addImageButton;
@property (weak, nonatomic) IBOutlet UIView *profileView;
@property (weak, nonatomic) IBOutlet UITextField *firstNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *lastNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *mobileTextField;
@property (weak, nonatomic) IBOutlet UITextField *birthdayTextField;
@property (weak, nonatomic) IBOutlet UIView *birthdayView;

@property (nonatomic, strong) WRCalenderPopUpView *calenderView;
@property (nonatomic, strong) UIView *blackView;

@property (nonatomic, strong) NSString *selectedDob;

@end

@implementation WREditProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self loadCustomNavigationViewWithTitle:[@"Edit Profile" uppercaseString]];
    [self configureViewsProperty];
    [self addContainerScrollView];
    [self loadViewWithUserDetails];
    UITapGestureRecognizer *birthdayTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(birthdayViewTapped)];
    [self.birthdayView addGestureRecognizer:birthdayTap];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}


-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
//    self.navigationController.navigationBarHidden = NO;
}

-(void)loadViewWithUserDetails
{
    
    self.firstNameTextField.text = self.userManager.user.firstName;
    self.lastNameTextField.text = self.userManager.user.lastName;
    self.emailTextField.text = self.userManager.user.emailId;
    if (![self.userManager.user.mobileNumber isEmptyString]) {
        self.mobileTextField.text = self.userManager.user.mobileNumber;
    }
    if (![self.userManager.user.dob isEmptyString]) {
        self.birthdayTextField.text = [self.userManager.user.dob convertDateIntoRequiredBikationString];
        self.selectedDob = [self.userManager.user.dob convertDateIntoStringForBirthdayDate];
    }
    if (![self.userManager.user.profileImageUrl isEmptyString]) {
        NSString *imageUrl = self.userManager.user.profileImageUrl;
        UIImage *placeholderImg = [UIImage imageNamed:@"user_profile"];
        
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:imageUrl]];
        
        __weak UIImageView *weakImg = self.profileImageView;
        
        [self.profileImageView setImageWithURLRequest:request placeholderImage:placeholderImg success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
         {
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 weakImg.image = image;
             });
             
             
         } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
             
         }];
        
    }

}

-(void)addContainerScrollView
{
    self.containerView.translatesAutoresizingMaskIntoConstraints = YES;
    UIScrollView *mainScrollView = [[UIScrollView alloc] initWithFrame:self.view.frame];
    self.containerView.frame = CGRectMake(0, 0, self.view.frame.size.width, 740.0);
    mainScrollView.contentSize = CGSizeMake(320, 740.0);
    [mainScrollView addSubview:self.containerView];
    [self.view addSubview:mainScrollView];
    [self.view bringSubviewToFront:self.customNavView];
}

-(void)configureViewsProperty
{
    UITapGestureRecognizer *profileTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(profileViewTapped)];
    [self.profileView addGestureRecognizer:profileTap];
    
    self.profileImageView.layer.cornerRadius = self.profileImageView.frame.size.width/2;
    self.profileImageView.clipsToBounds = YES;

}

#pragma mark UIButton/ Tap Actions

- (IBAction)updateUserAccounDetailsButtonAction:(id)sender
{
    if ([self.firstNameTextField.text isEmptyString]|| [self.emailTextField.text isEmptyString]) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Please fill madatory fields" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
    }
    else if (![self.emailTextField.text validateEmail])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Please enter valid email id" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
    }
    else if (![self.mobileTextField.text isEmptyString] && self.mobileTextField.text.length<10)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Please enter valid mobile number" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
    }
    else
    {
        NSMutableDictionary *params =[NSMutableDictionary dictionaryWithDictionary:@{@"first_name":self.firstNameTextField.text
                                                                                     }];
        if (![self.lastNameTextField.text isEmptyString]) {
            [params setObject:self.lastNameTextField.text forKey:@"last_name"];
        }
        if (![self.mobileTextField.text isEmptyString]) {
            [params setObject:self.mobileTextField.text forKey:@"mobile"];
        }
        if (![self.birthdayTextField.text isEmptyString]) {
            [params setObject:self.selectedDob forKey:@"dob"];
        }
//        [params setObject:self.profileImageView.image forKey:@"profile_image"];
        [self callWebServiceToUpdateUserInformation:params];
    }
}

-(void)callWebServiceToUpdateUserInformation:(NSDictionary *)params
{
    
    [self loadMBProgressCustomLoaderView];    
    [self.networkManager.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@",[[NSUserDefaults standardUserDefaults] objectForKey:kToken]] forHTTPHeaderField:@"Authorization"];
    
    NSData *imageData = UIImageJPEGRepresentation(self.profileImageView.image, 1);
    
    AFHTTPRequestOperation *operation = [self.networkManager POST:aProfileDetails parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileData:imageData name:@"profile_image" fileName:@"photo.png" mimeType:@"image/png"];
        
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *responseDictionary = (NSDictionary *)responseObject;
        NSString *messageStr = [[responseDictionary objectForKey:@"result"] objectForKey:@"message"];
        if ([messageStr isEqualToString:@"Success!"]) {
            self.userManager.user = [WRUser getLoggedInUserValueWith:[[responseDictionary objectForKey:@"result"] objectForKey:@"data"]];
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Updated Successfully" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
            [self dismissViewControllerAnimated:YES completion:nil];
        }
        else
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:messageStr delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
        [self hideMBProgressCutomLoader];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self hideMBProgressCutomLoader];
        if (![error.localizedDescription isEqualToString:kErrorMessage]) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Something went Wrong. Please try later" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
        else
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:error.localizedDescription delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
    }];
    [operation start];

    
}

- (IBAction)addProfileImageButtonAction:(id)sender
{
    [self profileViewTapped];
}

- (IBAction)changePasswordButtonAction:(id)sender
{
    WRResetViewController *resetVC = [self.storyboard instantiateViewControllerWithIdentifier:kResetViewControllerID];
    [self.navigationController pushViewController:resetVC animated:YES];
}

-(void)doneClicked
{
    [self.view endEditing:YES];
}

-(void)birthdayViewTapped
{
    [self.view endEditing:YES];
    [self addBlackView];
    dispatch_async(dispatch_get_main_queue(), ^{
        self.calenderView = [[[NSBundle mainBundle] loadNibNamed:kCalendarViewID owner:nil options:nil] objectAtIndex:0];
        self.calenderView.frame = CGRectMake(0, 0, 300, 256);
        self.calenderView.center = self.view.center;
        self.calenderView.delegate = self;
        [self.calenderView configureViewWithType:@"Calender"];
        [self viewZoomInEffect:self.calenderView];
        [self.view addSubview:self.calenderView];
    });
}

#pragma mark - Black View methods

-(void)addBlackView
{
    if (!self.blackView)
    {
        self.blackView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        self.blackView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.2];
        UITapGestureRecognizer *blackViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(blackViewTapped)];
        [self.blackView addGestureRecognizer:blackViewTap];
        [self.view addSubview:self.blackView];
    }
    else
    {
        self.blackView.hidden = NO;
    }
}

-(void)removeBlackView
{
    [self.blackView removeFromSuperview];
    self.blackView = nil;
}


-(void)blackViewTapped
{
    [self.blackView removeFromSuperview];
    self.blackView = nil;
    [self viewZoomOutEffect:self.calenderView];
    self.calenderView = nil;
}


-(void)profileViewTapped
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Select a Photo" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
                                  @"Camera",
                                  @"Gallery",
                                  nil];
    [actionSheet showInView:self.view];
}

#pragma mark UIActionSheetDelegate Method

- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (buttonIndex) {
        case 0:
            [self loadImageFromCamera];
            break;
        case 1:
            [self loadImageFromGallery];
            break;
        default:
            break;
    }
}

-(void)loadImageFromCamera
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePicker.mediaTypes = @[(NSString *) kUTTypeImage];
        imagePicker.allowsEditing = NO;
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
    
}

-(void)loadImageFromGallery
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
        imagePicker.sourceType =  UIImagePickerControllerSourceTypePhotoLibrary;
        imagePicker.mediaTypes = @[(NSString *) kUTTypeImage];
        imagePicker.allowsEditing = NO;
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
}

#pragma mark - UINavigationControllerDelegate / UIImagePickerControllerDelegate Methods

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *mediaType = info[UIImagePickerControllerMediaType];
    [self dismissViewControllerAnimated:YES completion:nil];
    if ([mediaType isEqualToString:(NSString *) kUTTypeImage]) {
        UIImage *image = info[UIImagePickerControllerOriginalImage];
        WRCropImageViewController *cropImageVc = [self.storyboard instantiateViewControllerWithIdentifier:kCropImageViewControllerID];
        cropImageVc.selectedImage = image;
        cropImageVc.delegate = self;
        [self presentViewController:cropImageVc animated:YES completion:nil];
    }
}

#pragma mark - ALCropImageViewControllerDelegate Methods

-(void)imageAfterCroping:(UIImage *)croppedImage
{
    self.profileImageView.image = croppedImage;
}

#pragma mark - UITextFieldDelegate Methods

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField == self.mobileTextField)
    {
        [self.containerView changedYAxisTo:-100];
        UIToolbar* keyboardDoneButtonView = [[UIToolbar alloc] init];
        [keyboardDoneButtonView sizeToFit];
        UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                       style:UIBarButtonItemStylePlain target:self
                                                                      action:@selector(doneClicked)];
        //    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:doneButton, nil]];
        
        [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                          [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                          doneButton,
                                          nil]];
        keyboardDoneButtonView.tintColor = [UIColor blackColor];
        textField.inputAccessoryView = keyboardDoneButtonView;
    }
    return YES;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    if (textField == self.mobileTextField) {
        [self.containerView changedYAxisTo:0];
    }
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.containerView changedYAxisTo:0];
    if (textField == self.firstNameTextField) {
        [self.lastNameTextField becomeFirstResponder];
    }
    else if (textField == self.lastNameTextField) {
        [self.mobileTextField becomeFirstResponder];
    }
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == self.mobileTextField) {
        NSString *resultingString = [textField.text stringByReplacingCharactersInRange: range withString: string];
        if (resultingString.length>10) {
            return NO;
        }
        else
        {
            return YES;
        }
    }
    return YES;
}


#pragma mark - WRCalenderViewDelegate Methods

-(void)doneButtonTappedWithSelectedDate:(NSString *)dateStr andDate:(NSDate *)date
{
    [self blackViewTapped];
    self.birthdayTextField.text = [dateStr convertDateIntoRequiredBikationString];
    self.selectedDob = [dateStr convertDateIntoStringForBirthdayDate];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
