//
//  WRSignUpViewController.m
//  WickedRide
//
//  Created by Ajith Kumar on 14/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRSignUpViewController.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "WRCropImageViewController.h"
#import "WRCalenderPopUpView.h"
#import "QGSdk.h"
#import "WROTPVerificationViewController.h"
#import "WRotpViewController.h"


@interface WRSignUpViewController () <UITextFieldDelegate, UIActionSheetDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate, WRCropImageViewControllerDelegate,WRCalenderViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;
@property (weak, nonatomic) IBOutlet UIButton *addImageButton;
@property (weak, nonatomic) IBOutlet UIView *profileView;
@property (weak, nonatomic) IBOutlet UITextField *firstNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *lastNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *birthdayTextField;
@property (weak, nonatomic) IBOutlet UITextField *mobileTextField;
@property (weak, nonatomic) IBOutlet UITextField *referrelTextField;

@property (weak, nonatomic) IBOutlet UIView *birthdayView;
@property (weak, nonatomic) IBOutlet UITextField *confirmPasswordTextField;
@property (weak, nonatomic) IBOutlet UILabel *signInLabel;

@property (nonatomic, strong) WRCalenderPopUpView *calenderView;
@property (nonatomic, strong) UIView *blackView;
@property (nonatomic, strong) NSString *selectedDob;

@property (nonatomic, assign) BOOL isProfilePicSelected;

@end

@implementation WRSignUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    [self loadCustomNavigationViewWithTitle:@"SIGN UP"];
    [self configureViewProperty];
    [self addContainerScrollView];
}


-(void)addContainerScrollView
{
    self.containerView.translatesAutoresizingMaskIntoConstraints = YES;
    UIScrollView *mainScrollView = [[UIScrollView alloc] initWithFrame:self.view.frame];
    self.containerView.frame = CGRectMake(0, 0, self.view.frame.size.width, 950.0);
    mainScrollView.contentSize = CGSizeMake(320, 1000.0);
//    mainScrollView.backgroundColor = [UIColor cyanColor];
//    self.containerView.backgroundColor = [UIColor purpleColor];
    [mainScrollView addSubview:self.containerView];
    [self.view addSubview:mainScrollView];
    [self.view bringSubviewToFront:self.customNavView];
}

-(void)configureViewProperty
{
    UITapGestureRecognizer *profileTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(profileViewTapped)];
    [self.profileView addGestureRecognizer:profileTap];
    
    self.profileImageView.layer.cornerRadius = self.profileImageView.frame.size.width/2;
    self.profileImageView.clipsToBounds = YES;
    
    UITapGestureRecognizer *signInTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(signInLabelTapped)];
    [self.signInLabel addGestureRecognizer:signInTap];
    
    UITapGestureRecognizer *birthdayTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(birthdayViewTapped)];
    [self.birthdayView addGestureRecognizer:birthdayTap];

}

#pragma mark UIButton/ Tap Actions

- (IBAction)createAccounButtonAction:(id)sender
{
    
    if ([self.firstNameTextField.text isEmptyString]|| [self.emailTextField.text isEmptyString] || [self.passwordTextField.text isEmptyString]|| [self.confirmPasswordTextField.text isEmptyString]) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Please fill all mandatory fields" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
    }
    else if (![self.emailTextField.text validateEmail])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Please enter valid email id" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
    }
    else if (![self.mobileTextField.text isEmptyString] && self.mobileTextField.text.length<10)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Please enter valid mobile number" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
    }
    else if (self.passwordTextField.text.length<5)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Password must be at least 5 characters" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
    }
    else if (![self.passwordTextField.text isEqualToString:self.confirmPasswordTextField.text])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Password mismatch" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
    }
    else
    {
        NSMutableDictionary *params =[NSMutableDictionary dictionaryWithDictionary:@{@"first_name":self.firstNameTextField.text,
                                 @"email":self.emailTextField.text,
                                 @"password":self.passwordTextField.text,
                                 @"confirm_password":self.confirmPasswordTextField.text,
                                 @"app_version":[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]
                                                                                     
                                                                                     }];
        if (![self.lastNameTextField.text isEmptyString]) {
            [params setObject:self.lastNameTextField.text forKey:@"last_name"];
        }
        if (![self.mobileTextField.text isEmptyString]) {
            [params setObject:self.mobileTextField.text forKey:@"mobile"];
        }
        if (![self.birthdayTextField.text isEmptyString]) {
            [params setObject:self.birthdayTextField.text forKey:@"dob"];
        }
        if (![self.referrelTextField.text isEmptyString]) {
            [params setObject:self.referrelTextField.text forKey:@"referral_code"];
        }
        
        [self callWebServiceToPostSignUpData:params];
    }


}

- (IBAction)addProfileImageButtonAction:(id)sender
{
    [self profileViewTapped];
}



-(void)doneClicked
{
    [self.view endEditing:YES];
}


-(void)signInLabelTapped
{
    [self.view endEditing:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)birthdayViewTapped
{
    [self.view endEditing:YES];
    [self addBlackView];
    dispatch_async(dispatch_get_main_queue(), ^{
        self.calenderView = [[[NSBundle mainBundle] loadNibNamed:kCalendarViewID owner:nil options:nil] objectAtIndex:0];
        self.calenderView.frame = CGRectMake(0, 0, 300, 256);
        self.calenderView.center = self.view.center;
        self.calenderView.delegate = self;
        [self.calenderView configureViewWithType:@"Calender"];
        [self viewZoomInEffect:self.calenderView];
        [self.view addSubview:self.calenderView];
    });
}


#pragma mark - Black View methods

-(void)addBlackView
{
    if (!self.blackView)
    {
        self.blackView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        self.blackView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.2];
        UITapGestureRecognizer *blackViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(blackViewTapped)];
        [self.blackView addGestureRecognizer:blackViewTap];
        [self.view addSubview:self.blackView];
    }
    else
    {
        self.blackView.hidden = NO;
    }
}

-(void)removeBlackView
{
    [self.blackView removeFromSuperview];
    self.blackView = nil;
}


-(void)blackViewTapped
{
    [self.blackView removeFromSuperview];
    self.blackView = nil;
    [self viewZoomOutEffect:self.calenderView];
    self.calenderView = nil;
}

-(void)profileViewTapped
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Select a Photo" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
                                  @"Camera",
                                  @"Gallery",
                                  nil];
    [actionSheet showInView:self.view];
}

#pragma mark UIActionSheetDelegate Method

- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (buttonIndex) {
        case 0:
            [self loadImageFromCamera];
            break;
        case 1:
            [self loadImageFromGallery];
            break;
        default:
            break;
    }
}

-(void)loadImageFromCamera
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePicker.mediaTypes = @[(NSString *) kUTTypeImage];
        imagePicker.allowsEditing = NO;
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
    
}

-(void)loadImageFromGallery
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
        imagePicker.sourceType =  UIImagePickerControllerSourceTypePhotoLibrary;
        imagePicker.mediaTypes = @[(NSString *) kUTTypeImage];
        imagePicker.allowsEditing = NO;
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
}

#pragma mark - UINavigationControllerDelegate / UIImagePickerControllerDelegate Methods

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *mediaType = info[UIImagePickerControllerMediaType];  
    [self dismissViewControllerAnimated:YES completion:nil];
    if ([mediaType isEqualToString:(NSString *) kUTTypeImage]) {
        UIImage *image = info[UIImagePickerControllerOriginalImage];
        WRCropImageViewController *cropImageVc = [self.storyboard instantiateViewControllerWithIdentifier:kCropImageViewControllerID];
        cropImageVc.selectedImage = image;
        cropImageVc.delegate = self;
        [self presentViewController:cropImageVc animated:NO completion:nil];
    }
}

#pragma mark - ALCropImageViewControllerDelegate Methods

-(void)imageAfterCroping:(UIImage *)croppedImage
{
    self.profileImageView.image = croppedImage;
    self.isProfilePicSelected = YES;
}


#pragma mark - UITextFieldDelegate Methods

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
//    UIToolbar* keyboardDoneButtonView = [[UIToolbar alloc] init];
//    [keyboardDoneButtonView sizeToFit];
//    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
//                                                                   style:UIBarButtonItemStylePlain target:self
//                                                                  action:@selector(doneClicked)];
//    //    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:doneButton, nil]];
//    
//    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
//                                      [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
//                                      doneButton,
//                                      nil]];
//    keyboardDoneButtonView.tintColor = [UIColor blackColor];
   
    
 //   textField.inputAccessoryView = keyboardDoneButtonView;
//    if (textField == self.mobileTextField) {
//        [self.view changedYAxisTo:-100];
//    }
//    else if (textField == self.referrelTextField)
//    {
//        [self.view changedYAxisTo:-160];
//    }
    return YES;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [self.view changedYAxisTo:0];
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view changedYAxisTo:0];
    if (textField == self.firstNameTextField) {
        [self.lastNameTextField becomeFirstResponder];
    }
    else if (textField == self.lastNameTextField)
    {
        [self.emailTextField becomeFirstResponder];
    }
    else if (textField == self.emailTextField)
    {
        [self.passwordTextField becomeFirstResponder];
    }
    else if (textField == self.passwordTextField)
    {
        [self.confirmPasswordTextField becomeFirstResponder];
    }
    else if (textField == self.confirmPasswordTextField)
    {
        [textField resignFirstResponder];
    }
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == self.mobileTextField) {
        NSString *resultingString = [textField.text stringByReplacingCharactersInRange: range withString: string];
        if (resultingString.length>10) {
            return NO;
        }
        else
        {
            return YES;
        }
    }
//    else if (textField == self.passwordTextField || textField == self.confirmPasswordTextField)
//    {
//        if ([string rangeOfCharacterFromSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].location != NSNotFound) {
//            return NO;
//        }
//        return YES;
//    }
    return YES;
}



#pragma mark - Post User info to server

-(void)callWebServiceToPostSignUpData:(NSDictionary *)params
{
    [self loadMBProgressCustomLoaderView];
    
    __weak WRSignUpViewController *weakself = self;
    [self.networkManager startPOSTRequestWithAPI:aRegistration andParameters:params withSuccess:^(NSDictionary *responseDictionary) {
        [weakself hideMBProgressCutomLoader];
        NSString *messageStr = [[responseDictionary objectForKey:@"result"] objectForKey:@"message"];
        if ([messageStr isEqualToString:@"Success!"])
        {
           
            
        [self uploadUserProfilePicToserver:self.profileImageView.image];
            
            [self logEventForRegistrationSuccessWithUserName:[params valueForKey:@"first_name"] userEmail:[params valueForKey:@"email"] userMobileNo:[params valueForKey:@"mobile"]];
        
            
            WRotpViewController *otpVC = [self.storyboard instantiateViewControllerWithIdentifier:@"WRotpViewController"];
            otpVC.userDetails = params;
            [self presentViewController:otpVC animated:YES completion:nil];

            
        }
        else
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:messageStr delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
        
    } andFailure:^(NSString *errorMessage) {
        [weakself hideMBProgressCutomLoader];
        if (![errorMessage isEqualToString:kErrorMessage]) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Something went Wrong. Please try later" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
        else
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:errorMessage delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
    }];
}

-(void)uploadUserProfilePicToserver:(UIImage *)profileImage
{
    [self loadMBProgressCustomLoaderView];
    [self.networkManager.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@",[[NSUserDefaults standardUserDefaults] objectForKey:kToken]] forHTTPHeaderField:@"Authorization"];

    NSData *imageData = UIImageJPEGRepresentation(profileImage, 1);
    
    AFHTTPRequestOperation *operation = [self.networkManager POST:aProfileDetails parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        [formData appendPartWithFileData:imageData name:@"profile_image" fileName:@"photo.png" mimeType:@"image/png"];
    } success:^(AFHTTPRequestOperation *operation, id responseObject)
    {
        NSDictionary *responseDictionary = (NSDictionary *)responseObject;
       
        [self hideMBProgressCutomLoader];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self hideMBProgressCutomLoader];
//        [self dismissViewControllerAnimated:YES completion:^{
//            [self.delegate registrationIsSuccessfull];
//        }];
    }];
    [operation start];
    
}

#pragma mark - QGraph log registration_success

-(void)logEventForRegistrationSuccessWithUserName:(NSString *)name userEmail:(NSString *)mail userMobileNo:(NSString *)mobileNo
{
    
    NSMutableDictionary *userDetails = [[NSMutableDictionary alloc] init];
    [userDetails setObject:name forKey:@"firts Name"];
    [userDetails setObject:mail forKey:@"email"];
    [userDetails setObject:mobileNo forKey:@"mobile"];
    
    [[QGSdk getSharedInstance] logEvent:@"registration_success" withParameters:userDetails];
    
    
}



#pragma mark - WRCalenderViewDelegate Methods

-(void)doneButtonTappedWithSelectedDate:(NSString *)dateStr andDate:(NSDate *)date
{
    [self blackViewTapped];
    self.birthdayTextField.text = [dateStr convertDateIntoRequiredBikationString];
    self.selectedDob = [dateStr convertDateIntoStringForBirthdayDate];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
