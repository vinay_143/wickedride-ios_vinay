//
//  WRotpViewController.h
//  WickedRide
//
//  Created by Boyapati Vinay Kumar on 27/06/17.
//  Copyright © 2017 Inkoniq. All rights reserved.
//

#import "WRBaseViewController.h"
@protocol WROtpViewControllerDelegate <NSObject>

@optional

-(void)otpRegistrationIsSuccessfull;

@end
@interface WRotpViewController : WRBaseViewController
@property (weak, nonatomic) IBOutlet UIButton *resendButton;

@property (weak, nonatomic) IBOutlet UIView *otpBackView;

@property (weak, nonatomic) IBOutlet UITextField *otptextone;
@property (weak, nonatomic) IBOutlet UITextField *otptexttwo;
@property (weak, nonatomic) IBOutlet UITextField *otptextthree;
@property (weak, nonatomic) IBOutlet UITextField *otptextfour;
@property (strong,nonatomic) NSString *PhoneNumber;
@property (strong,nonatomic) NSDictionary *userDetails;
@property (weak, nonatomic) IBOutlet UILabel *otpHeaderLbl;
@property (nonatomic) int otpNumber;
@property (strong,nonatomic) NSString *enterdOtp;

@property (nonatomic, weak) id <WROtpViewControllerDelegate> delegate;

@end
