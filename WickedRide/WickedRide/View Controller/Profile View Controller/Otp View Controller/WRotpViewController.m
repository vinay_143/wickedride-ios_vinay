//
//  WRotpViewController.m
//  WickedRide
//
//  Created by Boyapati Vinay Kumar on 27/06/17.
//  Copyright © 2017 Inkoniq. All rights reserved.
//

#import "WRotpViewController.h"
#import <IQKeyboardManager.h>

@interface WRotpViewController ()<UITextFieldDelegate>

@end

@implementation WRotpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setOtpHeaderAndTileLabels:YES];
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:false];

    
    _otpBackView.layer.borderWidth = 1.0f;
    _otpBackView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _otpBackView.clipsToBounds =YES;
    
    
    _resendButton.layer.borderWidth = 1.0f;
    _resendButton.layer.cornerRadius = 3.0f;
    _resendButton.layer.borderColor = [UIColor grayColor].CGColor;
    _resendButton.clipsToBounds =YES;
    _resendButton.enabled = YES;
    
    [self setPlaceHolder];
    
    
    _otptextone.delegate= self;
    _otptexttwo.delegate = self;
    _otptextthree.delegate = self;
    _otptextfour.delegate = self;
    
    _otptextone.tintColor = [UIColor clearColor];
    _otptexttwo.tintColor = [UIColor clearColor];
    _otptextthree.tintColor = [UIColor clearColor];
    _otptextfour.tintColor = [UIColor clearColor];
    
    [_otptextone becomeFirstResponder];
    [_otptextfour addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];

  //  [self forgotPasswordWebServiceMethod];
    // Do any additional setup after loading the view.
}
-(void)setPlaceHolder
{
    
    UIColor *color = [UIColor blackColor];
    self.otptextone.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"-" attributes:@{NSForegroundColorAttributeName: color}];
    self.otptexttwo.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"-" attributes:@{NSForegroundColorAttributeName: color}];
    self.otptextthree.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"-" attributes:@{NSForegroundColorAttributeName: color}];
    self.otptextfour.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"-" attributes:@{NSForegroundColorAttributeName: color}];
    
    
}
-(void)viewWillDisappear:(BOOL)animated
{
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:true];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - textfield Delegate methods
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    
    if ((textField.text.length == 1) && (string.length > 0))
    {
        
        NSInteger nextTag = textField.tag + 1;
        // Try to find next responder
        UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
        //        if (! nextResponder)
        //            nextResponder = [textField.superview viewWithTag:1];
        
        if (nextResponder)
            // Found next responder, so set it.
            [nextResponder becomeFirstResponder];
        
        return YES;
    }
    else if ((textField.text.length <= 1) && (string.length <= 0))
    {
        NSInteger nextTag = textField.tag - 1;
        // Try to find next responder
        UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
        if (! nextResponder)
            nextResponder = [textField.superview viewWithTag:1];
        
        if (nextResponder)
            // Found next responder, so set it.
            [nextResponder becomeFirstResponder];
        textField.text = @"";
        
        return NO;
        
        
    }
    return YES;
    
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSLog(@"i ma in should return");
    
    if (textField.text.length == 0) {
        
        NSLog(@"text value zer0");
    }
    
    [textField resignFirstResponder];
    return YES;
}
-(void)textFieldDidChange :(UITextField *)theTextField{
    NSLog( @"text changed: %@", theTextField.text);
    
    NSString *finalenterdOtp = [NSString stringWithFormat:@"%@%@%@%@",_otptextone.text,_otptexttwo.text,_otptextthree.text,_otptextfour.text];
    [theTextField resignFirstResponder];
    
    
    
    
NSDictionary *params = @{@"email":[_userDetails valueForKey:@"email"],
                             @"password":[_userDetails valueForKey:@"password"],
                             @"mobile":[_userDetails valueForKey:@"mobile"],
                         @"otp":finalenterdOtp};
    //[self sendOtpToVerify:params];
    [self sendOtpToVerify:finalenterdOtp];
    //[self forgotPasswordWebServiceMethod];
    
    
}

-(void)sendOtpToVerify:(NSString *)otp {
    
    [self loadMBProgressCustomLoaderView];
    __weak WRotpViewController *weakself = self;


    //Init the NSURLSession with a configuration
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    //Create an URLRequest
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",Base_URL,aVerifyOtp]];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    
    //Create POST Params and add it to HTTPBody
    NSString *params = [NSString stringWithFormat:@"email=%@&password=%@&mobile=%@&otp=%@",[_userDetails valueForKey:@"email"],[_userDetails valueForKey:@"password"],[_userDetails valueForKey:@"mobile"],otp];
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
    
    //Create task
    
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
    {
       
        if (!error) {
            //Handle your response here
            NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
            
            BOOL status = [[[[responseDictionary objectForKey:@"result"] objectForKey:@"data"] objectForKey:@"status"] boolValue];
            if (status)
            {
                [weakself hideMBProgressCutomLoader];

                self.userManager.user = [WRUser getLoggedInUserValueWith:[[responseDictionary objectForKey:@"result"] objectForKey:@"data"]];
                [[NSUserDefaults standardUserDefaults] setObject:[[[responseDictionary objectForKey:@"result"] objectForKey:@"data"] objectForKey:@"token"] forKey:kToken];
            
                [self.presentingViewController.presentingViewController dismissViewControllerAnimated:YES completion:nil];
                NSNotification *myNotification = [NSNotification notificationWithName:kDismissLoginView
                                                                               object:self
                                                                             userInfo:nil];
                [[NSNotificationCenter defaultCenter] postNotification:myNotification];
            }
            else
            {
                NSString *messageStr = [[[responseDictionary objectForKey:@"result"] objectForKey:@"data"] objectForKey:@"message"] ;
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:messageStr delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alertView show];
                [self setOtpHeaderAndTileLabels:NO];
                [weakself hideMBProgressCutomLoader];
                

            }
        }
        else
        {
            if (![error.localizedDescription isEqualToString:kErrorMessage]) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Something went Wrong. Please try later" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alertView show];
                [weakself hideMBProgressCutomLoader];

                [self setOtpHeaderAndTileLabels:NO];
                
            }
            else
            {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:error.localizedDescription delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alertView show];
                [weakself hideMBProgressCutomLoader];

                [self setOtpHeaderAndTileLabels:NO];
                
            }
        
        }
       
    }];
    [dataTask resume];
}

/*
-(void)sendOtpToVerify:(NSDictionary *)params
{
     [self loadMBProgressCustomLoaderView];
    
      __weak WRotpViewController *weakself = self;
    [self.networkManager startPOSTRequestWithAPI:aVerifyOtp andParameters:params withSuccess:^(NSDictionary *responseDictionary)
    {
        [weakself hideMBProgressCutomLoader];
        BOOL status = [[[[responseDictionary objectForKey:@"result"] objectForKey:@"data"] objectForKey:@"status"] boolValue];
        if (status)
        {
            self.userManager.user = [WRUser getLoggedInUserValueWith:[[responseDictionary objectForKey:@"result"] objectForKey:@"data"]];
        [[NSUserDefaults standardUserDefaults] setObject:[[[responseDictionary objectForKey:@"result"] objectForKey:@"data"] objectForKey:@"token"] forKey:kToken];
    NSNotification *myNotification = [NSNotification notificationWithName:kDismissLoginView
                                                                                       object:self
                                                                                     userInfo:nil];
                [[NSNotificationCenter defaultCenter] postNotification:myNotification];
        }
        else
        {
            NSString *messageStr = [[[responseDictionary objectForKey:@"result"] objectForKey:@"data"] objectForKey:@"message"] ;
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:messageStr delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
            [self setOtpHeaderAndTileLabels:NO];
      
        }

    } andFailure:^(NSString *errorMessage) {
        [weakself hideMBProgressCutomLoader];
        if (![errorMessage isEqualToString:kErrorMessage]) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Something went Wrong. Please try later" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
            [self setOtpHeaderAndTileLabels:NO];

        }
        else
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:errorMessage delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
            [self setOtpHeaderAndTileLabels:NO];

        }
    }];
}
 */

- (IBAction)resendOtpToTheUser:(id)sender {
    [self loadMBProgressCustomLoaderView];
    
    __weak WRotpViewController *weakself = self;
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            [_userDetails valueForKey:@"email"], @"email",
                            [_userDetails valueForKey:@"mobile"], @"mobile",
                            nil];
    
    [self.networkManager startPOSTRequestWithAPI:aResendOtp andParameters:params withSuccess:^(NSDictionary *responseDictionary) {
       [weakself hideMBProgressCutomLoader];
        BOOL status = [[[[responseDictionary objectForKey:@"result"] objectForKey:@"data"] objectForKey:@"status"] boolValue];
        if (status)
        {
           
            NSString *messageStr = [[[responseDictionary objectForKey:@"result"] objectForKey:@"data"] objectForKey:@"message"] ;
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:messageStr delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
        else
        {
           NSString *messageStr = [[[responseDictionary objectForKey:@"result"] objectForKey:@"data"] objectForKey:@"message"] ;
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:messageStr delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
        
    } andFailure:^(NSString *errorMessage) {
        [weakself hideMBProgressCutomLoader];
        if (![errorMessage isEqualToString:kErrorMessage]) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Something went Wrong. Please try later" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
        else
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:errorMessage delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
    }];
}


-(void)forgotPasswordWebServiceMethod
{
    [self loadMBProgressCustomLoaderView];
    NSDictionary *params = @{@"email":@"vinay.k@wickedride.com"};
    [self.networkManager startGETRequestWithAPI:aForgotPassword andParameters:params withSuccess:^(NSDictionary *responseDictionary) {
        [self hideMBProgressCutomLoader];
        int status = [[[responseDictionary objectForKey:@"result"] objectForKey:@"status_code"] intValue];
        NSString *msgStr = [[responseDictionary objectForKey:@"result"] objectForKey:@"data"];
        if (status == 200) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Email Sent" message:@"Please check your email to continue" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
            [self dismissViewControllerAnimated:YES completion:nil];
        }
        else
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:msgStr delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    } andFailure:^(NSString *errorMessage) {
        [self hideMBProgressCutomLoader];
        if (![errorMessage isEqualToString:kErrorMessage]) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Something went Wrong. Please try later" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
        else
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:errorMessage delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
    }];
}


-(void)setOtpHeaderAndTileLabels:(BOOL)isVerified
{
    
    
    if (isVerified)
    {
        _otpHeaderLbl.textColor = [UIColor blackColor];
        _otpHeaderLbl.text = @"Thanks! Please enter the OTP";
        
        
        
    }
    else
    {
        
        _otpHeaderLbl.textColor = [UIColor blackColor];
        _otpHeaderLbl.text = @"Incorrect OTP entered";
        
        _otptextone.text = @"";
        _otptexttwo.text = @"";
        _otptextthree.text = @"";
        _otptextfour.text = @"";
        [_otptextone becomeFirstResponder];
        
        
        
    }
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
