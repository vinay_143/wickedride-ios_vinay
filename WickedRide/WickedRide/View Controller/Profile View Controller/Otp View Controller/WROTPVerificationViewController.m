//
//  WROTPVerificationViewController.m
//  WickedRide
//
//  Created by Boyapati Vinay Kumar on 27/06/17.
//  Copyright © 2017 Inkoniq. All rights reserved.
//

#import "WROTPVerificationViewController.h"
#import "WRSignUpViewController.h"

@interface WROTPVerificationViewController ()<UITextFieldDelegate>

@end

@implementation WROTPVerificationViewController


-(void)viewDidLoad
{
    
    [self setOtpHeaderAndTileLabels:YES];
    
    _otpBackView.layer.borderWidth = 1.0f;
    _otpBackView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _otpBackView.clipsToBounds =YES;
    
    
    _resendButton.layer.borderWidth = 1.0f;
    _resendButton.layer.cornerRadius = 3.0f;
    _resendButton.layer.borderColor = [UIColor grayColor].CGColor;
    _resendButton.clipsToBounds =YES;
    _resendButton.enabled = YES;
    
    [self setPlaceHolder];
    
    
    _otptextone.delegate= self;
    _otptexttwo.delegate = self;
    _otptextthree.delegate = self;
    _otptextfour.delegate = self;
    
    _otptextone.tintColor = [UIColor clearColor];
    _otptexttwo.tintColor = [UIColor clearColor];
    _otptextthree.tintColor = [UIColor clearColor];
    _otptextfour.tintColor = [UIColor clearColor];
    
    [_otptextone becomeFirstResponder];
    [_otptextfour addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    
    
}

-(void)setPlaceHolder
{
    
    UIColor *color = [UIColor blackColor];
    self.otptextone.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"-" attributes:@{NSForegroundColorAttributeName: color}];
    self.otptexttwo.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"-" attributes:@{NSForegroundColorAttributeName: color}];
    self.otptextthree.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"-" attributes:@{NSForegroundColorAttributeName: color}];
    self.otptextfour.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"-" attributes:@{NSForegroundColorAttributeName: color}];
    
    
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    
    if ((textField.text.length == 1) && (string.length > 0))
    {
        
        NSInteger nextTag = textField.tag + 1;
        // Try to find next responder
        UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
        //        if (! nextResponder)
        //            nextResponder = [textField.superview viewWithTag:1];
        
        if (nextResponder)
            // Found next responder, so set it.
            [nextResponder becomeFirstResponder];
        
        return YES;
    }
    else if ((textField.text.length <= 1) && (string.length <= 0))
    {
        NSInteger nextTag = textField.tag - 1;
        // Try to find next responder
        UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
        if (! nextResponder)
            nextResponder = [textField.superview viewWithTag:1];
        
        if (nextResponder)
            // Found next responder, so set it.
            [nextResponder becomeFirstResponder];
        textField.text = @"";
        
        return NO;
        
        
    }
    return YES;
    
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSLog(@"i ma in should return");
    
    if (textField.text.length == 0) {
        
        NSLog(@"text value zer0");
    }
    
    [textField resignFirstResponder];
    return YES;
}
-(void)textFieldDidChange :(UITextField *)theTextField{
    NSLog( @"text changed: %@", theTextField.text);
    
    NSString *finalenterdOtp = [NSString stringWithFormat:@"%@%@%@%@",_otptextone.text,_otptexttwo.text,_otptextthree.text,_otptextfour.text];
    [theTextField resignFirstResponder];
    
    
    NSMutableDictionary *params =[NSMutableDictionary dictionaryWithDictionary:@{@"phone_number":[_userDetails valueForKey:@"mobile"],
                                                                                 @"userEntered":finalenterdOtp,
                                                                                 @"email":[_userDetails valueForKey:@"email"]
                                                                                 }];
    
  //  [self sendOtpToVerify:params];
    [self testUrl];
    

}

-(void)testUrl
{
    [self loadMBProgressCustomLoaderView];
    NSDictionary *params = @{@"email":@"vinay.k@wickedride.com"};
    [self.networkManager startGETRequestWithAPI:aForgotPassword andParameters:params withSuccess:^(NSDictionary *responseDictionary) {
        [self hideMBProgressCutomLoader];
        int status = [[[responseDictionary objectForKey:@"result"] objectForKey:@"status_code"] intValue];
        NSString *msgStr = [[responseDictionary objectForKey:@"result"] objectForKey:@"data"];
        if (status == 200) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Email Sent" message:@"Please check your email to continue" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
            [self dismissViewControllerAnimated:YES completion:nil];
        }
        else
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:msgStr delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    } andFailure:^(NSString *errorMessage) {
        [self hideMBProgressCutomLoader];
        if (![errorMessage isEqualToString:kErrorMessage]) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Something went Wrong. Please try later" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
        else
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:errorMessage delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
    }];
}


/*
-(void)sendOtpToVerify:(NSDictionary *)params
{
   // [self loadMBProgressCustomLoaderView];
    
  //  __weak WROTPVerificationViewController *weakself = self;
    [self.networkManager startPOSTRequestWithAPI:aVerifyOtp andParameters:params withSuccess:^(NSDictionary *responseDictionary) {
        //[weakself hideMBProgressCutomLoader];
        NSString *messageStr = [[responseDictionary objectForKey:@"result"] objectForKey:@"message"];
        if ([messageStr isEqualToString:@"Success!"])
        {
            self.userManager.user = [WRUser getLoggedInUserValueWith:[[responseDictionary objectForKey:@"result"] objectForKey:@"data"]];
            [[NSUserDefaults standardUserDefaults] setObject:[[[responseDictionary objectForKey:@"result"] objectForKey:@"data"] objectForKey:@"token"] forKey:kToken];
//                    [self dismissViewControllerAnimated:YES completion:^{
//                        [self.delegate registrationIsSuccessfull];
//                    }];
        }
        else
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:messageStr delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
        
    } andFailure:^(NSString *errorMessage) {
       // [weakself hideMBProgressCutomLoader];
        if (![errorMessage isEqualToString:kErrorMessage]) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Something went Wrong. Please try later" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
        else
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:errorMessage delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
    }];
}
 */
- (IBAction)resendOtpToTheUser:(id)sender {
    //[self loadMBProgressCustomLoaderView];
    
    __weak WROTPVerificationViewController *weakself = self;
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                                [_userDetails valueForKey:@"password"],@"password",
                                [_userDetails valueForKey:@"first_name"],@"first_name",
                                [_userDetails valueForKey:@"last_name"],@"last_name",
                                [_userDetails valueForKey:@"confirm_password"], @"confirm_pw",
                                nil];

    
    [self.networkManager startPOSTRequestWithAPI:aResendOtp andParameters:params withSuccess:^(NSDictionary *responseDictionary) {
        [weakself hideMBProgressCutomLoader];
        NSString *messageStr = [[responseDictionary objectForKey:@"result"] objectForKey:@"message"];
        if ([messageStr isEqualToString:@"Success!"])
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:messageStr delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
        else
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:messageStr delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
        
    } andFailure:^(NSString *errorMessage) {
        [weakself hideMBProgressCutomLoader];
        if (![errorMessage isEqualToString:kErrorMessage]) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Something went Wrong. Please try later" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
        else
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:errorMessage delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
    }];
}


/*
-(void)loadMBProgressCustomLoaderView
{
    self.loaderView = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.loaderView.customView = [WRCustomLoader sharedInstance].loaderImageView;
    self.loaderView.mode = MBProgressHUDModeCustomView;
    self.loaderView.dimBackground = YES;
    self.loaderView.color = [UIColor whiteColor];
    [[WRCustomLoader sharedInstance].loaderImageView startAnimating];
}
*/

-(void)setOtpHeaderAndTileLabels:(BOOL)isVerified
{
    
    
    if (isVerified)
    {
        _otpHeaderLbl.textColor = [UIColor blackColor];
        _otpHeaderLbl.text = @"Thanks! Please enter the OTP";
        
        
        
    }
    else
    {
        
        _otpHeaderLbl.textColor = [UIColor blackColor];
        _otpHeaderLbl.text = @"Incorrect OTP entered";
       
        _otptextone.text = @"";
        _otptexttwo.text = @"";
        _otptextthree.text = @"";
        _otptextfour.text = @"";
        [_otptextone becomeFirstResponder];
        
        
        
    }
    
    
}
//- (IBAction)cancel:(id)sender {
//    [self.presentingViewController.presentingViewController dismissViewControllerAnimated:YES completion:nil];
//    
//    
//    
//}



-(void)resendOtpTomobileNumber
{
    
    //  NSDictionary *catogeryIds = [NSDictionary dictionaryWithObject: forKey:@"categoryId"];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:
                                [prefs stringForKey:@"selectedLoc"],@"location",
                                @"register",@"userStatus",
                                _PhoneNumber,@"phNo",
                                @"", @"emailId",
                                nil];
    
    /*
    [DataManager reskitResponseWithParameter:parameters getOrPost:getParam andUrl:kRegisterMobileUrl andCallback:^(NSDictionary *dictionary,NSError *error)
     {
         DLog(@"%@",dictionary);
         [Utility displayAlertWithMessage:[NSString stringWithFormat:@"%@",[[dictionary valueForKey:@"otpDetails"] valueForKey:@"otpNumber"]] andTitle:@"OTP"];
         
         
         
         
     }];
    
    
    */
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
