//
//  WRLeftMenuViewController.m
//  WickedRide
//
//  Created by Ajith Kumar on 11/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRLeftMenuViewController.h"
#import "WRConstants.h"
#import "WRLeftMenuTableViewCell.h"
#import "WRLeftMenuSeperatorTableViewCell.h"
#import <UIImageView+AFNetworking.h>
#import "QGSdk.h"


@interface WRLeftMenuViewController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *locationView;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UIImageView *dropDownImageView;
@property (nonatomic, strong) UIView *blackView;
@property (nonatomic, strong) NSArray *locationArray;
@end

@implementation WRLeftMenuViewController

-(void)viewDidLoad
{
    [super viewDidLoad];
    [self addGestureAndConfigureViewsProperty];
}

-(void)addGestureAndConfigureViewsProperty
{
    self.dropDownImageView.userInteractionEnabled = YES;
    [self.view bringSubviewToFront:self.locationView];
    self.locationTableView.hidden = YES;
    self.locationTableView.layer.masksToBounds = NO;
    self.locationTableView.layer.shadowOffset = CGSizeMake(5, 5);
    self.locationTableView.layer.shadowRadius = 5;
    self.locationTableView.layer.shadowOpacity = 0.5;

    self.view.backgroundColor = [UIColor colorWithPatternImage: [UIImage imageNamed:@"Menu-bg2"]];
    self.locationTableView.backgroundColor = [UIColor colorWithPatternImage: [UIImage imageNamed:@"Menu-bg2"]];
    
    UITapGestureRecognizer *profileTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(profileViewTapped)];
    [self.profileImageView addGestureRecognizer:profileTap];

    UITapGestureRecognizer *profileLabelTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(profileViewTapped)];
    [self.userNameLabel addGestureRecognizer:profileLabelTap];

    UITapGestureRecognizer *locationTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(locationViewTapped)];
    [self.locationView addGestureRecognizer:locationTap];
    
    UITapGestureRecognizer *locationDropImageViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(locationDropImageViewTapped)];
    [self.dropDownImageView addGestureRecognizer:locationDropImageViewTap];
    
    self.profileImageView.layer.cornerRadius = self.profileImageView.frame.size.width/2;
    self.profileImageView.clipsToBounds = YES;
}

-(void)reloadTableViewWithDetails
{
    self.locationTableView.scrollEnabled = NO;
    self.locationTableView.hidden = YES;
    [self.blackView removeFromSuperview];
    self.blackView = nil;
    self.locationLabel.text = [[[NSUserDefaults standardUserDefaults] objectForKey:kSelectedLocation] objectForKey:kCityKey];
    self.locationArray = [[NSUserDefaults standardUserDefaults] objectForKey:kLocationArray];

    if (![self.userManager hasPreviousLoggedInUser])
    {
        self.userEmailIdLabel.hidden = YES;
        self.profileImageView.hidden = YES;
        self.userNameLabel.text = @"Sign In";
        [self.locationView changedYAxisTo:65.0];
        [self.locationTableView changedYAxisTo:65.0];
    }
    else
    {
        self.userEmailIdLabel.hidden = NO;
        self.profileImageView.hidden = NO;
        self.userNameLabel.text = [NSString stringWithFormat:@"%@ %@",self.userManager.user.firstName, self.userManager.user.lastName];
        self.userEmailIdLabel.text = self.userManager.user.emailId;
        [self.locationView changedYAxisTo:75.0];
        [self.locationTableView changedYAxisTo:75.0];
    }
    
    if (IS_IPHONE_6_PLUS) {
        [self.userProfileView changeWidthTo:290.0];
        [self.dropDownImageView changedXAxisTo:155.0];
        [self.locationTableView changeWidthTo:185.0 andHeightTo:35*self.locationArray.count];
    }
    else if (IS_IPHONE_5_OR_BELLOW)
    {
        [self.userProfileView changeWidthTo:230.0];
        [self.dropDownImageView changedXAxisTo:105.0];
        [self.locationTableView changeWidthTo:125.0 andHeightTo:35*self.locationArray.count];
    }
    else
    {
        [self.userProfileView changeWidthTo:260.0];
        [self.dropDownImageView changedXAxisTo:130.0];
        [self.locationTableView changeWidthTo:155.0 andHeightTo:35*self.locationArray.count];
    }

    if (![self.userManager.user.profileImageUrl isEmptyString]) {
        NSString *imageUrl = self.userManager.user.profileImageUrl;
        UIImage *placeholderImg = [UIImage imageNamed:@"user_profile"];
        
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:imageUrl]];
        
        __weak UIImageView *weakImg = self.profileImageView;
        
        [self.profileImageView setImageWithURLRequest:request placeholderImage:placeholderImg success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
         {
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 weakImg.image = image;
             });
             
             
         } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
             
         }];
    }
    self.isTableViewScrolled = NO;
    [self.leftMenuTableView reloadData];
}

#pragma mark - UITableViewDataSource/ UITableViewDelegate Methods

-(NSInteger )numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView == self.leftMenuTableView) {
        return 1;
    }
    return 1;
}

-(NSInteger )tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.leftMenuTableView) {
        if (section == 0) {
            if ([self.userManager hasPreviousLoggedInUser]) {
                return 10;
            }
            return 8;
        }
    }
    return self.locationArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.leftMenuTableView)
    {
        WRLeftMenuTableViewCell *cell = (WRLeftMenuTableViewCell *)[tableView dequeueReusableCellWithIdentifier:kLeftMenuTableViewCellID];
        
        if (indexPath.section == 0) {
            switch (indexPath.row) {
                case WRLeftMenuTypeRentBike:
                    cell.leftMenuNameLabel.text = @"RENT A BIKE";
                    break;
                case WRLeftMenuTypeBikation:
                    cell.leftMenuNameLabel.text = @"BIKATION";
                    break;
                case WRLeftMenuTypeSeperator:
                {
                    WRLeftMenuSeperatorTableViewCell *cell = (WRLeftMenuSeperatorTableViewCell *)[tableView dequeueReusableCellWithIdentifier:kLeftMenuSeperatorTableViewCellID];
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    return cell;
                }
                break;
                case WRLeftMenuTypeRefer:
                    cell.leftMenuNameLabel.text = @"REVIEW";
                    break;
                case WRLeftMenuTypeGallery:
                    cell.leftMenuNameLabel.text = @"TARIFF";
                    break;
                case WRLeftMenuTypeFAQ:
                    cell.leftMenuNameLabel.text = @"FAQ's";
                    break;
                case WRLeftMenuTypeNews:
                    cell.leftMenuNameLabel.text = @"ABOUT";
                    break;
                case WRLeftMenuTypeAbout:
                    cell.leftMenuNameLabel.text = @"CONTACT US";
                    break;
                case WRLeftMenuTypeUploadDocument:
                    cell.leftMenuNameLabel.text = @"UPLOAD DOCS";
                    break;
                case WRLeftMenuTypeWallet:
                    cell.leftMenuNameLabel.text = @"WALLET";
                    break;
//                case WRLeftMenuTypeLogout:
//                    cell.leftMenuNameLabel.text = @"LOGOUT";
//                    break;
                default:
                    break;
            }
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    cell.textLabel.text = [[self.locationArray objectAtIndex:indexPath.row] objectForKey:kCityKey];
    cell.textLabel.font = AVENIR_BOOK(14.0);
    cell.textLabel.alpha = 0.7;
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}


-(CGFloat )tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.leftMenuTableView)
    {
        return 40;
    }
    return 35;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.leftMenuTableView) {
        if (indexPath.section == 0) {
            WRLeftMenuFirstSectionType firstSectionSelectionType;
            switch (indexPath.row) {
                case WRLeftMenuTypeRentBike:
                    firstSectionSelectionType = WRLeftMenuTypeRentBike;
                    break;
                case WRLeftMenuTypeBikation:
                    firstSectionSelectionType = WRLeftMenuTypeBikation;
                    break;
                case WRLeftMenuTypeSeperator:
                    firstSectionSelectionType = WRLeftMenuTypeSeperator;
                    break;
                case WRLeftMenuTypeRefer:
                    firstSectionSelectionType = WRLeftMenuTypeRefer;
                    break;
                case WRLeftMenuTypeGallery:
                    firstSectionSelectionType = WRLeftMenuTypeGallery;
                    break;
                case WRLeftMenuTypeFAQ:
                    firstSectionSelectionType = WRLeftMenuTypeFAQ;
                    break;
                case WRLeftMenuTypeNews:
                    firstSectionSelectionType = WRLeftMenuTypeNews;
                    break;
                case WRLeftMenuTypeAbout:
                    firstSectionSelectionType = WRLeftMenuTypeAbout;
                    break;
                case WRLeftMenuTypeUploadDocument:
                    firstSectionSelectionType = WRLeftMenuTypeUploadDocument;
                    break;
                case WRLeftMenuTypeWallet:
                    firstSectionSelectionType = WRLeftMenuTypeWallet;
                    break;
//                case WRLeftMenuTypeLogout:
//                    firstSectionSelectionType = WRLeftMenuTypeLogout;
//                    break;
                default:
                    break;
            }
            if (firstSectionSelectionType != WRLeftMenuTypeSeperator) {
                [self.delegate leftMenuFirstSectionTappedWithSelectedItem:firstSectionSelectionType];
            }
        }
    }
    else
    {
        [self blackViewTapped];
        [[NSUserDefaults standardUserDefaults] setObject:[self.locationArray objectAtIndex:indexPath.row] forKey:kSelectedLocation];
        self.locationLabel.text = [[[NSUserDefaults standardUserDefaults] objectForKey:kSelectedLocation] objectForKey:kCityKey];
        [self callWebServiceToGetAllArea];
        self.locationTableView.hidden = YES;
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationDropDownValueSelected object:nil];
        [self logEventForSelectedCity:self.locationLabel.text];
        
    
    }
}

#pragma mark - QGraph log city selected
-(void)logEventForSelectedCity:(NSString *)cityName
{
    NSMutableDictionary *selectedCity = [[NSMutableDictionary alloc] init];
    [selectedCity setObject:cityName forKey:@"City"];
    [[QGSdk getSharedInstance] logEvent:@"city_selected" withParameters:selectedCity];
    

}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.leftMenuTableView) {
        if (!self.isTableViewScrolled) {
            //1. Setup the CATransform3D structure
            CATransform3D rotation;
            rotation = CATransform3DMakeTranslation(-cell.frame.size.width, 0, 0);
            //2. Define the initial state (Before the animation)
            cell.layer.shadowColor = [[UIColor blackColor]CGColor];
            cell.alpha = 0;
            cell.layer.transform = rotation;
            cell.layer.anchorPoint = CGPointMake(1,0);
            //3. Define the final state (After the animation) and commit the animation
            CGFloat animationDuration = 0.3;
            [UIView animateWithDuration:animationDuration delay:(animationDuration*indexPath.row*.25) options:0 animations:^{
                cell.layer.transform = CATransform3DIdentity;
                cell.alpha = 1;
            } completion:nil];
        }
    }
}


#pragma mark - UIButton/ Tap Guster Methods

-(void )profileViewTapped
{
    [self.delegate leftMenuProfileViewTapped];
}

-(void)locationViewTapped
{
    if (self.locationTableView.hidden == YES) {
        [self addBlackView];
        [self.locationTableView reloadData];
        self.locationTableView.hidden = NO;
        [self.view bringSubviewToFront:self.locationTableView];

    }
    else
    {
        self.locationTableView.hidden = YES;
    }
}

-(void)locationDropImageViewTapped
{
    [self locationViewTapped];
}


#pragma mark - Custom black view

-(void)addBlackView
{
    if (!self.blackView)
    {
        self.blackView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        self.blackView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.2];
        UITapGestureRecognizer *blackViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(blackViewTapped)];
        [self.blackView addGestureRecognizer:blackViewTap];
        [self.view addSubview:self.blackView];
    }
    else
    {
        self.blackView.hidden = NO;
    }
}

-(void)removeBlackView
{
    [self.blackView removeFromSuperview];
    self.blackView = nil;
    self.locationTableView.hidden = YES;
}


-(void)blackViewTapped
{
    [self.blackView removeFromSuperview];
    self.blackView = nil;
    self.locationTableView.hidden = YES;
    
}

#pragma mark - UIScrollViewDelegate Method

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    if (scrollView == self.leftMenuTableView) {
        self.isTableViewScrolled = YES;
    }
}



@end
