//
//  WRLeftMenuViewController.h
//  WickedRide
//
//  Created by Ajith Kumar on 11/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WRBaseViewController.h"

typedef enum
{
    WRLeftMenuTypeRentBike = 0,
    WRLeftMenuTypeBikation,
    WRLeftMenuTypeSeperator,
    WRLeftMenuTypeRefer ,
    WRLeftMenuTypeGallery,
    WRLeftMenuTypeFAQ,
    WRLeftMenuTypeNews,
    WRLeftMenuTypeAbout,
    WRLeftMenuTypeUploadDocument,
    WRLeftMenuTypeWallet,
   // WRLeftMenuTypeLogout
    
}WRLeftMenuFirstSectionType;


@protocol LeftMenuViewControllerDelegate <NSObject>

@optional

-(void)leftMenuFirstSectionTappedWithSelectedItem:(WRLeftMenuFirstSectionType )firstSectionSelectedType;
-(void)leftMenuProfileViewTapped;

@end


@interface WRLeftMenuViewController : WRBaseViewController

@property (weak, nonatomic) IBOutlet UITableView *leftMenuTableView;
@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *userEmailIdLabel;
@property (weak, nonatomic) IBOutlet UIView *userProfileView;
@property (weak, nonatomic) IBOutlet UITableView *locationTableView;

@property (nonatomic, assign) BOOL isTableViewScrolled;

@property (nonatomic, weak) id <LeftMenuViewControllerDelegate> delegate;


-(void)reloadTableViewWithDetails;


@end

