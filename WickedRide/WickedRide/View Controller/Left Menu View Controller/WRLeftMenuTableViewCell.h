//
//  WRLeftMenuTableViewCell.h
//  WickedRide
//
//  Created by Ajith Kumar on 20/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WRLeftMenuTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *leftMenuNameLabel;

@end
