//
//  WRHomeViewController.h
//  WickedRide
//
//  Created by Ajith kumar on 1/28/16.
//  Copyright © 2016 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WRBaseViewController.h"
#import "WRLeftMenuViewController.h"

@interface WRHomeViewController : WRBaseViewController

-(void)loadFirstSectionViewsWithItem:(WRLeftMenuFirstSectionType )menuType;
-(void)leftMenuProfileViewTapped;

@end
