//
//  WRPickLocationViewController.m
//  WickedRide
//
//  Created by Ajith Kumar on 02/09/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRPickLocationViewController.h"
#import "WRBikationViewController.h"
#import "WRBikeListingViewController.h"
#import "QGSdk.h"


#define kTableViewCellHeight 60.0;

@interface WRPickLocationViewController () <UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *locationTableView;

@end

@implementation WRPickLocationViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    // Do any additional setup after loading the view.
    [self loadCustonTitleNavigationViewWith:@"SELECT YOUR LOCATION"];
    self.navigationItem.hidesBackButton = YES;
    self.locationTableView.hidden = YES;
    [self callWebServiceToFetchAvailabelLocation];
//    self.locationTableView.scrollEnabled = NO;
}

-(void)callWebServiceToFetchAvailabelLocation
{
    [self loadMBProgressCustomLoaderView];
    [self.networkManager startGETRequestWithAPI:aGetAllCities andParameters:nil withSuccess:^(NSDictionary *responseDictionary) {
        NSLog(@"%@",responseDictionary);
        self.userManager.locationArray = [[responseDictionary objectForKey:@"result"] objectForKey:@"data"];
        [[NSUserDefaults standardUserDefaults] setObject:self.userManager.locationArray forKey:kLocationArray];
        self.locationTableView.hidden = NO;
        [self changeLocationTableViewFrame];
        [self.locationTableView reloadData];
        [self hideMBProgressCutomLoader];
        
    } andFailure:^(NSString *errorMessage)
    {
        [self hideMBProgressCutomLoader];
        if (![errorMessage isEqualToString:kErrorMessage]) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Something went Wrong. Please try later" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            alertView.tag = 111;
            [alertView show];
        }
        else
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:errorMessage delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            alertView.tag = 111;
            [alertView show];
        }
    }];
}


-(void)changeLocationTableViewFrame
{
//    self.locationTableView.backgroundColor = [UIColor cyanColor];
//    self.locationTableView.backgroundColor = [UIColor purpleColor];
    CGFloat height = 60.0 * self.userManager.locationArray.count;
    if (height>=self.view.frame.size.height) {
        [self.locationTableView changedYAxisTo:0 andHeightTo:self.view.frame.size.height];
        [self.locationTableView changedXAxisTo:0 andWidthTo:self.view.frame.size.width];
    }
    else
    {
//        [self.locationTableView changeWidthTo:self.view.frame.size.width andHeightTo:height];
        
        self.locationTableView.translatesAutoresizingMaskIntoConstraints = YES;
        self.locationTableView.frame = CGRectMake(0, (self.view.frame.size.height - height-22)/2, self.view.frame.size.width, height);
    }
}

#pragma mark - UITableViewDataSource, UITableViewDelegate Methods

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.userManager.locationArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    cell.textLabel.text = [[self.userManager.locationArray objectAtIndex:indexPath.row] objectForKey:kCityKey];
    cell.textLabel.font = AVENIR_BOOK(18.0);
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    cell.contentView.backgroundColor = [UIColor clearColor];
    return cell;
}

-(CGFloat )tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60.0;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:IS_HIDE_LOCATION_VIEW];
    [[NSUserDefaults standardUserDefaults] setObject:[self.userManager.locationArray objectAtIndex:indexPath.row] forKey:kSelectedLocation];
    [self logEventForSelectedCity:[[self.userManager.locationArray objectAtIndex:indexPath.row] objectForKey:kCityKey]];
    
    
    
    if (self.isFromBikation) {
        WRBikationViewController *bikationVC = [self.storyboard instantiateViewControllerWithIdentifier:kBikationViewControllerID];
        [self.navigationController pushViewController:bikationVC animated:YES];
    }
    else
    {
        WRBikeListingViewController *bikeListVC = [self.storyboard instantiateViewControllerWithIdentifier:kBikeListingViewControllerID];
        [self.navigationController pushViewController:bikeListVC animated:YES];
    }
}


#pragma mark - QGraph log city selected
-(void)logEventForSelectedCity:(NSString *)cityName
{
    NSMutableDictionary *selectedCity = [[NSMutableDictionary alloc] init];
    [selectedCity setObject:cityName forKey:@"City"];
    [[QGSdk getSharedInstance] logEvent:@"city_selected" withParameters:selectedCity];
    
    
}


#pragma mark - UIAlertViewDelegate Methods

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 111) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
