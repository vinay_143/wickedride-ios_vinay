//
//  WRPickLocationViewController.h
//  WickedRide
//
//  Created by Ajith Kumar on 02/09/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRBaseViewController.h"

@interface WRPickLocationViewController : WRBaseViewController

@property (nonatomic, assign) BOOL isFromBikation;

@end
