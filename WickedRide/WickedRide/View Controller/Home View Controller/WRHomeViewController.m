//
//  ViewController.m
//  WickedRide
//
//  Created by Ajith Kumar on 10/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRHomeViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "WRBikationViewController.h"
#import "WRBikeListingViewController.h"
#import "WRLoginViewController.h"
#import "WRProfileViewController.h"
#import "WRPickLocationViewController.h"
#import "WRWebViewController.h"
#import "WRAppDelegate.h"
#import "WRUploadDocumentViewController.h"
#import "WRWalletViewController.h"

@interface WRHomeViewController () <UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (nonatomic, strong) WRAppDelegate *appDelegate;


@end

@implementation WRHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.appDelegate = (WRAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle]
                                         pathForResource:@"video1" ofType:@"mp4"]];
    
    //    NSURL *url = [NSURL URLWithString:@"https://www.youtube.com/watch?v=GGErfAmSK9I"];
    
    AVPlayer *avPlayer = [AVPlayer playerWithURL:url];
    AVPlayerLayer *avPlayerLayer = [AVPlayerLayer playerLayerWithPlayer:avPlayer];
    
    avPlayerLayer.frame = self.view.layer.bounds;
    [self.view.layer addSublayer: avPlayerLayer];
    [avPlayer play];

    avPlayerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    
    avPlayer.actionAtItemEnd = AVPlayerActionAtItemEndNone;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:[avPlayer currentItem]];
    [self.view bringSubviewToFront:self.bottomView];
}

-(void)playVideoInBackGround
{
    NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle]
                                         pathForResource:@"video1" ofType:@"mp4"]];
    
    //    NSURL *url = [NSURL URLWithString:@"https://www.youtube.com/watch?v=GGErfAmSK9I"];
    
    AVPlayer *avPlayer = [AVPlayer playerWithURL:url];
    AVPlayerLayer *avPlayerLayer = [AVPlayerLayer playerLayerWithPlayer:avPlayer];
    
    avPlayerLayer.frame = self.view.layer.bounds;
    [self.view.layer addSublayer: avPlayerLayer];
    [avPlayer play];
    
    avPlayerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    
    avPlayer.actionAtItemEnd = AVPlayerActionAtItemEndNone;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:[avPlayer currentItem]];
    [self.view bringSubviewToFront:self.bottomView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playVideoInBackGround) name:kBecomeActivePlayVideo object:nil];
}


#pragma mark - UIButton/Tap Action


- (IBAction)bikationButtonAction:(id)sender
{
    [self addSoundAndVibrateMobileOnButtonAction];
    if ([[NSUserDefaults standardUserDefaults] boolForKey:IS_HIDE_LOCATION_VIEW]) {
        self.navigationController.navigationBarHidden = YES;
        WRBikationViewController *bikationVC = [self.storyboard instantiateViewControllerWithIdentifier:kBikationViewControllerID];
        [self.navigationController pushViewController:bikationVC animated:YES];
    }
    else
    {
        WRPickLocationViewController *pickLocationVC = [self.storyboard instantiateViewControllerWithIdentifier:kPickLocationViewControllerID];
        pickLocationVC.isFromBikation = YES;
        [self.navigationController pushViewController:pickLocationVC animated:YES];
    }
}

- (IBAction)rentARideButtonAction:(id)sender
{
    [self addSoundAndVibrateMobileOnButtonAction];
    if ([[NSUserDefaults standardUserDefaults] valueForKey:IS_HIDE_LOCATION_VIEW]) {
        WRBikeListingViewController *bikeListVC = [self.storyboard instantiateViewControllerWithIdentifier:kBikeListingViewControllerID];
        [self.navigationController pushViewController:bikeListVC animated:YES];

    }
    else
    {
        WRPickLocationViewController *pickLocationVC = [self.storyboard instantiateViewControllerWithIdentifier:kPickLocationViewControllerID];
        pickLocationVC.isFromBikation = NO;
        [self.navigationController pushViewController:pickLocationVC animated:YES];
    }
}



#pragma mark - NSNotification methods

-(void)playerItemDidReachEnd:(NSNotification *)notification
{
    AVPlayerItem *p = [notification object];
    [p seekToTime:kCMTimeZero];
}

#pragma mark Custom method to load All the views

-(void)loadFirstSectionViewsWithItem:(WRLeftMenuFirstSectionType )menuType
{
    switch (menuType) {
        case WRLeftMenuTypeRentBike:
        {
            WRBikeListingViewController *bikeListVC = [self.storyboard instantiateViewControllerWithIdentifier:kBikeListingViewControllerID];
           [self.navigationController pushViewController:bikeListVC animated:YES];
        }
            break;
        case WRLeftMenuTypeBikation:
        {
            self.navigationController.navigationBarHidden = YES;
            WRBikationViewController *bikationVC = [self.storyboard instantiateViewControllerWithIdentifier:kBikationViewControllerID];
            [self.navigationController pushViewController:bikationVC animated:YES];
        }
            break;
        case WRLeftMenuTypeRefer:
            [self loadReviewViewController];
            break;
        case WRLeftMenuTypeGallery:
            [self loadTariffViewController];
            break;
        case WRLeftMenuTypeFAQ:
            [self loadFAQViewController];
            break;
        case WRLeftMenuTypeNews:
            [self loadAboutViewController];
            break;
        case WRLeftMenuTypeAbout:
            [self loadContactUsViewController];
            break;
        case WRLeftMenuTypeUploadDocument:
            [self loadUploadDocumentController];
            break;
        case WRLeftMenuTypeWallet:
            [self loadWalletViewController];
            break;
//        case WRLeftMenuTypeLogout:
//            [self logoutUser];
            break;
            break;
    }
}



#pragma mark Load Left menu view controllers

-(void)leftMenuProfileViewTapped
{
    if (![self.userManager hasPreviousLoggedInUser])
    {
        WRLoginViewController *loginVC = [self.storyboard instantiateViewControllerWithIdentifier:kLoginViewControllerID];
        [self.navigationController pushViewController:loginVC animated:YES];
    }
    else
    {
        WRProfileViewController *profileVC = [self.storyboard instantiateViewControllerWithIdentifier:kProfileViewControllerID];
        [self.navigationController pushViewController:profileVC animated:YES];
    }
    
}

-(void)loadReviewViewController
{
    WRWebViewController *webViewVC = [self.storyboard instantiateViewControllerWithIdentifier:kWebViewControllerID];
    webViewVC.isFromReview = YES;
    [self.navigationController pushViewController:webViewVC animated:YES];
}

-(void)loadTariffViewController
{
    WRWebViewController *webViewVC = [self.storyboard instantiateViewControllerWithIdentifier:kWebViewControllerID];
    webViewVC.isFromTariff = YES;
    [self.navigationController pushViewController:webViewVC animated:YES];
}

-(void)loadFAQViewController
{
    WRWebViewController *webViewVC = [self.storyboard instantiateViewControllerWithIdentifier:kWebViewControllerID];
    webViewVC.isFromFAQ = YES;
    [self.navigationController pushViewController:webViewVC animated:YES];
}

-(void)loadAboutViewController
{
    WRWebViewController *webViewVC = [self.storyboard instantiateViewControllerWithIdentifier:kWebViewControllerID];
    webViewVC.isFromAboutUs = YES;
    [self.navigationController pushViewController:webViewVC animated:YES];
}

-(void)loadContactUsViewController
{
    WRWebViewController *webViewVC = [self.storyboard instantiateViewControllerWithIdentifier:kWebViewControllerID];
    webViewVC.isFromContactUs = YES;
    [self.navigationController pushViewController:webViewVC animated:YES];
}


-(void)loadUploadDocumentController
{
    WRUploadDocumentViewController *uploadDocsVC = [self.storyboard instantiateViewControllerWithIdentifier:kUploadViewControllerID];
    [self.navigationController pushViewController:uploadDocsVC animated:YES];
}

-(void)loadWalletViewController
{

    WRWalletViewController *walletVC = [self.storyboard instantiateViewControllerWithIdentifier:kWRWalletViewControllerID];
    [self.navigationController pushViewController:walletVC animated:YES];
    
}

-(void)logoutUser
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Logout" message:@"Are you sure?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    [alert show];
}


#pragma UIAlertViewDelegate Methods

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 1:
            [self.userManager logOutUser];
            [self.navigationController popToRootViewControllerAnimated:YES];
            break;
            
        default:
            break;
    }
}

#pragma mark - Dealloc the objects

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self]; // Remove notification object
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
