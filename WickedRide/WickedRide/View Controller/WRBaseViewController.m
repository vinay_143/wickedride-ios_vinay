//
//  WRBaseViewController.m
//  WickedRide
//
//  Created by Ajith Kumar on 10/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRBaseViewController.h"
#import "WRAppDelegate.h"
#import "WRCustomLoader.h"
#import "WRLeftMenuViewController.h"
#import <AVFoundation/AVFoundation.h>


@interface WRBaseViewController ()<UINavigationControllerDelegate, WRCustomNavViewDelegate>

@property (nonatomic, assign) BOOL isFromBikeList;
@property UIScreenEdgePanGestureRecognizer *edgePanGestureRecognizer;
@property UIPercentDrivenInteractiveTransition *percentDrivenInteractiveTransition;


@end

@implementation WRBaseViewController

-(void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = NO;
    self.userManager = [WRUserManager sharedManager];
    self.networkManager = [WRNetworkManager sharedManager];
}

#pragma mark - Load CustomNavigationView

-(void)loadCustomNavigationViewWithTitle:(NSString *)titleString
{
    if (self.customNavView == nil) {
        self.customNavView = [[[NSBundle mainBundle] loadNibNamed:kCustomNavigationViewID owner:self options:nil] firstObject];
        self.customNavView.frame = CGRectMake(0, 0, self.view.frame.size.width, 64);
        self.customNavView.delegate = self;
        self.customNavView.navTitleLabel.text = titleString;
        [self.view addSubview:self.customNavView];
    }
}


#pragma mark - Interactive Gestures

//- (void)setupInteractiveGestures
//{
//    // Setup a screen edge pan gesture recognizer to trigger from the right edge of the screen.
//    self.edgePanGestureRecognizer = [[UIScreenEdgePanGestureRecognizer alloc] initWithTarget:self action:@selector(handleScreenEdgePanGesture:)];
//    
//    /* to detect pan gestures from right to left */
//    //self.edgePanGestureRecognizer.edges = UIRectEdgeRight;
//    
//    /* to detect pan gesture from left to right*/
//    self.edgePanGestureRecognizer.edges = UIRectEdgeLeft;
//    
//    [self.view addGestureRecognizer:self.edgePanGestureRecognizer];
//}
//
//
//- (void)handleScreenEdgePanGesture:(UIScreenEdgePanGestureRecognizer *)sender
//{
//    CGFloat width = self.view.frame.size.width,
//    
//    //    // For swipe from right to left (push)
//    //    percent = MAX(-[sender translationInView:self.view].x, 0) / width;
//    
//    // For swipe from left to right (pop)
//    percent = MAX([sender translationInView:self.view].x, 0) / width;
//    
//    switch (sender.state) {
//        case UIGestureRecognizerStateBegan:
//            // Configure the navigation controller to return the custom (interactive/animated) transition.
//            self.navigationController.delegate = self;
//            // Trigger a segue (or push another view controller, whatevs you want).
//            if (self.navigationController.viewControllers.count > 1)
//            {
//                [self.navigationController popViewControllerAnimated:YES];
//            }
//            break;
//        case UIGestureRecognizerStateChanged:
//            // Update the transition using a UIPercentDrivenInteractiveTransition.
//            [self.percentDrivenInteractiveTransition updateInteractiveTransition:percent];
//            break;
//        case UIGestureRecognizerStateEnded:
//            // Cancel or finish depending on how the gesture ended.
//            if (percent > 0.5 || fabs([sender velocityInView:self.view].x) > 1000)
//                [self.percentDrivenInteractiveTransition finishInteractiveTransition];
//            else
//                [self.percentDrivenInteractiveTransition cancelInteractiveTransition];
//            break;
//        default:
//            NSLog(@"unhandled state for gesture=%@", sender);
//    }
//}



#pragma mark - Navigation view custom methods

-(void)showNavigationMenuForFilterScreenWithTitle: (NSString *)titleString andIsFromBikeList:(BOOL )isFromBikeList
{
    self.isFromBikeList = isFromBikeList;
    self.navigationController.navigationBarHidden = NO;
    
    UIBarButtonItem *spacerLeft = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    [spacerLeft setWidth:-10];
    UIBarButtonItem *spacerRight = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    [spacerRight setWidth:-16];
    
    if (!self.leftmenuBarButton) {
        self.leftmenuBarButton = [[UIBarButtonItem alloc] init];
        [self.leftmenuBarButton setCustomView:[self setCustomViewWithImageName:@"menu" action:@selector(leftMenuBarBtnTapped:) target:self]];
        
    }
    
    if (!self.nextBarButton) {
        self.nextBarButton = [[UIBarButtonItem alloc] init];

        if (isFromBikeList) {
            [self.nextBarButton setCustomView:[self setCustomViewWithImageName:@"filter-icon" action:@selector(filterNextBarBtnTapped:) target:self]];
        }
        else
        {
            [self.nextBarButton setCustomView:[self setCustomViewWithImageName:@"arrow" action:@selector(filterNextBarBtnTapped:) target:self]];
        }
    }
    
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:spacerLeft, self.leftmenuBarButton, nil, nil] animated:YES];
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:spacerRight, self.nextBarButton, nil, nil] animated:YES];
    
    [self loadCustonTitleNavigationViewWith:titleString];
}



-(void)showNavigationMenuAndEditButtonForProfileScreenWithTitle: (NSString *)titleString
{
    UIBarButtonItem *spacerLeft = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    [spacerLeft setWidth:-10];
    UIBarButtonItem *spacerRight = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    [spacerRight setWidth:-10];

    if (!self.leftmenuBarButton) {
        self.leftmenuBarButton = [[UIBarButtonItem alloc] init];
        [self.leftmenuBarButton setCustomView:[self setCustomViewWithImageName:@"menu" action:@selector(leftMenuBarBtnTapped:) target:self]];
        
    }
    if (!self.nextBarButton) {
        self.nextBarButton = [[UIBarButtonItem alloc] init];
        
        [self.nextBarButton setCustomView:[self setCustomViewWithImageName:@"pencil" action:@selector(editProfileBarBtnTapped:) target:self]];
    }
    
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:spacerLeft, self.leftmenuBarButton, nil, nil] animated:YES];
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:spacerRight, self.nextBarButton, nil, nil] animated:YES];

    [self loadCustonTitleNavigationViewWith:titleString];
}

-(void)showNavigationMenuForFilterScreenWithTitle: (NSString *)titleString
{
    UIBarButtonItem *spacerLeft = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    [spacerLeft setWidth:-10];
    UIBarButtonItem *spacerRight = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    [spacerRight setWidth:-10];

    if (!self.leftmenuBarButton) {
        self.leftmenuBarButton = [[UIBarButtonItem alloc] init];
        [self.leftmenuBarButton setCustomView:[self setCustomViewWithImageName:@"menu" action:@selector(leftMenuBarBtnTapped:) target:self]];
        
    }
    if (!self.nextBarButton) {
        self.nextBarButton = [[UIBarButtonItem alloc] init];
         
        [self.nextBarButton setCustomView:[self setCustomViewWithImageName:@"" action:@selector(editProfileBarBtnTapped:) target:self]];
    }

    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:spacerLeft, self.leftmenuBarButton, nil, nil] animated:YES];
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:spacerRight, self.nextBarButton, nil, nil] animated:YES];
    [self loadCustonTitleNavigationViewWith:titleString];
}

-(void)loadCustonTitleNavigationViewWith:(NSString *)titleString
{
    UIView *titleContainer = [[UIView alloc] initWithFrame:CGRectMake(0, 7, self.view.frame.size.width-110, 30)];
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, titleContainer.frame.size.width, 30)];
    titleLabel.text = titleString;
    titleLabel.adjustsFontSizeToFitWidth = YES;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.font = MONTSERRAT_REGULAR(17.0);
    titleLabel.textColor = [UIColor colorWithRed:29.0/255.0 green:29.0/255.0 blue:38.0/255.0 alpha:1.0];
    [titleContainer addSubview:titleLabel];
    titleContainer.center = self.navigationItem.titleView.center;
    self.navigationItem.titleView = titleContainer;
}


-(void)showNavigationViewWithBackButtonWithTitle:(NSString *)title
{
    UIBarButtonItem *spacerLeft = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    [spacerLeft setWidth:-10];
    UIBarButtonItem *spacerRight = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    [spacerRight setWidth:-10];

    if (!self.backBarButton) {
        self.backBarButton = [[UIBarButtonItem alloc] init];
        [self.backBarButton setCustomView:[self setCustomViewWithImageName:@"back-arrow-black" action:@selector(backBarButtonTapped:) target:self]];
    }
    if (!self.nextBarButton) {
        self.nextBarButton = [[UIBarButtonItem alloc] init];
        
        [self.nextBarButton setCustomView:[self setCustomViewWithImageName:@"" action:@selector(editProfileBarBtnTapped:) target:self]];
    }
    
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:spacerLeft, self.backBarButton, nil, nil] animated:YES];
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:spacerRight, self.nextBarButton, nil, nil] animated:YES];

    if (![title isEmptyString]) {
        [self loadCustonTitleNavigationViewWith:title];
    }
}

-(void)showNavigationViewWithBackButtonWithAttributeTitle:(NSString *)title
{
    UIBarButtonItem *spacerLeft = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    [spacerLeft setWidth:-10];
    UIBarButtonItem *spacerRight = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    [spacerRight setWidth:-10];
    if (!self.backBarButton) {
        self.backBarButton = [[UIBarButtonItem alloc] init];
        [self.backBarButton setCustomView:[self setCustomViewWithImageName:@"back-arrow-black" action:@selector(backBarButtonTapped:) target:self]];
    }
    if (!self.nextBarButton) {
        self.nextBarButton = [[UIBarButtonItem alloc] init];
        
        [self.nextBarButton setCustomView:[self setCustomViewWithImageName:@"" action:@selector(editProfileBarBtnTapped:) target:self]];
    }
    
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:spacerLeft, self.backBarButton, nil, nil] animated:YES];
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:spacerRight, self.nextBarButton, nil, nil] animated:YES];

    if (![title isEmptyString]) {
        [self loadCustomAttributeTitleNavigationViewWith:title];
    }
}

-(void)loadCustomAttributeTitleNavigationViewWith:(NSString *)titleString
{
    UIView *titleContainer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width-110, 44)];
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width-110, 44)];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.numberOfLines = 2;
    titleLabel.font = MONTSERRAT_REGULAR(17.0);
    titleLabel.textColor = [UIColor colorWithRed:29.0/255.0 green:29.0/255.0 blue:38.0/255.0 alpha:1.0];
    
    NSMutableAttributedString *reserveText = [[NSMutableAttributedString alloc] initWithString:@"Reserve your\n"];
    [reserveText addAttribute:NSFontAttributeName value:MONTSERRAT_LIGHT(15.0) range:NSMakeRange(0, reserveText.length)];

    
    NSMutableAttributedString *bikeNameText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ TODAY!",titleString]];
    [bikeNameText addAttribute:NSFontAttributeName value:MONTSERRAT_BOLD(18.0) range:NSMakeRange(0, bikeNameText.length)];
    
    [reserveText appendAttributedString:bikeNameText];
    titleLabel.attributedText = reserveText;
    [titleContainer addSubview:titleLabel];
    self.navigationItem.titleView = titleContainer;
}

-(UIView *) setCustomViewWithImageName:(NSString *)imageName action:(SEL)actionSel target:(id)target
{
    UIView *view =[[UIView alloc] initWithFrame:CGRectMake(0, 0, 44 , 40)];
    UIImageView *leftMenuImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageName]];
    leftMenuImg.frame = CGRectMake(0, 0, leftMenuImg.frame.size.width, leftMenuImg.frame.size.height);
    leftMenuImg.center = CGPointMake(20, 20);
    leftMenuImg.contentMode = UIViewContentModeScaleAspectFit;
    [view addSubview:leftMenuImg];
    [view addTapGestureWithTarget:target action:actionSel];
    return view;
}

#pragma mark - Navigation View Button Actions

- (IBAction)leftMenuBarBtnTapped:(id)sender {
    WRAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    [appDelegate showLeftMenuViewController];
}

- (IBAction)filterNextBarBtnTapped:(id)sender
{
    
}

-(IBAction)editProfileBarBtnTapped:(id)sender
{
    
}

- (IBAction)backBarButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - WRCustomNavViewDelegate Methods

-(void)customNavCloseButtonTapped
{
    [self.view endEditing:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)customNavBackButtonTapped
{
    [self.view endEditing:YES];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Transition and Animation Methods

-(void)viewZoomInEffect:(UIView *)aView
{
    
    aView.alpha = 0.0f;
    aView.transform = CGAffineTransformMakeScale(0.1,0.1);
    [UIView animateWithDuration:0.2 animations:^{
        aView.transform = CGAffineTransformMakeScale(1.0,1.0);
        aView.alpha = 1.0f;
    }];
    
}

-(void)viewZoomOutEffect:(UIView *)aView
{
    
    aView.alpha = 1.0f;
    aView.transform = CGAffineTransformMakeScale(1.0,1.0);
    [UIView animateWithDuration:0.2 animations:^{
        aView.transform = CGAffineTransformMakeScale(0.1,0.1);
        aView.alpha = 0.0f;
    }];
    
}

#pragma mark Get All Areas API Call

-(void)callWebServiceToGetAllArea
{
    NSString *city_id = [NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults] valueForKey:kSelectedLocation] objectForKey:@"id"]];
    [self.networkManager startGETRequestWithAPI:aGetAllAreas(city_id) andParameters:nil withSuccess:^(NSDictionary *responseDictionary) {
        self.userManager.areaArray = [[responseDictionary objectForKey:@"result"] objectForKey:@"data"];
    } andFailure:^(NSString *errorMessage) {
        
    }];
}

-(void)loadMBProgressCustomLoaderView
{
    self.loaderView = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.loaderView.customView = [WRCustomLoader sharedInstance].loaderImageView;
    self.loaderView.mode = MBProgressHUDModeCustomView;
    self.loaderView.dimBackground = YES;
    self.loaderView.color = [UIColor whiteColor];
    [[WRCustomLoader sharedInstance].loaderImageView startAnimating];
}

-(void)hideMBProgressCutomLoader
{
    self.loaderView.hidden = YES;
}

#pragma mark - Change Date

-(NSDate *)returnDateFromString:(NSString *)dateStr
{
    // Convert string to date object
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [dateFormat dateFromString:dateStr];
    return [date dateByAddingTimeInterval:60*60*24*1];
}


#pragma mark - Interactive Gestures

- (void)setupInteractiveGestures
{
    // Setup a screen edge pan gesture recognizer to trigger from the right edge of the screen.
    self.edgePanGestureRecognizer = [[UIScreenEdgePanGestureRecognizer alloc] initWithTarget:self action:@selector(handleScreenEdgePanGesture:)];
    
    /* to detect pan gestures from right to left */
    //self.edgePanGestureRecognizer.edges = UIRectEdgeRight;
    
    /* to detect pan gesture from left to right*/
    self.edgePanGestureRecognizer.edges = UIRectEdgeLeft;
    
    [self.view addGestureRecognizer:self.edgePanGestureRecognizer];
}


- (void)handleScreenEdgePanGesture:(UIScreenEdgePanGestureRecognizer *)sender
{
    CGFloat width = self.view.frame.size.width,
    
    //    // For swipe from right to left (push)
    //    percent = MAX(-[sender translationInView:self.view].x, 0) / width;
    
    // For swipe from left to right (pop)
    percent = MAX([sender translationInView:self.view].x, 0) / width;
    
    // DISABLE INTERACTION IN PAYMENT SCREENS
    switch (sender.state) {
        case UIGestureRecognizerStateBegan:
            // Configure the navigation controller to return the custom (interactive/animated) transition.
            self.navigationController.delegate = self;
            // Trigger a segue (or push another view controller, whatevs you want).
            if (self.navigationController.viewControllers.count > 1)
            {
                [self.navigationController popViewControllerAnimated:YES];
            }
            break;
        case UIGestureRecognizerStateChanged:
            // Update the transition using a UIPercentDrivenInteractiveTransition.
            [self.percentDrivenInteractiveTransition updateInteractiveTransition:percent];
            break;
        case UIGestureRecognizerStateEnded:
            // Cancel or finish depending on how the gesture ended.
            if (percent > 0.5 || fabs([sender velocityInView:self.view].x) > 1000)
                [self.percentDrivenInteractiveTransition finishInteractiveTransition];
            else
                [self.percentDrivenInteractiveTransition cancelInteractiveTransition];
            break;
        default:
            NSLog(@"unhandled state for gesture=%@", sender);
    }
}

- (id<UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController animationControllerForOperation:(UINavigationControllerOperation)operation fromViewController:(UIViewController *)fromVC toViewController:(UIViewController *)toVC
{
    // Have to return a custom transition here in order for the navigation controller to ask about the interactive stuff below. The SlideAnimatedTransitioning just tries to duplicate the default transition as much as possible.
    return [WRSlideAnimatedTransitioning new];
}

- (id<UIViewControllerInteractiveTransitioning>)navigationController:(UINavigationController *)navigationController interactionControllerForAnimationController:(id<UIViewControllerAnimatedTransitioning>)animationController
{
    // Unset the delegate so that all other types of transitions (back, normal animated push not initiated by a gesture) get the default behavior.
    self.navigationController.delegate = nil;
    
    if (self.edgePanGestureRecognizer.state == UIGestureRecognizerStateBegan) {
        self.percentDrivenInteractiveTransition = [UIPercentDrivenInteractiveTransition new];
        self.percentDrivenInteractiveTransition.completionCurve = UIViewAnimationOptionCurveEaseOut;
    } else {
        self.percentDrivenInteractiveTransition = nil;
    }
    
    return self.percentDrivenInteractiveTransition;
}


-(void)addSoundAndVibrateMobileOnButtonAction
{
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
    [[AVAudioSession sharedInstance] setActive: YES error: nil];
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
    SystemSoundID soundID;
    
    NSString *soundPath = [[NSBundle mainBundle] pathForResource:@"motorcycle-Start_wickedride" ofType:@"mp3"];
    NSURL *soundUrl = [NSURL fileURLWithPath:soundPath];
    
    AudioServicesCreateSystemSoundID ((__bridge CFURLRef)soundUrl, &soundID);
    AudioServicesPlaySystemSound(soundID);
}


@end
