//
//  WRConstants.h
//  WickedRide
//
//  Created by Ajith Kumar on 10/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#ifndef WickedRide_WRConstants_h
#define WickedRide_WRConstants_h

#define IS_IPHONE [[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone
#define IS_IPHONE_5_OR_BELLOW (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height <= 568.0f)
#define IS_IPHONE_6_OR_BELLOW (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height <= 667.0f)
#define IS_IPHONE_4 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 480.0f)
#define IS_IPHONE_6_PLUS (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 736.0f)



#define kFacebookURLSchema @"fb126945310983186"
#define kGooglePlusiOSAppClientId @"826013063776-r76f0k6d5h3b5hr6lapadtvr6eb3njqf.apps.googleusercontent.com"
#define kAppId @"1121704128"
#define kQGraphId @"05bb9b608429d78d961e"

#define kCityKey @"city_name"

// User Login details

#define IS_USER_LOGGED_IN @"isUserLoggedIn"
#define IS_LOGGED_IN_USER_DETAILS @"loggedInUserDetails"
#define IS_HIDE_LOCATION_VIEW @"isHideLocationView"
#define kSelectedLocation @"selectedLocation"
#define kLocationArray @"locationArray"
#define kAreasArray @"areaArray"
#define kToken @"token"
#define kReferelCode "referel_code"
#define kDateDictionary @"dateDictionary"
#define kErrorMessage @"The Internet connection appears to be offline."


// Base Url

#define Base_URL @"https://wickedride.com/api/"
//#define Base_URL @"http://test4238.wickedride.com/api/"

// Razor Pay Keys

//#define kRazorPayPublicKey @"rzp_test_dkY8raHW2tnsD3" //test
#define kRazorPayPublicKey @"rzp_live_n343RhLr067fS0" //live

// View Controller identifiers

#define kHomeViewControllerID @"WRHomeViewController"
#define kRentBikeFilterViewControllerID @"WRRentBikeFilterViewController"
#define kLeftMenuViewControllerID @"WRLeftMenuViewController"
#define kBikeListingViewControllerID @"WRBikeListingViewController"
#define kLoginViewControllerID @"WRLoginViewController"
#define kSignUpViewControllerID @"WRSignUpViewController"
#define kCropImageViewControllerID @"WRCropImageViewController"
#define kForgorPasswordViewControllerID @"WRForgotPasswordViewController"
#define kBikeDetailsViewControllerID @"WRBikeDetailsViewController"
#define kReservationViewControllerID @"WRReservationViewController"
#define kAccessoriesViewControllerID @"WRAccessoriesViewController"
#define kSummaryViewControllerID @"WRSummaryViewController"
#define kBikationViewControllerID @"WRBikationViewController"
#define kBikationDetailsViewControllerID @"WRBikationDetailsViewController"
#define kBikationConfirmViewControllerID @"WRBikationConfirmViewController"
#define kProfileViewControllerID @"WRProfileViewController"
#define kThankYouViewControllerID @"WRThankYouViewController"
#define kPickLocationViewControllerID @"WRPickLocationViewController"
#define kCalenderViewControllerID @"WRCalenederViewController"
#define kClockViewControllerID @"WRClockViewController"
#define kEditProfileViewControllerID @"WREditProfileViewController"
#define kWebViewControllerID @"WRWebViewController"
#define kResetViewControllerID @"WRResetViewController"
#define kBikeAvailabilityCalendarViewControllerID @"WRBikeAvailabilityCalendarViewController"
#define kReviewViewControllerID @"WRReviewViewController"
#define kUploadViewControllerID @"WRUploadDocumentViewController"
#define kGalleryViewControllerID @"WRGalleryViewController"
#define kUploadImageViewControllerID @"WRUploadImageViewController"
#define kWRWalletViewControllerID @"WRWalletViewController"
#define KWROTPVerificationViewController @"WROTPVerificationViewController"


// Collection/Table View Cell Identifiers

#define kbikationCollectionViewCellID @"WRBikationCollectionViewCell"
#define kbikeListingCollectionViewCellID @"WRBikeListingCollectionViewCell"
#define kRentBikeCommonFilterCollectionViewCellID @"WRCommonFilterCollectionViewCell"
#define kBikesBrandFilterCollectionViewCellID @"WRBikesBrandCollectionViewCell"
#define kPickDateFilterCollectionViewCellID @"WRPickDateCollectionViewCell"
#define kNoteFilterCollectionViewCellID @"WRNoteFilterCollectionViewCell"
#define kLeftMenuSeperatorTableViewCellID @"WRLeftMenuSeperatorTableViewCell"
#define kLeftMenuTableViewCellID @"WRLeftMenuTableViewCell"
#define kBikeDetailsImageViewTableViewCellID @"WRBikeDetailsImageViewTableViewCell"
#define kBikeDetailsTableViewCellID @"WRBikeDetailsTableViewCell"
#define kBikeDetailsBookNowTableViewCellID @"WRBikeDetailBookNowTableViewCell"
#define kAccessoriesTableViewCellID @"WRAccessoriesTableViewCell"
#define kSummaryBikeDetailsTableViewCellID @"WRSummaryBikeDetailTableViewCell"
#define kSummaryAccessoryTableViewCellID @"WRSummaryAccessoryTableViewCell"
#define kSummaryTableViewCellID @"WRSummaryTableViewCell"
#define kBikationGeneralInfoTableViewCellID @"WRBikationGeneralInfoTableViewCell"
#define kBikationGeneralDetailsTableViewCellID @"WRBikationGeneralDetailsTableViewCell"
#define kBikationReviewsTableViewCellID @"WRBikationReviewTableViewCell"
#define kBikationMoreInfoTableViewCellID @"WRBikationMoreInfoTableViewCell"
#define kBikationMoreInfoRuleTableViewCellID @"WRMoreInfoRuleTableViewCell"
#define kBikeCalenderCollectionViewCellID @"WRBikeCalenderCollectionViewCell"
#define kClockTableViewCellID @"WRClockTableViewCell"
#define kPromoCodeTableViewCellID @"WRPromoCodeTableViewCell"
#define kSummaryTermsTableViewCellID @"WRSummaryTermsTableViewCell"
#define kUploadDocumentTableViewCellID @"WRUploadDocumentTableViewCell"

// UIView Identifiers

#define kswitchViewID @"WRSwitchView"
#define kCustomNavigationViewID @"WRCustomNavView"
#define kRentalViewID @"WRRentalView"
#define kAccessoryViewID @"WRAccessoryView"
#define kBikationMoreInfoViewID @"WRBikationMoreInfoView"
#define kBikesCurrentBookingViewID @"WRBikesCurrentBookingView"
#define kClockViewID @"WRClockView"
#define kCalendarViewID @"WRCalenderPopUpView"
#define kEventsDetailViewID @"WREventsDetailView"




/* FONTS */
#define MONTSERRAT_SEMIBOLD(fontsize) [UIFont fontWithName:@"Montserrat-SemiBold" size:fontsize]
#define MONTSERRAT_BOLD(fontsize) [UIFont fontWithName:@"Montserrat-Bold" size:fontsize]
#define MONTSERRAT_BLACK(fontsize) [UIFont fontWithName:@"Montserrat-Black" size:fontsize]
#define MONTSERRAT_REGULAR(fontsize) [UIFont fontWithName:@"Montserrat-Regular" size:fontsize]
#define MONTSERRAT_HAIRLINE(fontsize) [UIFont fontWithName:@"Montserrat-Hairline" size:fontsize]
#define MONTSERRAT_ULTRALIGHT(fontsize) [UIFont fontWithName:@"Montserrat-UltraLight" size:fontsize]
#define MONTSERRAT_LIGHT(fontsize) [UIFont fontWithName:@"Montserrat-Light" size:fontsize]
#define MONTSERRAT_EXTRABOLD(fontsize) [UIFont fontWithName:@"Montserrat-ExtraBold" size:fontsize]

#define AVENIR_MEDIUM(fontsize) [UIFont fontWithName:@"Avenir-Medium" size:fontsize]
#define AVENIR_HEAVYOBLIQUE(fontsize) [UIFont fontWithName:@"Avenir-HeavyOblique" size:fontsize]
#define AVENIR_BOOK(fontsize) [UIFont fontWithName:@"Avenir-Book" size:fontsize]
#define AVENIR_LIGHT(fontsize) [UIFont fontWithName:@"Avenir-Light" size:fontsize]
#define AVENIR_ROMAN(fontsize) [UIFont fontWithName:@"Avenir-Roman" size:fontsize]
#define AVENIR_BOOKOBLIQUE(fontsize) [UIFont fontWithName:@"Avenir-BookOblique" size:fontsize]
#define AVENIR_BLACK(fontsize) [UIFont fontWithName:@"Avenir-Black" size:fontsize]
#define AVENIR_MEDIUMOBLIQUE(fontsize) [UIFont fontWithName:@"Avenir-MediumOblique" size:fontsize]
#define AVENIR_BLACKOBLIQUE(fontsize) [UIFont fontWithName:@"Avenir-BlackOblique" size:fontsize]
#define AVENIR_HEAVY(fontsize) [UIFont fontWithName:@"Avenir-Heavy" size:fontsize]
#define AVENIR_LIGHTOBLIQUE(fontsize) [UIFont fontWithName:@"Avenir-LightOblique" size:fontsize]
#define AVENIR_OBLIQUE(fontsize) [UIFont fontWithName:@"Avenir-Oblique" size:fontsize]



#define ROBOTO_THIN(fontsize) [UIFont fontWithName:@"Roboto-Thin" size:fontsize]
#define ROBOTO_ITALIC(fontsize) [UIFont fontWithName:@"Roboto-Italic" size:fontsize]
#define ROBOTO_BLAK_ITALIC(fontsize) [UIFont fontWithName:@"Roboto-BlackItalic" size:fontsize]
#define ROBOTO_LIGHT(fontsize) [UIFont fontWithName:@"Roboto-Light" size:fontsize]
#define ROBOTO_BOLD_ITALIC(fontsize) [UIFont fontWithName:@"Roboto-BoldItalic" size:fontsize]
#define ROBOTO_LIGHT_ITALIC(fontsize) [UIFont fontWithName:@"Roboto-LightItalic" size:fontsize]
#define ROBOTO_THIN_ITALIC(fontsize) [UIFont fontWithName:@"Roboto-ThinItalic" size:fontsize]
#define ROBOTO_BLACK(fontsize) [UIFont fontWithName:@"Roboto-Black" size:fontsize]
#define ROBOTO_BOLD(fontsize) [UIFont fontWithName:@"Roboto-Bold" size:fontsize]
#define ROBOTO_REGULAR(fontsize) [UIFont fontWithName:@"Roboto-Regular" size:fontsize]
#define ROBOTO_MEDIUM(fontsize) [UIFont fontWithName:@"Roboto-Medium" size:fontsize]
#define ROBOTO_MEDIUM_ITALIC(fontsize) [UIFont fontWithName:@"Roboto-MediumItalic" size:fontsize]


// API's Identifiers

#define aGetAllCities @"cities"
#define aGetAllMakeLogo @"makes/by-city"
#define aGetBikeList(city_id) [NSString stringWithFormat:@"cities/%@/models",city_id]
#define aGetAllAreas(city_id) [NSString stringWithFormat:@"cities/%@/areas",city_id]

#define aBikeDetails @"models"
#define aFromDateAvailabiltyCalender @"from-availability-calendar"
#define aToDateAvailabiltyCalender @"to-availability-calendar"
#define aGetTotalPrice @"bookings/total-price"
#define aBikeAccessories @"accessories/find"
#define aPromoCode @"promocodes/apply"
#define aCheckBalance @"wallet/check_balance"
#define aApplyAmount @"wallet/apply_amount"

#define aBikationList @"bikations"
#define aReviewOnBikation(bikation_id) [NSString stringWithFormat:@"bikations/%@/reviews",bikation_id]

#define aGetLoginDetails @"signin"
#define aRegistration @"signup"
#define aForgotPassword @"forgot-password"
#define aResetPassword @"reset-password"
#define aProfileDetails @"me/profile"
#define aUploadProfileImage @"update-profile-image"
#define aGetAllHtmlUrl @"urls"
#define aGetFromWorkingHourApi @"from-working-hours"
#define aGetToWorkingHourApi @"to-working-hours"
#define aGetUploadedDocuments @"get_user_document_status"
#define aUploadUserDocuments @"user_document/upload"
#define aDeleteUploadedDocument @"user_document/delete"
#define aCheckBalance @"wallet/check_balance"
#define aVerifyOtp @"verify-otp"
#define aResendOtp @"resend-otp"
#define aDocRequest @"http://wickedride.com/app/docs-req"

#define aRentBikeBookingApi @"bookings"
#define aBookingResultApi(booking_id) [NSString stringWithFormat:@"bookings/%@",booking_id]

#define kQGraphId @"05bb9b608429d78d961e"
//#define kTermsConditionsHtmlFileUrl @"http://www.wickedride.com/api/v1/tc"
//#define kFAQHtmlFileUrl @"http://54.255.177.26/faqs"
//#define kReviewHtmlFileUrl @"http://54.255.177.26/reviews"
//#define kContactUsHtmlFileUrl @"http://54.255.177.26/contact-us"
//#define kAboutHtmlFileUrl @"http://54.255.177.26/about-us"
//#define kTariffHtmlFileUrl @"http://54.255.177.26/tariff"

#define nullCheck(str) ([str isEqual:[NSNull null]] || !str) ? @"" : [NSString stringWithFormat:@"%@",str]

// Notification keys

#define kNotificationDropDownValueSelected @"dropDrownValueSelected"
#define kNotificationBikeAvailableDateSelected @"bikeAvailableDateSelected"
#define kNotificationCalendarDateSelected @"calendarDateSelected"
#define kNotificationCalendarDateSelectedFromBikeListing @"calendarDateSelectedFromBikeListing"
#define kBecomeActivePlayVideo @"playVideo"
#define kDismissLoginView @"dissmissLoginVC"

//Paytm Check Sum Urls

#define kPaytmGenerateCheckSumUrl @"https://wickedride.com/generateChecksum.php"
#define kPaytmVerifyCheckSumUrl @"https://wickedride.com/verifyChecksum.php"
#endif
